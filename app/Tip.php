<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tip extends Model
{
  public function motorcyclist()
  {
      return $this->belongsTo(Motorcyclist::class,'code','code');
  }
}
