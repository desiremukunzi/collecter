<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Motari_moto extends Model
{
	use SoftDeletes;

	public function motorcyclist()
	{
      return $this->belongsTo(Motorcyclist::class,'code','code');
	}
	public function moto_detail()
	{
      return $this->belongsTo(moto_detail::class,'plate_number','plate_number');
	}

}
