<?php
namespace App\Console;
use App\Bpr_response;
use App\Motari_moto;
use App\MotoPay;
use App\Motorcyclist;
use App\Payment;
use App\moto_detail;
use App\Payment_service;
use App\Services\PaymentService;
use GuzzleHttp\Client;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
protected function schedule(Schedule $schedule)
{
// Backups (to Google Drive)
$schedule->command('telescope:prune')->daily();
$schedule->command('backup:clean')->dailyAt('23:15');
$schedule->command('backup:run --only-db')->dailyAt('23:20');
// $schedule->command('inspire')
//          ->hourly();
$schedule->call(function () {   
MotoPay::whereDate('created_at', '<', now()->subHour(5))->delete();
$bprRefNo=NULL;

$queryid=Payment::where('updated_at','>',now()->subHours(4))->whereNotNull('trxId')->where('status','0')->get();
//$queryid=Payment::where('updated_at','>',now()->subHours(20))->whereNotNull('trxId')->where('status','0')->get();
$bprRefNo=NULL;

foreach ($queryid  as $record) {
$trxId=$record->trxId;
$payment_id=$record->id;

$client = new Client();
 $response = $client->get('https://mo.mopay.rw/api/v1/status?token=H4wshnJ5EyqMPjVaU0n4sfiLtifJjgz1&external_transaction_id='.$trxId);

$status_code = json_decode($response->getBody())->status_code;
$momo_ref_number = json_decode($response->getBody())->momo_ref_number;

if(($status_code!='200')AND($status_code!='202'))
Payment::where('trxid',$trxId)->update(['status' => $status_code]);

if($status_code=='200'){ 
  $p=new PaymentService();
  echo $p->__invoke($trxId,$momo_ref_number);
}
            }
       })->everyFiveMinutes();
       //})->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
