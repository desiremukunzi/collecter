<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\Traits\CausesActivity;


class moto_detail extends Model
{
  use SoftDeletes,LogsActivity,CausesActivity;
  
  protected $guarded=['id'];

  protected static $logUnguarded = true;    
  
  protected static $logOnlyDirty = true;

	public function motorcyclist()
	{
      return $this->belongsTo(Motorcyclist::class);
	}

}
