<?php

namespace App\Listeners;

use App\Events\Registered;
use App\Jobs\SendSMS;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class SendRegisteredNotification implements ShouldQueue
{

    /**
     * Handle the event.
     *
     * @param  Registered  $event
     * @return void
     */
    public function handle(Registered $event)
    {
        $roles = $event->user->getRoleNames();

        if ($roles->isEmpty())
            return;

        $role = $roles->first();

        if ($role != 'fee_collector') {
            // send email verification notification
            if ($event->user instanceof MustVerifyEmail && ! $event->user->hasVerifiedEmail()) {
                $event->user->sendEmailVerificationNotification();
            }
        } else {
            // create a pin for the user and send it in sms
            $pin = mt_rand(1000, 9999);

            // save pin first
            $user = $event->user;
            $user->password = bcrypt($pin);
            $user->save();


            $phone_numbers = collect($event->user->telephone);
            $message = 'PIN  : ' . $pin;

            Log::debug( $user->name . ' - ' . $message);

            SendSMS::dispatch($message, $phone_numbers);

        }
    }
}
