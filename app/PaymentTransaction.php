<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\PaymentTransaction
 *
 * @property int $id
 * @property string|null $fee_type_id
 * @property string $name
 * @property string|null $tin_number
 * @property int $amount
 * @property string $province
 * @property string $district
 * @property string $sector
 * @property string $cell
 * @property string $description
 * @property string $telephone
 * @property string|null $user_id
 * @property array|null $request_response_body
 * @property array|null $debit_response_body
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\User|null $doneBy
 * @property-read \App\FeeType|null $feeType
 * @property-read \App\User|null $user
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentTransaction newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentTransaction newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\PaymentTransaction onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentTransaction query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentTransaction whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentTransaction whereCell($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentTransaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentTransaction whereDebitResponseBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentTransaction whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentTransaction whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentTransaction whereDistrict($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentTransaction whereFeeTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentTransaction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentTransaction whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentTransaction whereProvince($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentTransaction whereRequestResponseBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentTransaction whereSector($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentTransaction whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentTransaction whereTelephone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentTransaction whereTinNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentTransaction whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentTransaction whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\PaymentTransaction withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\PaymentTransaction withoutTrashed()
 * @mixin \Eloquent
 */
class PaymentTransaction extends Model
{
    use SoftDeletes;

    protected $fillable = ['fee_type_id','name', 'tin_number', 'amount' ,'province', 'district', 'sector', 'cell', 'description', 'telephone', 'user_id', 'request_response_body', 'debit_response_body', 'status'];
    protected $casts = [ 'request_response_body'  => 'json', 'debit_response_body' => 'json' ];

    public function feeType() {
        return $this->belongsTo(FeeType::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function doneBy() {
        return $this->belongsTo(User::class, 'user_id');
    }

}
