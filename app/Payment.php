<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Payment extends Model
{
	// public function newQuery()
	// {

	//     $query = parent::newQuery();

	//     $query->where('status','1');

	//     return $query;
	// }
	public function cooperative_member()
	{
		return $this->belongsTo(Cooperative_member::class,'code','code');
	}
	public function motorcyclist()
	{
		return $this->belongsTo(Motorcyclist::class,'code','code');
	}
	public function cooperative()
	{
		return $this->belongsTo(Cooperative::class);
	}
	public function cooperativeLocation($district)
	{
		return $this->cooperative_member()->where('district',$district);
	}
	public function motari_moto()
	{
		return $this->belongsTo(Motari_moto::class,'plate_number','plate_number');
	}
	public function payment_service()
	{
		return $this->belongsTo(Payment_service::class,'trxId','trxId');
	}
	
	
}
