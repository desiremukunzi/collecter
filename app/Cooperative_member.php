<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Cooperative_member extends Model
{
	public function payment()
	{
      return $this->hasMany(Payment::class,'code','code');
	}
	public function cooperative()
	{
      return $this->belongsTo(Cooperative::class);
	}
	
}
