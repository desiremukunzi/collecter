<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\RwandaADU
 *
 * @property int $id
 * @property string $province
 * @property string $district
 * @property string $church
 * @property string $cell
 * @property string $village
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RwandaADU newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RwandaADU newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RwandaADU query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RwandaADU whereCell($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RwandaADU whereDistrict($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RwandaADU whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RwandaADU whereProvince($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RwandaADU whereSector($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RwandaADU whereVillage($value)
 * @mixin \Eloquent
 */
class RwandaADU extends Model
{
    protected $table = 'rwanda_adus';
}
