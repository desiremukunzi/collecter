<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Sms_outbox extends Model
{
	use SoftDeletes;
	protected $fillable=['cooperative_id','message','sender','session','characters','recipients'];
	
}
