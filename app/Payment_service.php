<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment_service extends Model
{
	use SoftDeletes;

	protected $guarded=['id'];

	public function payment()
	{
		return $this->belongsTo(Payment::class,'trxId','trxId');
	}
	public function scopeActive($query)
	{
		return $query->where('is_used',0);
	}
}
