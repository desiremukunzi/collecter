<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MotoPay extends Model
{
    protected $table = 'moto_pays';

    public function motorcyclist()
	{
      return $this->belongsTo(Motorcyclist::class,'code','code');
	}
}
