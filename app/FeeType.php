<?php

namespace App;

use App\Traits\UuidModel;
use Askedio\SoftCascade\Traits\SoftCascadeTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\FeeType
 *
 * @property string $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\PaymentTransaction[] $paymentTransactions
 * @property-read int|null $payment_transactions_count
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FeeType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FeeType newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\FeeType onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FeeType query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FeeType uuid($uuid, $first = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FeeType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FeeType whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FeeType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FeeType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\FeeType whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\FeeType withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\FeeType withoutTrashed()
 * @mixin \Eloquent
 */
class FeeType extends Model
{
    use UuidModel, SoftDeletes, SoftCascadeTrait;

    public $incrementing = false;
    protected $casts =  ['id' => 'string'];
    protected $fillable = ['name'];
    protected $softCascade = [ 'paymentTransactions' ];



    public function paymentTransactions() {
        return $this->hasMany(PaymentTransaction::class);
    }
}
