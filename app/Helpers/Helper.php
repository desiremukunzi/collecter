<?php

use App\Bpr_response;
use App\Cooperative;
use App\MotoPay;
use App\Motari_moto;
use App\Motorcyclist;
use App\Payment;
use App\RwandaADU;
use App\Sms_outbox;
use App\Traffic;
use App\moto_detail;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Facades\Http;

function randomTimeLineBadge() {
    $classes =  [ 'kt-list-timeline__badge--success', 'kt-list-timeline__badge--danger', 'kt-list-timeline__badge--warning', 'kt-list-timeline__badge--primary', 'kt-list-timeline__badge--brand' ];
    return $classes[rand(0, 4)];
}

function active_link($name) {

    if (request()->is('country') || request()->is('province/*') || request()->is('district/*') || request()->is('sector/*')  ) {
        if ($name == 'dashboard') {
            return 'kt-menu__item--here';
        }

    }

    if (request()->is('users') || request()->is('users/*')) {
        if ($name == 'users') {
            return 'kt-menu__item--here';
        }
    }
    if (request()->is('attendance') || request()->is('attendance/*')) {
        if ($name == 'attendance') {
            return 'kt-menu__item--here';
        }
    }

    return null;
}


function provinces() {
    return RwandaADU::distinct()->orderBy('province', 'asc')->get(['province as name']);
}

function districts($province) {
    return RwandaADU::where('province', $province)->distinct()->orderBy('district', 'asc')->get(['district as name']);
}
function sectors($district) {
    return RwandaADU::where('district', $district)->distinct()->orderBy('sector', 'asc')->get(['sector as name']);
}
function cells($sector) {
    return RwandaADU::where('sector', $sector)->distinct()->orderBy('cell', 'asc')->get(['cell as name']);
}

function villages($cell) {
    return RwandaADU::where('cell', $cell)->distinct()->orderBy('village', 'asc')->get(['village as name']);
}

function count_members($group,$id)
{
    if($group=="province"){
        $districts=districts($id)->pluck('name');
    $cooperatives=Cooperative::whereIn('district',$districts)->pluck('id');
    return Motorcyclist::whereIn('cooperative_id',$cooperatives)->count();}

    if($group=="district"){
    $cooperatives=Cooperative::where('district',$id)->pluck('id');
    return Motorcyclist::whereIn('cooperative_id',$cooperatives)->count();
}

    if($group=="cooperative_id")
    return Motorcyclist::where($group,$id)->count();

    if($group=="deleted_at")
    $cooperatives=Cooperative::pluck('id');
    return Motorcyclist::whereIn('cooperative_id',$cooperatives)->count();
}
function code()
{
        $res = \App\Motorcyclist::withTrashed()->orderBy('code', 'desc')->limit(1)->value('code');

        $lastKnownID = substr($res,5);
        $newId=$lastKnownID+1;
        $year=date('y');

        return $code=$year.'FED'.$newId;
}
function new_code()
{
        $res = \App\Motorcyclist::withTrashed()->orderBy('new_code', 'desc')->limit(1)->value('new_code');
        return $newId=$res+1;
}
function cooperative_id($name)
{
    return Cooperative::where('name',$name)->value('id');
}
function cooperative_name($id)
{
    return Cooperative::where('id',$id)->value('name');
}
function moto_detail($id,$value)
{
    return moto_detail::where('motorcyclist_id',$id)->value($value);
}
function all_districts() {
    return RwandaADU::distinct()->orderBy('district', 'asc')->get(['district as name']);
}
function all_sectors() {
    return RwandaADU::distinct()->orderBy('sector', 'asc')->get(['sector as name']);
}
function all_cells() {
    return RwandaADU::distinct()->orderBy('cell', 'asc')->get(['cell as name']);
}
function all_villages() {
    return RwandaADU::distinct()->orderBy('village', 'asc')->get(['village as name']);
}
function card_number($cooperative_id)
{
  $query=Motorcyclist::where(['cooperative_id'=>$cooperative_id])
        ->where('card_payed','like','MTC%')
        ->whereNull('card_number');
        //$datas=$query->get();
        //$subquery1=$query->whereNotNull('photo')->get();

        $subquery1 = $query->get()->filter( function($item) {
         if ((file_exists( public_path().'/photo/'.$item->photo)AND($item->photo!=NULL)))
         return true;
         else return false;
        })->count();
        $subquery2 = $query->get()->filter( function($item) {
        return file_exists(public_path().'/photo/public/'.$item->code.'.jpg');
        })->count();

        return $subquery1+$subquery2;
}
function allCard_number()
{

        $query=Motorcyclist::whereNull('card_number')
        ->where('card_payed','like','MTC%');
        //$datas=$query->get();
        //$subquery1=$query->whereNotNull('photo')->get();

        $subquery1 = $query->get()->filter( function($item) {
         if ((file_exists( public_path().'/photo/'.$item->photo)AND($item->photo!=NULL)))
         return true;
         else return false;
        })->count();
        $subquery2 = $query->get()->filter( function($item) {
        return file_exists(public_path().'/photo/public/'.$item->code.'.jpg');
        })->count();

        return $subquery1+$subquery2;


  //return Motorcyclist::whereNull('card_number')->whereNotNull('photo')->count();
}
function sendSMS($numbers, $message, $cooperative_id,$source,$session,$recipients,$characters,$messageOnly)
{
    //$source=\App\Cooperative::where('id',$cooperative_id)->value('name');
    $client = new \GuzzleHttp\Client([
        // Base URI is used with relative requests
        'base_uri' => 'http://api.rmlconnect.net/bulksms/bulksms',
        // You can set any number of default request options.
        'timeout' => 100.0,
    ]);
    $parms = [
        'username' => 'altum',
        'password' => 'VnpRlXSf',
        'type' => 0,
        'dlr' => 1,
        'destination' => "+".validSMSNumber($numbers),
        'source' =>$source ?? 'FERWACOTAMO',
        'message' => $message
    ];

    $uri = "http://api.rmlconnect.net/bulksms/bulksms?" . http_build_query($parms);
    $res = $client->get($uri);
    $resp = $res->getBody()->getContents();

    $cResp = explode("|", $resp);
    if ($cResp[0] == '1701') {
        if(!Sms_outbox::where("session",$session)->exists())
        \App\Sms_outbox::create([
            'sender' => $source,
            'cooperative_id' => $cooperative_id,
            'message' => $messageOnly,
            'session' =>$session,
            'characters' =>$characters,
            'recipients' =>$recipients
        ]);
        return true;
    }
    return false;
}

function validSMSNumber($num)
{
    if (preg_match('[^\+250|250]', $num))
        return trim($num, "+");
    else {
        if ($num[0] == '0')
            return "25" . $num;
        else
            return "250" . $num;
    }


}
function cooperatives()
{
    return Cooperative::orderBy('name','asc')->get();
}
function saveMotoPay($message,$input,$session,$msisdn,$level,$sublevel1)
{
    $MotoPay=new MotoPay();$MotoPay->message=$message;$MotoPay->input=$input;
    $MotoPay->session=$session;$MotoPay->telephone=$msisdn;$MotoPay->level=$level;
    $MotoPay->sublevel1=$sublevel1;$MotoPay->save();

    return true;
}
function UnderMaintenance()
{
    return array(
    "action" =>"FB" ,
    "message"=>"Under Maintenance");
}
function InvalidInput()
{
     return array(
     "action" =>"FB" ,
     "message"=>"Ushyizemo ibidateganyijwe");
}
function Ntabonetse()
{
    return array(
    "action" => "FC", 
    "message" => "Motari ntabonetse, wongere ushyiremo kode neza#*.Ongera");
}
function FC($message)
{
     return array(
     "action" =>"FC" ,
     "message"=>$message);
}
function FB($message)
{
     return array(
     "action" =>"FB" ,
     "message"=>$message);
}
function SaveTraffic($code,$msisdn)
{
   $Traffic=new Traffic();$Traffic->code=$code;$Traffic->msisdn=$msisdn;$Traffic->save();
}
function UpdateTraffic($field,$value,$msisdn)
{
Traffic::where('id',Traffic::where('msisdn',$msisdn)->orderBy('id','desc')->value('id'))->update([$field=>$value]);
}
function Pay2($input,$telephone,$code,$msisdn)
{

                $uri = 'https://motar.mopay.rw/api/v1/payment';
                $client = new Client([
                    'base_uri' => $uri,
                    'timeout' => 120.0,
                ]);

                try {
                    $response = $client->request('POST', $uri,
                        ['headers'=> ['Accept' => 'application/json'],
                         'form_params' => [
                            'amount' => $input,
                            'motari_telephone' => '25'.$telephone,
                            'motari_code' => $code,
                            'client_telephone' => $msisdn,
                            'platform' => 0,
                            // 'source' => 'FMMS',                            

                        ]
                    ]);
                } catch (RequestException $e) {
                    return response()
                        ->json(['error' => 'Payment Sever Error!'], 401);
                } catch (GuzzleException $e) {
                    return response()
                        ->json(['error' => 'Payment Sever Error!'], 401);
                }
                return $body = (string) $response->getBody();
}
function PayMotari($input,$code,$msisdn,$motari_telephone)
{
        //if($msisdn=='250788354222')$input=133;

        $paym=Payment::where('tel',$msisdn)->latest()->take(1)->first();
        $external_transaction_id='MOTARIP'.$paym->id; 
        $paid=$input;
        $msg=" paid ".$paid;
       
        $client = new Client();

        $response=$client->post('https://mo.mopay.rw/api/v2/payment', [
         RequestOptions::JSON=>[
        'token' => 'H4wshnJ5EyqMPjVaU0n4sfiLtifJjgz1',
        'external_transaction_id' => $external_transaction_id,
        'callback_url' => 'https://motari.rw/api/motariMomo',
        'debit' =>[
        'phone_number' => $msisdn,
        'amount' => $input,
        'message' => $msg
        ],
        'transfers' =>  [
            
            [
                //'phone_number'=> '250788354222',
                'phone_number'=> '25'.$motari_telephone,
                'amount'=> $input,
                //'amount'=> $amount-1,
                'message' =>  $msg
            ]
        ],
        ]
    
]);

         $body = (string) $response->getBody();
         $status_code = json_decode($response->getBody())->status_code;
         if($status_code=='202'){
         $momo_ref_number = json_decode($response->getBody())->momo_ref_number;
        Payment::where('id',$paym->id)->whereNotNull('amount')->update(['trxid'=>$external_transaction_id,'transaction'=>$momo_ref_number]);
         }
         return  json_decode($response->getBody())->message;
    }
function PayFee($input,$code,$msisdn,$motari_telephone,$type)
{
        //if($msisdn=='250788354222')$input=133;

        $paym=Payment::where('tel',$msisdn)->latest()->take(1)->first();
        $external_transaction_id='MOTARI'.$paym->id; 
        $paid=$input-132;
        $msg=" paid ".$paid;

        if($type=="Parking")
        {
            $mopay_tel="250786177479";
            $mopay_amount=1000;
            $another_amount=1000;
            //$mopay_amount=50;
            //$another_amount=50;
        }
        if($type!="Parking")
        {
            $mopay_tel="250788354222";
            //$mopay_tel="250781361493";
            //$another_tel="0787476340"
            $mopay_amount=132;
            $another_amount=$input-132;
        }
       
        $client = new Client();

        $response=$client->post('https://mo.mopay.rw/api/v2/payment', [
         RequestOptions::JSON=>[
        'token' => 'H4wshnJ5EyqMPjVaU0n4sfiLtifJjgz1',
        'external_transaction_id' => $external_transaction_id,
        'callback_url' => 'https://motari.rw/api/motariMomo',
        'debit' =>[
        'phone_number' => $msisdn,
        'amount' => $input,
        //'amount' => 100,
        'message' => $msg
        ],
        'transfers' =>  [
            [
                'phone_number'=>$mopay_tel,
                'amount'=> $mopay_amount,
                //'amount'=> 1,
                'message' =>$msg

            ],
            [
                'phone_number'=> '250788354222',
                //'phone_number'=> $motari_telephone,
                'amount'=> $another_amount,
                //'amount'=> $amount-1,
                'message' =>  $msg
            ]
        ],
        ]
    
]);

         $body = (string) $response->getBody();
         $status_code = json_decode($response->getBody())->status_code;
         if($status_code=='202'){
         $momo_ref_number = json_decode($response->getBody())->momo_ref_number;
        Payment::where('id',$paym->id)->whereNotNull('amount')->update(['trxid'=>$external_transaction_id,'transaction'=>$momo_ref_number]);
         }
         return  json_decode($response->getBody())->message;
    }
    function PayService($input,$code,$msisdn,$type)
{
        //if($msisdn=='250788354222')$input=133;
        if($type=="ikarita"){ $prefix="MTC";}
        if($type=="umusanzu"){ $prefix="MTU";}
        if($type=="Kubaruza moto"){ $prefix="MTK";}

        $paym=Payment::where('tel',$msisdn)->latest()->take(1)->first();
        $external_transaction_id=$prefix.$paym->id;         
        $msg=" hishyuwe ".$input." ".$type;

        $client = new Client();

        $response=$client->post('https://mo.mopay.rw/api/v2/payment', [
         RequestOptions::JSON=>[
        'token' => 'H4wshnJ5EyqMPjVaU0n4sfiLtifJjgz1',
        'external_transaction_id' => $external_transaction_id,
        'callback_url' => 'https://motari.rw/api/motariMomo',
        'debit' =>[
        'phone_number' => $msisdn,
        'amount' => $input,
        //'amount' => 100,
        'message' => $msg
        ],
        'transfers' =>  [
            [
                //'phone_number'=>"250788354222",
                'phone_number'=>"250781173787",
                'amount'=> $input,
                //'amount'=> 1,
                'message' =>$msg

            ]
        ],
        ]
    
]);

         $body = (string) $response->getBody();
         $status_code = json_decode($response->getBody())->status_code;
         if($status_code=='202'){
         $momo_ref_number = json_decode($response->getBody())->momo_ref_number;
        Payment::where('id',$paym->id)->whereNotNull('amount')->update(['trxid'=>$external_transaction_id,'transaction'=>$momo_ref_number]);
         }
         return  json_decode($response->getBody())->message;
    }

function creditBpr($amount,$cooperative_account,$description,$payment_id){
$client = new Client();
try {
$response2=$client->post('https://bpr.mopay.rw/api/v1/transaction',[
    RequestOptions::JSON=>[
        'transactionId' => 'FMMS'.$payment_id.'R1',
        'postings' =>  [
            
            [ 
                'account'=> $cooperative_account,
                //'account'=> "409415943110187",
                //'amount'=> $amount,
                'amount'=> 5,
                'narrative' => $description
            ]
            
        ]
    ]
]);

    } catch (\Exception $e) {
        echo $e->getMessage();
    }

    return json_decode($response2->getBody());


}
function countBprFailed(){

$query1=Bpr_response::with('payment')->where('statusCode','!=',200)->count();

$query2=Bpr_response::whereHas('payment',function($q){
    $q->whereNull('bprRefNo');
})->where('statusCode','=',200)->count();

return $query1+$query2;
}
function motarCode($input)
{
    return Motorcyclist::where('code', $input)->orWhere('telephone',$input)->first();
}
function SaveMotariMoto($field,$input){
$motari_code=new Motari_moto();
$motari_code->$field=$input;
$motari_code->save(); 
}
function UpdateMotariMoto($field,$input,$plate_number){
Motari_moto::where('plate_number',$plate_number)->update([$field=>$input]);
}
function OthersRwanda($upperlevel, $level, $session)
{
       $variables=RwandaADU::select($level)->orderBy($level,'asc')->where($upperlevel,Motari_moto::where('plate_number',PlateNumber($session))->value($upperlevel))->distinct()->get();
        $count=1;
        $data2=array();
        foreach ($variables as $key => $variable)
        {
            $data2[]="#".$count.".".$variable->$level;
            $count++;
        }
        return implode($data2);
}
function UpdateProvinceRda($level,$input,$session)
{
    $locations=RwandaADU::select($level)->orderBy($level,'asc')->distinct()->get();
        $count=1;
        foreach ($locations as $key => $location)
        {
           if($input==$count)
           {
            $discovered=$location->$level;
            UpdateMotariMoto($level,$discovered,PlateNumber($session));                 

           }

            $count++;
        }
        return $discovered;

}
function UpdateOthersRda($upperlevel,$level,$input,$session)
{
    $locations=RwandaADU::select($level)->orderBy($level,'asc')
    ->where($upperlevel,Motari_moto::where('plate_number',PlateNumber($session))->value($upperlevel))->distinct()->get();
        $count=1;
        foreach ($locations as $key => $location)
        {
           if($input==$count)
           {
            $discovered=$location->$level;
            UpdateMotariMoto($level,$discovered,PlateNumber($session));                 
           }

            $count++;
        }
        return $discovered;

}
function PlateNumber($session){
return MotoPay::where(['session'=>$session,'level'=>'kMotoOwner'])->latest()->value('input');             
}
function checkMotari($input)
{
    return Motorcyclist::where('code', $input)
    ->orWhere('new_code', $input)
    ->orWhere('telephone', $input);
}
function savingInfo($motari_code,$pin){
$url = config('custom.saving_info');
        //try {
            //Log::debug('Debit URL : ' . $motari_code);

            $response = Http::withHeaders([
                'Authorization' => config('custom.authorization_token'),
                'Accept' => 'application/json',
            ])->get($url,
                [
                'motari_code'=>$motari_code,
                'password'=>$pin
                ]
            );

        // } catch (Throwable $throwable) {
        //     Log::error($throwable->getMessage());
        //     Log::error($throwable->getTraceAsString());
        // }
        return $response;
        //return $data['status'];
}
function withdrawSaving($motari_code,$pin,$amount){
$url = config('custom.saving_withdraw');
        //try {
            $response = Http::withHeaders([
                'Authorization' => config('custom.authorization_token'),
                'Accept' => 'application/json',
            ])->post($url,
                [
                'motari_code'=>$motari_code,
                'password'=>$pin,
                'amount'=>$amount
                ]
            );

        return $response->body();    
}
function resetPin($motari_code,$pin){
$url = config('custom.reset_pin');
        //try {
            $response = Http::withHeaders([
                'Authorization' => config('custom.authorization_token'),
                'Accept' => 'application/json',
            ])->post($url,
                [
                'motari_code'=>$motari_code,
                'password'=>$pin
                ]
            );
        return $response->json()['message'];    
}
function motariCode($msisdn,$session){
    return MotoPay::where(['telephone'=>$msisdn,'session'=>$session,'level'=>'pin'])->latest()->value('sublevel1');
}
function contribution($motari_code,$month){
 $url = 'https://sp.mopay.rw/api/v2/contribution';
        //try {
            $response = Http::withHeaders([
                'Authorization' => config('custom.authorization_token'),
                'Accept' => 'application/json',
            ])->get($url,
                [
                'motari_code'=>$motari_code,
                'month'=>$month
                ]
            );
        return $response->json()['amount'];     
}
