<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;



class Cooperative extends Model
{
	// protected $table='cooperatives';
	use SoftDeletes;
	protected $fillable = [
        'id','name', 'acronym','district','bpr_account'
    ];

	public function cooperative_member()
	{
      return $this->hasMany(Cooperative_member::class);
	}
	public function motorcyclist()
	{
      return $this->hasMany(Motorcyclist::class);
	}
	
}
