<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Bpr_response extends Model
{
		use SoftDeletes;

	public function payment()
	{
		return $this->belongsTo(Payment::class);
	}
}
