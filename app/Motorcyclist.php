<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\Traits\CausesActivity;



class Motorcyclist extends Model
{
use SoftDeletes,LogsActivity,CausesActivity;
  
  protected $guarded=['id'];

  protected static $logUnguarded = true;    
  
  protected static $logOnlyDirty = true;
  
	public function payment()
	{
      return $this->hasMany(Payment::class,'code','code')->where('status',1);
	}
  public function paymentUmusanzu()
  {
      return $this->hasMany(Payment::class,'code','code')->where(['status'=>1,'fee_type'=>'Umusanzu']);
  }
  public function paymentParking()
  {
      return $this->hasMany(Payment::class,'code','code')->where(['status'=>1,'fee_type'=>'Parking']);
  }
	public function moto_detail()
	{
      return $this->hasOne(moto_detail::class);
	}
	public function cooperative()
	{
      return $this->belongsTo(Cooperative::class,'cooperative_id','id');
	}
  public function motari_moto()
  {
      return $this->belongsTo(Motari_moto::class,'code','code');
  }

	public function getFullNameAttribute() {
        return ucfirst($this->first_name) . ' ' . strtoupper($this->last_name);
    }

    public function photoExists()
{
    if (file_exists(public_path().'/photo/public/'.$this->code.'.jpg')) {
        return '/photo/public/'.$this->code.'.jpg';
    }
    else if ((file_exists( public_path().'/photo/'.$this->photo)AND($this->photo!=NULL))) {
        return '/photo/'.$this->photo;
    } else {
        return '/background/avatar.png';
    }     
}
	
}
