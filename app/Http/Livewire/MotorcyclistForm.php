<?php
namespace App\Http\Livewire;

use Livewire\WithFileUploads;
use Livewire\Component;
use App\Motorcyclist;
use App\RwandaADU;
use Illuminate\Support\Facades\Http;

class MotorcyclistForm extends Component
{
use WithFileUploads;

public $cooperative_id;
public $photo;
public $plate_number;
public $code;
public $new_code;
public $name="";
public $telephone;
public $province;
public $district;
public $sector;
public $cell;
public $village;
public $zone;
public $id_number=0;
public $permit_number;
public $share;
public $gender;
public $owner="Njyewe";
public $nyirimoto_names;
public $nyirimoto_telephone;
public $province2;
public $district2;
public $sector2;
public $cell2;
public $village2;
public $success=false;

public $distr_owner=["name"=>"--select--"];
public $sector_owner=["name"=>"--select--"];
public $cell_owner=["name"=>"--select--"];
public $village_owner=["name"=>"--select--"];

public $distr_owner2=["name"=>"--select--"];
public $sector_owner2=["name"=>"--select--"];
public $cell_owner2=["name"=>"--select--"];
public $village_owner2=["name"=>"--select--"];


protected $rules=
[
//'cooperative_id' => 'required',
'photo' => 'image',
'code'=>'string|size:10',
'plate_number'=>'string|size:6',
'new_code'=>'numeric|size:5',
'name'=>'regex:/^[\pL\s\-]+$/u|min:3',
'district'=>'string',
'sector'=>'string',
'cell'=>'string',
'village'=>'string',
'zone'=>'alpha',
'share'=>'numeric',
'gender'=>'in:Male,Female',
'owner'=>'in:Sinjye,Njyewe',
'nyirimoto_telephone'=>'regex:/07[9,8,2,3]{1}[0-9]{7}/',
'province2'=>'string',
'sector2'=>'string',
'cell2'=>'string',
'village2'=>'string',

'telephone' => 'required|unique:motorcyclists,telephone,NULL,id,deleted_at,NULL|regex:/07[9,8,2,3]{1}[0-9]{7}/',
'id_number'=>'nullable|size:16|unique:motorcyclists,id_number,NULL,id,deleted_at,NULL',       
'permit_number'=>'nullable|size:16|unique:motorcyclists,permit_number,NULL,id,deleted_at,NULL', 
'nyirimoto_names'=>'nullable|regex:/^[\pL\s\-]+$/u|min:3|required_if:owner,Sinjye',
];
protected $messages= 
[
'name.min' => 'amazina nibura agomba kuba arengeje inyuguti 3',               
'telephone.regex' => 'nimero ya telefone ntimeze neza',               
'telephone.unique' => 'iyi telefone isanzwe muri system',               
'id_number.unique' => 'iyi indangamuntu isanzwe muri system',               
'permit_number.unique' => 'iyi perimi isanzwe muri system',               
'id_number.size' => 'indangamuntu ntimeze neza',               
'permit_number.regex' => 'perimi ntimeze neza',
'nyirimoto_names.required_if' => 'niba moto atari iyawe umwirondoro wa nyirimoto urakenewe',               
];

	public function mount()
	{
        $this->code=code();
        $this->new_code=new_code();
		//$this->motorcyclist=new Motorcyclist();
        //$this->distr_owner2=["name"=>"--elect--"];      
        //$this->district2=$district2;  
        $this->cooperative_id=request()->segment(2);

    
	}
    public function render()
    {
        //dd($this->cooperative_id);
        if($this->province){
        $this->distr_owner=RwandaADU::where('province', $this->province)->distinct()->orderBy('district', 'asc')->get(['district as name']);   
        $this->sector_owner=RwandaADU::where('district',$this->district ?? $this->distr_owner->first()->name)->distinct()->orderBy('sector', 'asc')->get(['sector as name']);  
        $this->cell_owner=RwandaADU::where('sector',$this->sector ?? $this->sector_owner->first()->name)->distinct()->orderBy('cell', 'asc')->get(['cell as name']); 
        $this->village_owner=RwandaADU::where('cell',$this->cell ?? $this->cell_owner->first()->name)->distinct()->orderBy('village', 'asc')->get(['village as name']); 
        }
        if($this->district){       
        $this->sector_owner=RwandaADU::where('district', $this->district)->distinct()->orderBy('sector', 'asc')->get(['sector as name']);
        $this->cell_owner=RwandaADU::where('sector',$this->sector ?? $this->sector_owner->first()->name)->distinct()->orderBy('cell', 'asc')->get(['cell as name']); 
        $this->village_owner=RwandaADU::where('cell',$this->cell ?? $this->cell_owner->first()->name)->distinct()->orderBy('village', 'asc')->get(['village as name']);
        }
       if($this->sector){
        $this->cell_owner=RwandaADU::where('sector', $this->sector)->distinct()->orderBy('cell', 'asc')->get(['cell as name']);
         $this->village_owner=RwandaADU::where('cell',$this->cell ?? $this->cell_owner->first()->name)->distinct()->orderBy('village', 'asc')->get(['village as name']);
       }
       if($this->cell || $this->village)
        $this->village_owner=RwandaADU::where('cell', $this->cell ?? $this->cell_owner->first()->name)->distinct()->orderBy('village', 'asc')->get(['village as name']);

    if($this->province2){
        $this->distr_owner2=RwandaADU::where('province', $this->province2)->distinct()->orderBy('district', 'asc')->get(['district as name']);   
        $this->sector_owner2=RwandaADU::where('district',$this->district2 ?? $this->distr_owner2->first()->name)->distinct()->orderBy('sector', 'asc')->get(['sector as name']);  
        $this->cell_owner2=RwandaADU::where('sector',$this->sector2 ?? $this->sector_owner2->first()->name)->distinct()->orderBy('cell', 'asc')->get(['cell as name']); 
        $this->village_owner2=RwandaADU::where('cell',$this->cell2 ?? $this->cell_owner2->first()->name)->distinct()->orderBy('village', 'asc')->get(['village as name']); 
        }
        if($this->district2){       
        $this->sector_owner2=RwandaADU::where('district', $this->district2)->distinct()->orderBy('sector', 'asc')->get(['sector as name']);
        $this->cell_owner2=RwandaADU::where('sector',$this->sector2 ?? $this->sector_owner2->first()->name)->distinct()->orderBy('cell', 'asc')->get(['cell as name']); 
        $this->village_owner2=RwandaADU::where('cell',$this->cell2 ?? $this->cell_owner2->first()->name)->distinct()->orderBy('village', 'asc')->get(['village as name']);
        }
       if($this->sector2){
        $this->cell_owner2=RwandaADU::where('sector', $this->sector2)->distinct()->orderBy('cell', 'asc')->get(['cell as name']);
         $this->village_owner2=RwandaADU::where('cell',$this->cell2 ?? $this->cell_owner2->first()->name)->distinct()->orderBy('village', 'asc')->get(['village as name']);
       }
       if($this->cell2 || $this->village2)
        $this->village_owner2=RwandaADU::where('cell', $this->cell2 ?? $this->cell_owner2->first()->name)->distinct()->orderBy('village', 'asc')->get(['village as name']);
      

        return view('livewire.motorcyclist-form');
    }
    public function saveMotorcyclist()
    {
        $fileName=NULL;
        $ifoto=$this->photo;
        if($ifoto){
        //$ext=$ifoto->getClientOriginalExtension(); 
        $fileName=code().'.'.'jpg';       
        $ifoto->move('photo/',$fileName);
        }
    	$validatedData = $this->validated();
        Motorcyclist::create($validatedData,['photo'=>$fileName]);
        $this->success=true;

        //return redirect()->route('cooperatives',$this->cooperative_id)->with('success', 'yabitswe neza!!');    
        
    }
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName,$this->rules);
    }
    // public function updatingCooperative_id($value)
    // {
    //     $this->cooperative_id=$value;
    // }
    public function updatingTelephone($value)
    {
    $NF='Not Found';
    try{
    $response =Http::get('https://mo.mopay.rw/api/v1/person?msisdn='.'25'.$value);
    $this->name = json_decode($response->getBody())->lastName.' '.json_decode($response->getBody())->firstName;

    $response2 =Http::get('https://mo.mopay.rw/api/v1/person/id?msisdn='.'25'.$value);
    $this->id_number = json_decode($response2->getBody())->nid;
    }   
    catch(\Exception $e){        

    } 
   

    // $this->name = "Des Muk";
    // $this->id_number = "11";
    }
    public function updatedProvince($value)
    {
        $this->distr_owner=RwandaADU::where('province', $value)->distinct()->orderBy('district', 'asc')->get(['district as name']);
        $this->district=$this->distr_owner->first()->name;
    }
    public function updatedDistrict($value)
    {        
        $this->district=$value;
        $this->sector_owner=RwandaADU::where('district', $value)->distinct()->orderBy('sector', 'asc')->get(['sector as name']);
        $this->sector=$this->sector_owner->first()->name;
    }
    public function updatedSector($value)
    {
        $this->sector=$value;
        $this->cell_owner=RwandaADU::where('sector', $value)->distinct()->orderBy('cell', 'asc')->get(['cell as name']);
        $this->cell=$this->cell_owner->first()->name;

    }
    public function updatedCell($value)
    {
       
        $this->cell=$value;
        $this->village_owner=RwandaADU::where('cell', $value)->distinct()->orderBy('village', 'asc')->get(['village as name']);
        $this->village=$this->village_owner->first()->name;

    }
    public function updatedVillage($value)
    {
        $this->village=$value;
    }

    public function updatedProvince2($value)
    {
        $this->distr_owner2=RwandaADU::where('province', $value)->distinct()->orderBy('district', 'asc')->get(['district as name']);
        $this->district2=$this->distr_owner2->first()->name;
    }
    public function updatedDistrict2($value)
    {        
        $this->district2=$value;
        $this->sector_owner2=RwandaADU::where('district', $value)->distinct()->orderBy('sector', 'asc')->get(['sector as name']);
        $this->sector2=$this->sector_owner2->first()->name;
    }
    public function updatedSector2($value)
    {
        $this->sector2=$value;
        $this->cell_owner2=RwandaADU::where('sector', $value)->distinct()->orderBy('cell', 'asc')->get(['cell as name']);
        $this->cell2=$this->cell_owner2->first()->name;

    }
    public function updatedCell2($value)
    {
       
        $this->cell2=$value;
        $this->village_owner2=RwandaADU::where('cell', $value)->distinct()->orderBy('village', 'asc')->get(['village as name']);
        $this->village2=$this->village_owner2->first()->name;

    }
    public function updatedVillage2($value)
    {
        $this->village2=$value;
    }
}
