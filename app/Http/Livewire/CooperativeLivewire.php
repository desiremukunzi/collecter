<?php

namespace App\Http\Livewire;

use App\Cooperative;
use Livewire\Component;

class CooperativeLivewire extends Component
{
public $i          = 1;
public $suppliers  = [];
public $test;
public $inputsearchsupplier = '';
public $supplier_id;

public function selectsupplier($supplier_id, $supplier_name)
{
	echo "string";
	$this->inputsearchsupplier="";
	$this->supplier_id = $supplier_id;
	return $this->inputsearchsupplier=$supplier_name;
	$this->test=$supplier_name;
}
 public function render()
       {

    	$searchsuppliers = [];
    	$this->suppliers=Cooperative::all();

    	if(strlen($this->inputsearchsupplier)>=2){
    		$this->suppliers= Cooperative::where('name', 'LIKE' , '%'.$this->inputsearchsupplier.'%')->get();
    	}

    	return view('livewire.cooperative-livewire')->with(['suppliers' => $this->suppliers,'searchsuppliers'=> $this->suppliers]);

    }

    // public function render()
    // {
    //     return view('livewire.cooperative-livewire');
    // }
}

