<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Cooperative_member;
use App\User;

class Codesearch extends Component
{
	public $query;

	public $code;

	public function mount()
	{
		$this->query='';

		$this->code=[];
	}

	public function updatedQuery()
	{
		$this->code=User::where('name','like','%'.$this->query.'%')->get();
	}
    public function render()
    {
        return view('livewire.codesearch');
    }
}
