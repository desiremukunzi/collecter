<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required'],
            'role' => ['required'],
            'password' => ['required','min:6','confirmed'],
            //'retype_password' => ['required','confirm'],
            'email' => ['required','email','unique:users'],
            'telephone' => ['required','regex:/07[8,2,3]{1}[0-9]{7}/','unique:users'],

            // 'email' => [Rule::requiredIf(function () {
            //     return request()->get('role') == 'admin'
            //         || request()->get('role') == 'regional_coordinator'
            //         || request()->get('role') == 'district_leader';
            // }), Rule::unique('users')->where(function ($query) {
            //     return $query->whereNotNull('email');
            // }) ],

            // 'telephone' => [Rule::requiredIf(function () {
            //     return request()->get('role') == 'fee_collector';
            // }), Rule::unique('users')->where(function ($query) {
            //     return $query->whereNotNull('telephone');
            // })],

            // 'province' => Rule::requiredIf(function () {
            //     return request()->get('role') == 'regional_coordinator'
            //         || request()->get('role') == 'district_leader'
            //         || request()->get('role') == 'fee_collector';
            // }),

            // 'district' => Rule::requiredIf(function () {
            //     return request()->get('role') == 'district_leader'
            //         || request()->get('role') == 'fee_collector';
            // }),

            // 'sector' => Rule::requiredIf(function () {
            //     return request()->get('role') == 'fee_collector';
            // }),

        ];
    }
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'email.required' => 'E-mail field is required',
            'telephone.required' => 'Telephone field is required',
            // 'province.required' => 'Province field is required when the user role is regional coordinator, district leader or fee collector',
            // 'district.required' => 'District field is required when the user role is district leader or fee collector',
            // 'sector.required' => 'Sector field is required when the user role is fee collector',
        ];
    }
}
