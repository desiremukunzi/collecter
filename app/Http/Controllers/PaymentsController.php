<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Bpr_response;
use App\Motari_moto;
use App\Motorcyclist;
use App\Payment;
use App\moto_detail;
use App\Payment_service;
use App\Payment_control;
use App\Services\PaymentService;

class PaymentsController extends Controller
{    
public function momo(Request $request) {
$external_transaction_id = $request->get('external_transaction_id');
$momo_ref_number = $request->get('momo_ref_number');
$status_code = $request->get('status_code');
$bprRefNo=NULL;
//$status_code = $data['status_code'];        
             
if($status_code==200){
	$p=new PaymentService();
	$p->__invoke($external_transaction_id,$momo_ref_number);
}
}
public function missedCardPayment( Request $request)
{
$motari_code=$request->code;
$trxId=$request->trxId;
if(Payment_control::Where('card_payment',1)->exists()){
request()->validate([
'code'=>'exists:motorcyclists|required',
'trxId'=>'exists:payments|required'
],
[
    'code.exists'=>'iyi code ntiri mu bubiko bwacu',
    'trxId.exists'=>'transaction Id ntibonetse,ubu bwishyu ntabwabayeho',
]
);
 
if(Payment::where('trxId',$trxId)
             ->whereHas('payment_service',function($q){
             $q->where('is_used',1);
          })->exists())
return back()->with('error','transaction Id yarakoreshejwe');
}
$update=Motorcyclist::where('code',$motari_code)->update(['card_payed'=>$request->trxId]);
if($update)
return back()->with('success','well saved');
else return back()->with('error','not saved');

}
}


