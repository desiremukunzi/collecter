<?php

namespace App\Http\Controllers;

use App\fee_type;
use Illuminate\Http\Request;

class FeeTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\fee_type  $fee_type
     * @return \Illuminate\Http\Response
     */
    public function show(fee_type $fee_type)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\fee_type  $fee_type
     * @return \Illuminate\Http\Response
     */
    public function edit(fee_type $fee_type)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\fee_type  $fee_type
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, fee_type $fee_type)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\fee_type  $fee_type
     * @return \Illuminate\Http\Response
     */
    public function destroy(fee_type $fee_type)
    {
        //
    }
}
