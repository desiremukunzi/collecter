<?php

namespace App\Http\Controllers;

use App\payment_transactions;
use Illuminate\Http\Request;

class PaymentTransactionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\payment_transactions  $payment_transactions
     * @return \Illuminate\Http\Response
     */
    public function show(payment_transactions $payment_transactions)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\payment_transactions  $payment_transactions
     * @return \Illuminate\Http\Response
     */
    public function edit(payment_transactions $payment_transactions)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\payment_transactions  $payment_transactions
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, payment_transactions $payment_transactions)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\payment_transactions  $payment_transactions
     * @return \Illuminate\Http\Response
     */
    public function destroy(payment_transactions $payment_transactions)
    {
        //
    }
}
