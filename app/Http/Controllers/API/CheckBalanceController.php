<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Cooperative_member;
use Illuminate\Http\Request;
use DB;
use App\Payment;
use DateTime;

class CheckBalanceController extends Controller
{
	public function index($id){
    
    $amounts=Payment::where(['code'=>$id,'status'=>'1'])->sum('amount');


    //$createdAt=Cooperative_member::where('code',$id)->value('created_at');
    
    $createdDate = new DateTime('2020-01-26');
    $first = new DateTime('2020-01-26');
 
    if($createdDate>$first)
    $first=$createdDate;

    $second = new DateTime(date('y-m-d'));
    $weeks=floor($first->diff($second)->days/7);
    //return $date1.$date2;
    $sofar=$weeks*500;
    $debt=$sofar-$amounts;

    return $debt;   

	}	

}
