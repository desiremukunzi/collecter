<?php

namespace App\Http\Controllers\API;

use App\PaymentTransaction;
use App\User;
//use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AndroidController extends Controller
{
    public function login(Request $request) {
        $username = $request->get('username');
        $password = $request->get('password');


        $user = User::where('telephone', $username)->first();

        if ($user && \Hash::check($password, $user->password)) {

            // TODO : check if deployment is full to sector level

            return response()
                ->json(
                    [
                        'status' => 0,
                        'user' => [
                            'id' => $user->id,
                            'name' => $user->name,
                            'deployment' => $user->district . ', ' . $user->sector,
                            'telephone' => $user->telephone
                        ]
                    ]
                );
        }

        return response()
            ->json(['status' => 2, 'message' => 'Ntitubashije kubamenya!']);

    }

    public function payment(Request $request) {
        // $fee_category = $request->get('fee_category');
        // $name = $request->get('name');
        // $tin_number = $request->get('tin_number');
        // $description = $request->get('description');
        // $amount = $request->get('amount');
        // $telephone = $request->get('phone_number');
        // $user_id = $request->get('operator');

        // // TODO : Check fee category
        // // TODO : check amount
        // // TODO : validate telephone
        // // TODO : Check operator
        // // TODO : Check empty fields if necessary

        // if ($amount > 3000 ) {
        //     $amount += 100;
        // }

        // $user = User::whereId($user_id)->first();

        // if (!$user) {
        //     return response()->json(['status' => 2, 'message' => 'Ntitubashije kubamenya!']);
        // }

        // // create a transaction
        // $transaction = PaymentTransaction::create(
        //     [
        //         'fee_type_id' => $fee_category,
        //         'name' => $name,
        //         'tin_number' => $tin_number,
        //         'amount' => $amount ,
        //         'province' => $user->province,
        //         'district' => $user->district,
        //         'sector' => $user->sector,
        //         'description' => $description,
        //         'telephone' => $telephone,
        //         'user_id' => $user_id
        //     ]
        // );

        $uri = 'https://novapay.rw/api/v1/novapay/initialize-payment';

        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => $uri,
            // You can set any number of default request options.
            'timeout' => 120.0,
        ]);

        try {
            // make the request
            $response = $client->request('POST', $uri, [
                'form_params' => [
                    'token' => 'tRtsoR0ujXpcPLhr474Pf52KSPzChrs1',
                    'amount' => 10,
                    'msisdn' => "0788354222",
                    'receiver_msisdn' => "0788354222",
                    'external_transaction_id' => "2",
                    'from_msg' => " ",
                    'to_msg' => " "
                ]
            ]);
        } catch (RequestException $e) {
            return response()
                ->json(['error' => 'Payment Sever Error!'], 401);
        } catch (GuzzleException $e) {
            return response()
                ->json(['error' => 'Payment Sever Error!'], 401);
        }

        $body = (string) $response->getBody();
        $action = json_decode($response->getBody())->action;
        $message = json_decode($response->getBody())->message;

        $transaction->update(
            [
                'request_response_body' => json_decode($body, true),
            ]
        );


        if ($action != 200) {
            return response()
                ->json(['status' => 2, 'message'  => $message]);
        }

        return response()
            ->json(['status' => 0, 'message'  => $message]);
    }

    public function check(Request $request) {
        $reference_number = $request->get('ref_number');

        $transactions = PaymentTransaction::where('tin_number', $reference_number)->with(['feeType'])->latest()->get();

        // if empty search using phone number
        if ($transactions->isEmpty()) {
            $transactions = PaymentTransaction::where('telephone', $reference_number)->with(['feeType'])->latest()->get();
        }

        $mappedTransactions = $transactions->map(function ($transaction) {
            return [
                'type' => $transaction->feeType ? $transaction->feeType->name : null,
                'amount' => number_format($transaction->amount) . ' Rwf',
                'location' => $transaction->district . ', ' . $transaction->sector,
                'description' => $transaction->description,
                'done_at' => $transaction->created_at->format('d/m/Y H:i')
            ];
        });

        return response()
            ->json(['status' => 0, 'data' => $mappedTransactions]);

    }

    public function balance(Request $request) {
        $user_id = $request->get('operator');

        $balance = PaymentTransaction::where('user_id', $user_id)
                                        ->whereDate('created_at', date('Y-m-d'))
                                        ->where('status', 2)
                                        ->sum('amount');

        return response()
            ->json(['status' => 0, 'balance' => number_format($balance) . ' Rwf']);
    }

    public function paymentResponse(Request $request) {
        $transaction_id = $request->get('external_transaction_id');
        $status = $request->get('status');

        $transaction = PaymentTransaction::find($transaction_id);

        if (!$transaction)
            abort(500, 'No Transaction Found');

        if ($transaction->status > 0)
            abort(500, 'Hacked');

        $transaction->update(
            [
                'debit_response_body' => $request->toArray(),
                'status' => $status == 2 ? 2 : 1,
            ]
        );

        return [ 'status' => true ];
    }
}











