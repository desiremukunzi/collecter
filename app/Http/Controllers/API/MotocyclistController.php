<?php

namespace App\Http\Controllers\Api;

use App\Cooperative;
use App\Http\Controllers\Controller;
use App\Motorcyclist;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;





class MotocyclistController extends Controller
{
	public function index($id){
    
    //$Motocyclists=Motorcyclist::where('code',$id)->first();

$uri = 'http://mopay.rw/api/motari-app/get-info/?keyword='.$id;

        $client = new Client();
        $response = $client->request('GET', $uri, [
    'keyword' => $id
]);
        $Motocyclists = json_decode($response->getBody());

     if($Motocyclists)
     return response()->json($Motocyclists);
     else
     return response()->json('Not Found');

	}

    public function motari($id)
    { 
        $motar=Motorcyclist::where('code',$id)
        ->orWhere('card_number',$id)
        ->orWhere('new_code',$id)
        ->orWhere('id_number',$id)
        ->orWhere('permit_number',$id)
        ->orWhere('vest_number',$id)
        ->orWhere('telephone',ltrim($id,'25'))
        ->orWhereHas('moto_detail',function($query) use ($id)
        {
             return $query->where('plate_number',$id);
        })->first();

        if (!empty($motar)) {           
//1004991819 3051266574
    $data = array(
    "id" =>$motar->id ,
    "photo_url"=>"https://motari.rw".$motar->photoExists(),
    "photo_nid"=>"https://motari.rw/photo/national_id/".$motar->file_nid,
    "photo_pid"=>"https://motari.rw/photo/permit_number/".$motar->file_pid,
    "cooperative"=>$motar->cooperative->name ?? '-',
    "cooperative_id"=>$motar->cooperative->id ?? '-',
    "cooperative_acronym"=>$motar->cooperative->acronym ?? '-',
    "code"=>$motar->code,
    "mtn_code"=>$motar->new_code,
    "name"=>$motar->name,
    "telephone"=>$motar->telephone,
    "card"=>$motar->card_number,
    "id_number"=>$motar->id_number,
    "permit_number"=>$motar->permit_number,
    "share"=>$motar->share,
    "gender"=>$motar->gender,
    "address"=>$motar->province.','.$motar->district.','.$motar->sector.','.$motar->cell.','.$motar->village,    
    "zone"=>$motar->zone,
    "owner"=>$motar->owner,
    "plate_number"=>$motar->moto_detail->plate_number ?? '-',
    "vest_number"=>$motar->vest_number,
    "vest_time"=>$motar->vest_time,
    "owner_name"=>$motar->moto_detail->name ?? '-',
    "owner_telephone"=>$motar->moto_detail->telephone ?? '-',
    "owner_address"=>optional($motar->moto_detail)->province.','.optional($motar->moto_detail)->district.','.optional($motar->moto_detail)->cell.','.optional($motar->moto_detail)->village);
    return response()->json($data);
    }
    else
    return response()->json('Not Found');
    }

    public function motari_equity(Request $request)
    {
    $motar=Motorcyclist::where('code',$request->motorcyclistId)->first();
    if (!empty($motar)) {           
    //1004991819 3051266574
    $data = array(
    "motorcyclistId"=>$motar->code, 
    "name"=>$motar->name,
    "NID"=>$motar->id_number,
    //"plate_number"=>$motar->moto_detail->first()->plate_number ?? '-',
    "bankAccount"=>$motar->equity_account ?? '-',
    "location"=>$motar->district, 
    "cooperative"=>$motar->cooperative->name ?? '-',
    //"card_number"=>$motar->card_number,
    "telephone"=>$motar->telephone,
    "requestDate"=>date('Y-m-d'));   

    
    return response()->json($data);
    }
    else
    return response()->json('Not Found');
    }    

    public function update($id,$vest,$airtel)
    {
        $data0 = array(
            "vest" => $vest,
            "airtel" => $airtel,
        );
            $validator = Validator::make($data0, [
                'vest' => 'string|min:4|max:10',
                'airtel' => 'regex:/07[2,3]{1}[0-9]{7}/',
            ]);
            if ($validator->fails())
            return response()->json('Vest or phone Number not valid');

        $motar=Motorcyclist::where('code',$id);
        $vest_time=$motar->value('vest_time');
        if(!$motar->exists())
            return response()->json('not found');
        if($vest_time>now()->subMonths(6))
            return response()->json('Motari yayifashe vuba');
          
          $update=Motorcyclist::where('code',$id)->update(['vest_number'=>$vest,'airtel_number'=>$airtel,'vest_time'=>now()]);

        if($update)  return response()->json('well updated');
        else return response()->json('not updated');
    }

    public function info(Request $request)
    {
        $id=$request->identifier;

        if(!empty($id))
        Session::put('id',$id);
        
        if(empty($id))
        $id=Session::get('id');
    

        $datas=Motorcyclist::where('code',$id)
        ->orWhere('card_number',$id)
        ->orWhere('id_number',$id)
        ->orWhere('permit_number',$id)
        ->orWhere('vest_number',$id)
        ->orWhere('telephone',$id)->orWhereHas('moto_detail',function($query) use ($id)
        {
             return $query->where('plate_number',$id);
        })->first();

        if($datas){              
       return view('pages.info',compact('datas')); 
        }
        else
        return redirect()->back()->with('error','ntabwo iri mu bubiko bwacu');
    }

    public function update_files(Request $request)
    {
        $ifoto=$request->file('file');
        $nid=$request->file('nid');
        $pid=$request->file('pid');
        $code=$request->code;
        
        if($ifoto){
        //$ext=$ifoto->getClientOriginalExtension(); 
        $fileName=$code.'.jpg'; 

        $image_path=public_path().'/photo/'.$fileName;        
        if (file_exists($image_path))
        unlink($image_path);

        // $fileName=$ifoto->getClientOriginalName();     
        $ifoto->move('photo/',$fileName);
        $photo=$fileName;
        $update=Motorcyclist::where('code',$code)->update(['photo'=>$photo]);
        }
        if($nid){
        //$ext=$ifoto->getClientOriginalExtension(); 
        $nidName=$code.'NID.jpg';    
        
        $image_path_nid=public_path().'/photo/national_id/'.$nidName;        
        if (file_exists($image_path_nid))
        unlink($image_path_nid);

        $nid->move('photo/national_id/',$nidName);
        $updateNid=Motorcyclist::where('code',$code)->update(['file_nid'=>$nidName]);
        }
        if($pid){
        //$ext=$ifoto->getClientOriginalExtension(); 
        $pidName=$code.'PID.jpg';      
        
        $image_path_pid=public_path().'/photo/permit_number/'.$pidName;        
        if (file_exists($image_path_pid))
        unlink($image_path_pid);  

        $pid->move('photo/permit_number/',$pidName); 
        //$test=implode(" ",$pid);      
        $updatePid=Motorcyclist::where('code',$code)->update(['file_pid'=>$pidName]);
      }
        
        if ($updatePid) 
        return response()->json('well updated');
        else return response()->json('not updated');
        
}  
      public function updateTel(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'telephone'=>'required|regex:/07[8]{1}[0-9]{7}/',    
            'code'=>'required'
        ]);
      if($validator->fails())
      return response()->json($validator->errors());

        try{
          $update=Motorcyclist::where('code',$request->code)->update(['telephone'=>$request->telephone]);

        if($update)  return response()->json('well updated');
        else return response()->json('not updated');

        } catch(\Illuminate\Database\QueryException $ex){ 
          return response()->json($ex->getMessage());
        }


    }

    public function cooperativesMembers()
    {
        return Cooperative::with('motorcyclist')->first();
    }
    public function cooperatives()
    {
        $data=Cooperative::select('id','acronym','name','district')->get();
        return response()
        ->json($data);
       

    }
}
