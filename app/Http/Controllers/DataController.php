<?php

namespace App\Http\Controllers;

use App\RwandaADU;
use Illuminate\Http\Request;

class DataController extends Controller
{
    public function getDistricts(Request $request) {
        $province = $request->get('province');
        return districts($province);
    }

    public function getSectors(Request $request) {
        $district = $request->get('district');
        return  sectors($district);
    }
    public function getCells(Request $request) {
        $sector = $request->get('sector');
        return cells($sector);
    }

    public function getVillages(Request $request) {
        $cell = $request->get('cell');
        return  villages($cell);
    }
}
