<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Redirect;
use App\Cooperative_member;
use App\Motorcyclist;
use App\moto_detail;
use App\Payment_service;
use App\Payment;
use App\Payment_control;

class MotorcyclistController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function store(Request $request)
    {    
    if(Auth::user()->role!=1)
    return redirect()->back()->with('error', 'Ntiwemerewe kongeramo Motari'); 
    
request()->validate([
'photo' => 'image',
'plate_number'=>'string|size:6',
'cooperative_id' => 'required',
'zone'=>'alpha',
'telephone' => 'required|unique:motorcyclists,telephone,NULL,id,deleted_at,NULL|regex:/07[9,8,2,3]{1}[0-9]{7}/',
'id_number'=>'nullable|size:16|unique:motorcyclists,id_number,NULL,id,deleted_at,NULL',       
'permit_number'=>'nullable|size:16|unique:motorcyclists,permit_number,NULL,id,deleted_at,NULL', 
'nyirimoto_names'=>'nullable|required_if:owner,Sinjye',
            ], [
'telephone.regex' => 'nimero ya telefone ntimeze neza',               
'telephone.unique' => 'iyi telefone isanzwe muri system',               
'id_number.unique' => 'iyi indangamuntu isanzwe muri system',               
'permit_number.unique' => 'iyi perimi isanzwe muri system',               
'id_number.size' => 'indangamuntu ntimeze neza',               
'permit_number.regex' => 'perimi ntimeze neza',             
              
'nyirimoto_names.required_if' => 'niba moto atari iyawe umwirondoro wa nyirimoto urakenewe',               
]);


        $member=new Motorcyclist();
  
        $ifoto=$request->file('photo');
        if($ifoto){
        //$ext=$ifoto->getClientOriginalExtension(); 
        $fileName=code().'.'.'jpg';       
        $ifoto->move('photo/',$fileName);

        $member->photo=$fileName;
        }

        $member->cooperative_id=$cooperative_id=$request->cooperative_id;
        //return request()->all();
        $member->code=code();
        $member->new_code=new_code();
        $member->name=$names=$request->name;
        $member->telephone=$telephone=$request->telephone;
        $member->province=$province=$request->province;
        $member->district=$district=$request->district;
        $member->sector=$sector=$request->sector;
        $member->cell=$cell=$request->cell;
        $member->village=$village=$request->village;
        $member->zone=$request->zone;
        $member->id_number=$request->id_number;
        $member->permit_number=$request->permit_number;
        $member->share=$request->share;
        $member->gender=$request->gender;
        $member->owner=$nyirimoto=$request->owner;

        $member->save();

        $motorcyclist_id=Motorcyclist::orderBy('id','desc')->limit(1)->value('id');
        moto_detail::where('plate_number',$request->plate_number)->delete(); 

        $owner=new moto_detail();
        $owner->plate_number=$request->plate_number;
        if($nyirimoto=="Njyewe"){
        $owner->name=$names;
        $owner->telephone=$telephone;
        $owner->province=$province;
        $owner->district=$district;
        $owner->sector=$sector;
        $owner->cell=$cell;
        $owner->village=$village;
        }
        else{
        $owner->name=$request->nyirimoto_names ?? '-';
        $owner->telephone=$request->nyirimoto_telephone;
        $owner->province=$request->province2;
        $owner->district=$request->district2;
        $owner->sector=$request->sector2;
        $owner->cell=$request->cell2;
        $owner->village=$request->village2;
        }
        $owner->motorcyclist_id=$motorcyclist_id;
          

        $owner->save();

        return redirect()->route('cooperatives',$cooperative_id)->with('success', 'yabitswe neza!!');    
        }

        public function update(Request $request)
    {
request()->validate([
'telephone' => 'required|regex:/07[9,8,2,3]{1}[0-9]{7}/',
'id_number'=>'nullable|size:16',       
'permit_number'=>'nullable|size:16', 
'nyirimoto_names'=>'nullable|required_if:owner,Sinjye',
            ], [
'telephone.regex' => 'nimero ya telefone ntimeze neza',               
'id_number.size' => 'indangamuntu ntimeze neza',               
'permit_number.size' => 'perimi ntimeze neza', 
'nyirimoto_names.required_if' => 'niba moto atari iyawe amazina ya nyirimoto arakenewe',               
]);

        $id=$request->id;
        $member=$before=Motorcyclist::find($id);
        $motari_code=$member->code;
        $plate_before=$member->moto_detail->plate_number ?? '';
        $card_number_before=$member->card_number ?? '';
        $plate_request=$request->plate_number;
        $card_number_request=$request->card_number;
        
        if(($plate_before!=$plate_request)&&
            (!empty($card_number_request))&&
            (Payment_control::Where('kubaruza_moto_payment',1)->exists()))
        {      
        $plate_b=$plate_before;
        $plate_a=$plate_request;  
        if($this->checkPayment($motari_code)==false)
        return back()->with('error','motari ntarishyura');
        }
        if($card_number_before!=$card_number_request){
            $card_b=$card_number_before;
            $card_a=$card_number_request;
        }
        
        $ifoto=$request->file('photo');
        
        if($ifoto){
        //$ext=$ifoto->getClientOriginalExtension(); 
        $ext='jpg'; 
        $fileName=$motari_code.'.'.$ext;  

        $image_path=public_path().'/photo/'.$fileName;
        $image_path2=public_path().'/photo/public/'.$motari_code.'.jpg';
        
        if (file_exists($image_path))
        unlink($image_path);

        if (file_exists($image_path2))
        unlink($image_path2);
        
        $ifoto->move('photo/',$fileName);

        $member->photo=$fileName;
        }
        $member->name=$names=$request->names;
        $member->telephone=$telephone=$request->telephone;
        $member->province=$province=$request->province;
        $member->district=$district=$request->district;
        $member->sector=$sector=$request->sector;
        $member->cell=$cell=$request->cell;
        $member->village=$village=$request->village;
        $member->zone=$request->zone;
        $member->id_number=$request->id_number;
        $member->permit_number=$request->permit_number;
        $member->share=$request->share;
        $member->gender=$request->gender;
        $member->owner=$nyirimoto=$request->owner;
        $member->card_number=$request->card_number;        
        //$member->card_payed=$request->card_payed;
        $member->cooperative_id=$request->cooperative_id;
        $member->track_username=$request->track_username;
        $member->track_password=$request->track_password;
        $member->save();

        $owner=moto_detail::where('motorcyclist_id',$id)->first();
        moto_detail::where('plate_number',$request->plate_number)->where('motorcyclist_id','!=',$id)->delete(); 


        if(empty($owner)){
        $owner=new moto_detail();
        $owner->motorcyclist_id=$id;
        }
        
        $owner->plate_number=$request->plate_number;
        if($nyirimoto=="Njyewe"){
        $owner->name=$names;
        $owner->telephone=$telephone;
        $owner->province=$province;
        $owner->district=$district;
        $owner->sector=$sector;
        $owner->cell=$cell;
        $owner->village=$village;
        }
        else{
        $owner->name=$request->nyirimoto_names;
        $owner->telephone=$request->nyirimoto_telephone;
        $owner->province=$request->province2;
        $owner->district=$request->district2;
        $owner->sector=$request->sector2;
        $owner->cell=$request->cell2;
        $owner->village=$request->village2;
        }
        $owner->save();
        //expire kubaruza moto or card payment

        if((($owner->plate_number==$plate_request)&&($plate_before!=$plate_request))
          ||(($member->card_number==$card_number_request)&&($card_number_before!=$card_number_request)))

            if(($member->card_number==$card_number_request)&&($card_number_before!=$card_number_request))
             Motorcyclist::where('code',$motari_code)->update(['card_payed'=>NULL]);

        Payment_service::whereHas('payment',function($q) use($motari_code){
          $q->where('code',$motari_code);
        })->active()->update([
        'is_used'=>1,
        'card_number_before'=>$card_b ?? NULL,
        'card_number_after'=>$card_a ?? NULL,
        'plate_number_before'=>$plate_b ?? NULL,
        'plate_number_after'=>$plate_a ?? NULL,
        ]); 

        
        return back()->with('success', 'Byagenze neza !');
    }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function card($type,$id)
    {
        if(!Auth::user()->role==1)
        return redirect()->back()->with('error', 'ntiwemerewe gu printinga !!');      

        $query=Motorcyclist::where($type,$id)
        ->where('card_payed','like','MTC%')
        ->whereNull('card_number');
        //$datas=$query->get();
        //$subquery1=$query->whereNotNull('photo')->get();     
        $motari_code=$query->value('code'); 

        // if($this->checkPayment($motari_code)==false)
        // return back()->with('error','motari ntarishyura');

        $subquery1 = $query->get()->filter( function($item) {
         if ((file_exists( public_path().'/photo/'.$item->photo)AND($item->photo!=NULL)))
         return true; 
         else return false;        
        });
        $subquery2 = $query->get()->filter( function($item) {
        return file_exists(public_path().'/photo/public/'.$item->code.'.jpg');            
        });


        if(($subquery1->isEmpty())AND($subquery2->isEmpty()))
        return redirect()->back()->with('error', 'nta karita yo gusohora, reba ko ifoto irimo cg ko yayishyuye');  
       
        else 
        { 
            $datas=$subquery1->merge($subquery2);
            return view('pages.card',compact('datas'));

        }
    }

    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function card_id(Request $request)
    {

        Motorcyclist::where('id',$request->id)->update(['card_number'=>$request->code]);

        return redirect()->back()->with('success', 'kode yongeweho neza !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        Motorcyclist::find($request->id)->delete();
        moto_detail::where('motorcyclist_id',$request->id)->delete();

        return redirect()->back()->with('success', 'yasibwe neza !');

    }
    private function checkPayment($motari_code)
    {
         if(!Payment::where('code',$motari_code)
             ->whereHas('payment_service',function($q){
             $q->active();
          })->exists())

        return false;
        else return true;

    }
}
