<?php

namespace App\Http\Controllers;

use App\Cooperative;
use App\Events\Registered;
use App\Http\Requests\UserStore;
use App\Motorcyclist;
use App\RwandaADU;
use App\Sms_outbox;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Spatie\Permission\Models\Role;
use Yajra\DataTables\DataTables;



class cooperativesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $districts = RwandaADU::select('district')->groupBy('district')->get();

        $moto=Motorcyclist::where(['card_number'=>NULL,'card_payed'=>'1'])->where('photo','!=',NULL)->distinct('cooperative_id')->pluck('cooperative_id');

        $amakarita = Cooperative::whereIn('id',$moto)->get();
        $cooperatives = Cooperative::latest()->get();

        if ($request->ajax()) {

          return DataTables::of($cooperatives)
            ->addIndexColumn()
            //$model = App\User::with('posts');           
            ->addColumn('actions', function ($cooperatives) {
                return '<span class="dropdown"> 
                    <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true"> 
                        <i class="la la-ellipsis-h"></i> 
                    </a> 
                    <div class="dropdown-menu dropdown-menu-right">                   
                    <a class="dropdown-item" 
                       data-name_edit="'. $cooperatives->name .'" data-acronym_edit="'. $cooperatives->acronym .'"
                       data-district_edit="'. $cooperatives->district .'"
                       data-bpr_account_edit="'. $cooperatives->bpr_account .'"
                       data-id_edit="'. $cooperatives->id .'"

                       data-toggle="modal" data-target="#edit_cooperative_modal"> 
                       Hindura
                    </a>                        
                        <a class="dropdown-item" href="'.route('cooperatives', $cooperatives->id) .'">
                            Abanyamuryango
                        </a>
                        <a class="dropdown-item" data-c_id="'. $cooperatives->id .'" data-toggle="modal" data-target="#sms_modal">
                            Ohereza mesaje
                        </a>
                         <a class="dropdown-item" href="#"
                           data-destroy_id2="'.$cooperatives->id.'" data-toggle="modal" data-target="#delete_cooperative_modal"> 
                             siba
                        </a>
                        
                        </div> 
                </span>
                ';
            })
            ->rawColumns(['actions'])
            ->make(true);
            // <a class="dropdown-item" data-cooperative_id="'. $cooperatives->id .'" data-toggle="modal" data-target="#add_member_modal">
                        //     Ongeramo umunyamuryango
                        /// </a>
        }

        return view('pages.cooperatives', compact('cooperatives', 'districts','amakarita'));
    }

    public function store(Request $request)
    {
        $cooperative_name = $request->get('cooperative_name');
        $acronym = $request->get('acronym');        
        $district = $request->get('district');
        $bpr_account = $request->get('bpr_account');

        $user = Cooperative::create(
            [
                'name' => $cooperative_name,
                'acronym' => $acronym,                
                'district' => $district,                
                'bpr_account' => $bpr_account,                
            ]
        );
       
        return redirect()->back()->with('success', 'Cooperative Created Successfully!');

    }

    public function update(Request $request) {

        $id = $request->get('id');
        $cooperative_name = $request->get('cooperative_name');
        $acronym = $request->get('acronym');        
        $district = $request->get('district');
        $bpr_account = $request->get('bpr_account');

        $cooperative=Cooperative::find($id);
        
        $cooperative->update(
            [
                'name' => $cooperative_name,
                'acronym' => $acronym,                
                'district' => $district,
                'bpr_account' => $bpr_account,                

            ]
        );        

        return redirect()->back()->with('success', 'Updated Successfully!');
    }

    public function destroy(Request $request)
    {
        Cooperative::where('id',$request->id)->delete();
        Motorcyclist::where('cooperative_id',$request->id)->delete();
        return redirect()->back()->with('success', 'ya sibwe neza !');
    }  
}
