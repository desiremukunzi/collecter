<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use App\PaymentTransaction;
use Carbon\Carbon;

class SectorController extends Controller
{
    public function index($id,Request $request)
    {
        $sector=$id;

        $district=DB::table('rwanda_adus')
         ->where('Sector',$id)->value('district');

    	$province=DB::table('rwanda_adus')
         ->where('sector',$id)->value('province');

         $category = null;
        $start_date = date('Y-m-d')  . ' 00:00:00';
        $end_date = date('Y-m-d')  . ' 23:59:00';

        if ($request->filled('category')) {
            $dateRange = explode(' - ', $request->get('daterange'));
            $start_date = Carbon::createFromFormat('d/m/Y H:i:s', $dateRange[0] . ' 00:00:00')->format('Y-m-d H:i:s');
            $end_date = Carbon::createFromFormat('d/m/Y H:i:s', $dateRange[1] . ' 23:59:00')->format('Y-m-d H:i:s');

            $category = $request->get('category') != 0 ? $request->get('category') : null;
        }


        $number_stats = DB::table('payment_transactions')
            ->selectRaw("CAST(COALESCE(SUM( (case when province like '%kigali%' " .( ($category != null) ? " and fee_type_id like $category  " : '')  ." and created_at BETWEEN '". $start_date ."' AND '". $end_date ."' then 1 end) * amount ), 0) AS UNSIGNED) as kigali")
            ->selectRaw("CAST(COALESCE(SUM( (case when province like '%north%' " .( ($category != null) ? " and fee_type_id like $category  " : '')  ." and created_at BETWEEN '". $start_date ."' AND '". $end_date ."' then 1 end) * amount ), 0) AS UNSIGNED) as north")
            ->selectRaw("CAST(COALESCE(SUM( (case when province like '%south%' " .( ($category != null) ? " and fee_type_id like $category  " : '')  ." and created_at BETWEEN '". $start_date ."' AND '". $end_date ."' then 1 end) * amount ), 0) AS UNSIGNED) as south")
            ->selectRaw("CAST(COALESCE(SUM( (case when province like '%east%' " .( ($category != null) ? " and fee_type_id like $category  " : '')  ." and created_at BETWEEN '". $start_date ."' AND '". $end_date ."' then 1 end) * amount ), 0) AS UNSIGNED) as east")
            ->selectRaw("CAST(COALESCE(SUM( (case when province like '%west%' " .( ($category != null) ? " and fee_type_id like $category  " : '')  ." and created_at BETWEEN '". $start_date ."' AND '". $end_date ."' then 1 end) * amount ), 0) AS UNSIGNED) as west")
            ->first();

             $transactions = PaymentTransaction::where(function ($q) use ($category) {
                    if ($category != null) {
                        return $q->where('fee_type_id', $category);
                    }
                    return $q->whereIn('fee_type_id', [1,2,3,4]);
                })
                ->whereBetween('created_at', [$start_date, $end_date])
                ->with(['feeType', 'doneBy'])
                ->where('sector',$sector)
                ->latest()
                ->get();


        return view('pages.sector',compact('sector','district','province','transactions', 'number_stats'));
    }
}
