<?php

namespace App\Http\Controllers;

use http\Client\Curl\User;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {

        $roles = \request()->user()->getRoleNames();

        // if there is no role found
        if ($roles->isEmpty()) {
            // no role assigned to the user
            // logout the user and display the message
            auth()->logout();
            return redirect()->route('login')->with('error', "The system didn't recognize your role");
        }

        $user_role = $roles->first();

        switch ($user_role) {
            case 'admin':
                // redirect to country level dashboard
                return redirect()->route('country.dashboard');
                break;
            case 'editor':
                // redirect to regional coordinator level dashboard
                return redirect()->route('country.dashboard');
                break;            
        }

        // unknown role
        // logout the user and display the message
        auth()->logout();
        return redirect()->route('login')->with('error', "The system didn't recognize your role");
    }
}
