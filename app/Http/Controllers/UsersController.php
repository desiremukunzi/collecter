<?php

namespace App\Http\Controllers;

use App\Events\Registered;
use App\Http\Requests\UserStore;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Spatie\Permission\Models\Role;
use App\User;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    public function index()
    {
        $roles = Role::all();
        $provinces = provinces();
        $users = User::latest()->get();
        return view('pages.users.index', compact('roles', 'users', 'provinces'));
    }

    /**
     * @param  Request  $request
     * @return mixed
     * @throws Exception
     */
    public function getUsers(Request $request)
    {

        $role = $request->get('role');
        $province = $request->get('province');
        $district = $request->get('district');
        $sector = $request->filled('sector');

        $users = User::where('status', 1)->latest()->with(['roles']);

        if ($role) {
            $users = $users->role($role);
        }

        if ($province) {
            $users = $users->where('province', $province);
        }

        if ($district) {
            $users = $users->where('district', $district);
        }

        if ($sector) {
            $users = $users->where('sector', $sector);
        }

        return DataTables::of($users)
            ->addIndexColumn()
            ->addColumn('role', function ($user) {
                return $user->roles->pluck('display_name')->implode(', ');
            })
            ->addColumn('deployment', function ($user) {
                return $user->deployment;
            })
            ->addColumn('action', function ($user) {
                $role = $user->roles->pluck('name')->implode(', ');
                return '<span class="dropdown"> 
                    <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true"> 
                        <i class="la la-ellipsis-h"></i> 
                    </a> 
                    <div class="dropdown-menu dropdown-menu-right">
                     ' . (($role == 'fee_collector') ? ' 
                        <a class="dropdown-item" href="#">
                            <i class="la la-lock"></i> Change PIN
                        </a> 
                        <a class="dropdown-item" href="#">
                            <i class="la la-list"></i> Collection Report
                        </a> ' : ""). '
                        <a class="dropdown-item" href="#" onclick="deactivateUser(\''. $user->id .'\')">
                            <i class="la la-trash"></i> Deactivate
                        </a> 
                    </div> 
                </span> 
                <button type="button" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit" data-id="'. $user->id .'" data-name="'. $user->name .'" data-email="'. $user->email .'" data-telephone="' . $user->telephone . '" data-role="' . $role . '" data-province="'. $user->province .'" data-district="'. $user->district .'" data-sector="'. $user->sector .'" data-toggle="modal" data-target="#edit-modal"> <i class="la la-edit"></i> </button>
                <form method="POST" action="'. route('users.destroy', $user->id) .'" id="deactivate-'. $user->id .'">'. csrf_field() .'<input type="hidden" name="_method" value="DELETE"></form>';
            })
            ->make();
    }

    /*function fetch(Request $request)
    {
        $select = $request->get('select');
        $value = $request->get('value');
        $dependent = $request->get('dependent');
        $data = DB::table('rwanda_adus')
            ->where($select, $value)
            ->groupBy($dependent)
            ->get();
        $output = '<option value="">Select '.ucfirst($dependent).'</option>';
        foreach ($data as $row) {
            $output .= '<option value="'.$row->$dependent.'">'.$row->$dependent.'</option>';
        }
        echo $output;
    }*/

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
//    public function create(Request $request)
//    {
//                $user = User::create(
//                [
//                'name' => $request->name,
//                'email' => $email=$request->email,
//                'telephone' => $request->telephone,
///*              'role' => $request->role,
//*/              'province' => $request->province,
//                'district' => $request->district,
//                'sector' => $request->sector,
//                'otp_status' => true,
//                'password' => Hash::make("password"),
//            ]
//        );
//
//                $user->assignRole([$request->role]);
//
//
//       // Mail::to("desiremukunzi@gmail.com")->send(new UserInitialization);
//        //$url="http://127.0.0.1:8000/password_generate/$email";
//        //url("/password_generate/{$email}");
//
//        $url=URL::signedRoute('password_generate', ['id' => $email]);
//
//
//        $data = array('url'=>$url,'email'=>$email);
//
//        Mail::send(['html'=>'emails.userinitialization'], $data, function($message) use ($data){
//        $message->to($data['email'], 'Developers')->subject('Generate Password');
//         //$message->to('desiremukunzi@gmail.com', 'Akagera Aviation')->subject('Flight Booking in Akagera Aviation');
//        $message->from('developers@gmail.com','Testing');
//
//         });
//
//        return redirect()->route('users')->with('success','Saved and email send for password reset');
//    }

    /**
     * @param  UserStore  $request
     * @return RedirectResponse
     */
    public function store(UserStore $request)
    {
        $name = $request->get('name');
        $telephone = $request->get('telephone');
        $email = $request->get('email');
        $role = $request->get('role');
        $password = $request->get('password');
        // $province = $request->get('province');
        // $district = $request->get('district');
        // $sector = $request->get('sector');

        $user = User::create(
            [
                'name' => $name,
                'email' => $email,
                'telephone' => $telephone,
                // 'province' => $province,
                // 'district' => $district,
                // 'sector' => $sector,
                'password' => bcrypt($password)
            ]
        );
        $user->assignRole($role);
       // event(new Registered($user));
        return redirect()->back()->with('success', 'User Created Successfully!');
    }

    public function update(Request $request) {
        $id = $request->get('id');
        $password = $request->get('password');   
        $currentPassword = $request->get('current-password');
        $name = $request->get('name');
        $telephone = $request->get('telephone');
        $email = $request->get('email');
        $role = $request->get('role');
        
        $user = User::find($id);

        $this->validate($request,[
            'telephone' => ['required', 'regex:/07[8,2,3]{1}[0-9]{7}/'],
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,'.$user->id],
            'current-password' => ['nullable','required_with:password'],
            'password' => ['nullable','string', 'min:8', 'confirmed'],
        ]);    


        if($currentPassword){
        if (!(\Hash::check($currentPassword, $user->password)))         
            return redirect()->back()->with('error', 'password usanzwe ukoresha si iyo');
        }
        $pass=$password??$user->password;
        
        // $province = $request->get('province');
        // $district = $request->get('district');
        // $sector = $request->get('sector');

        //$before_email = $user->email;

        $user->update(
            [
                'name' => $name,
                'telephone' => $telephone,
                'email' => $email,
                'role' => $role,
                'password' => bcrypt($pass),

                // 'province' => $province,
                // 'district' => $district,
                // 'sector' => $sector
            ]
        );
        $user->syncRoles($role);

        // if ($before_email != $email && $user->password == 'password') {
        //     event(new Registered($user));
        // }

        return redirect()->back()->with('success', 'Updated Successfully!');
    }
    public function updateFew(Request $request) {

        $id = $request->get('id');
        $name = $request->get('name');
        $telephone = $request->get('telephone');
        $email = $request->get('email');
        $password = $request->get('password');
        $currentPassword = $request->get('current-password');

        $user = User::find($id);

        $this->validate($request,[
            'telephone' => ['required', 'regex:/07[8,2,3]{1}[0-9]{7}/'],
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,'.$user->id],
            'current-password' => ['nullable','required_with:password'],
            'password' => ['nullable','string', 'min:8', 'confirmed'],
        ]);
       
        if($currentPassword){
        if (!(\Hash::check($currentPassword, $user->password)))         
            return redirect()->back()->with('error', 'password usanzwe ukoresha si iyo');
        }
        $pass=$password??$user->password;
        
        $user->update(
            [
                'name' => $name,
                'telephone' => $telephone,
                'email' => $email,
                'password' => Hash::make($pass),
            ]
        );
        return redirect()->back()->with('success', 'Updated Successfully!');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
//    public function password_generate($id)
//    {
//            Route::get('/password_generate/{id}', function (Request $request) {
//            if (! $request->hasValidSignature()) {
//                abort(401); }})->name('password_generate');
//             $email=$id;
//          return view('pages.reset',compact('email'));
//
//
//
//    }
//
//    public function password_update(Request $request)
//    {
//        $email=$request->email;
//        $password=Hash::make($request->password);
//
//
//    $user= User::where('email','=',$request->email)->update(['password'=>$password]);
//    return redirect()->route('login_page')->with('success','Password Updated Successfully!!');
//    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
//    public function show($id)
//    {
//        //
//    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
//    public function edit($id)
//    {
//        $roles = Role::all();
//        $users = User::all();
//
//
//        $province_list = DB::table('rwanda_adus')
//            ->groupBy('province')
//            ->get();
//    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    /*public function update(Request $request)
    {
        $user = User::find($request->id);
        $user->email = $request->email;
        $user->role = $request->role;
        $user->telephone = $request->telephone;
        $user->province = $request->province;
        $user->district = $request->district;
        $user->sector = $request->sector;
        $user->save();

        return redirect()->route('users')->with('success', "User updated ");

    }*/

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return void
     */
    public function destroy($id)
    {
        User::destroy($id);
        return redirect()->back()->with('success', 'Deactivated Successfully!');
    }
    public function deleted_users()
    {

        $roles = Role::all();
        $provinces = provinces();
        $users =User::onlyTrashed()->get();

        return view('pages.users.index', compact('roles', 'users', 'provinces'));
    }

     public function getDeletedUsers(Request $request)
    {

        $role = $request->get('role');
        $province = $request->get('province');
        $district = $request->get('district');
        $sector = $request->filled('sector');

        $users = User::onlyTrashed()->with(['roles']);

        if ($role) {
            $users = $users->role($role);
        }

        if ($province) {
            $users = $users->where('province', $province);
        }

        if ($district) {
            $users = $users->where('district', $district);
        }

        if ($sector) {
            $users = $users->where('sector', $sector);
        }

        return DataTables::of($users)
            ->addIndexColumn()
            ->addColumn('role', function ($user) {
                return $user->roles->pluck('display_name')->implode(', ');
            })
            ->addColumn('deployment', function ($user) {
                return $user->deployment;
            })
            ->addColumn('action', function ($user) {
                $role = $user->roles->pluck('name')->implode(', ');
                return '
                        <a  href="#" onclick="restoreUser(\''. $user->id .'\')">
                            <i class="la la-rotate-left"></i> 
                        </a> 
                    
                
                <form method="POST" action="'. route('users.restore', $user->id) .'" id="restore-'. $user->id .'">'. csrf_field() .'<input type="hidden" name="_method" value="POST"></form>';
            })
            ->make();
    }

     public function restore($id)
    {
        User::withTrashed()->find($id)->restore();
        $user=User::find($id);
        $user->getRoleNames();


        event(new Registered($user));

        return redirect()->route('users.index')->with('success', 'Restored Successfully!');
    }
    public function generateToken()
    {
        //return response()->json(["token"=>csrf_token()]);
        return csrf_token();
    }
}
