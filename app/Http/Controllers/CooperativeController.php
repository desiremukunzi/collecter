<?php

namespace App\Http\Controllers;
use App\Cooperative_member;
use App\Cooperative;
use App\Motorcyclist;
use App\Payment;
use App\PaymentTransaction;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class CooperativeController extends Controller
{
    public $cooperative_name;
    public $plate_number;
    public $owner_name;
    public $owner_telephone;
    public $owner_province;
    public $owner_district;
    public $owner_sector;
    public $owner_cell;
    public $owner_village;

    public function __construct()
    {
        $this->cooperative_name = '-';
        $this->plate_number = '-';
        $this->owner_name = '-';
        $this->owner_telephone = '-';
        $this->owner_province = '-';
        $this->owner_district = '-';
        $this->owner_sector = '-';
        $this->owner_cell = '-';
        $this->owner_village = '-';
    }
    public function index($district,Request $request)
    {
        //$cooperative=$id;

        $cooperatives=Cooperative::where('district',$district)->get();

         // $district=DB::table('cooperatives')
         // ->where('district',$id)->value('district');

        $province=DB::table('rwanda_adus')
         ->where('district',$district)->value('province');    

         $cooperativ=Cooperative::where('district',$district)->pluck('id');
         $members1=Motorcyclist::whereIn('cooperative_id',$cooperativ)->orderBy('id','desc')->get();


         if ($request->ajax()) {
          return $members= DataTables::of($members1)
            ->addIndexColumn()           
            
            ->make(true);
        }

        return view('pages.district',compact('cooperatives','province','district'));
    }

     public function cooperatives($id,Request $request)
    {
         $cooperative=DB::table('cooperatives')
         ->where('id',$id)->value('name');        
         
         $district=DB::table('cooperatives')
         ->where('id',$id)->value('district');

         $cooperatives=Cooperative::where('district',$district)->get();

         $province=DB::table('rwanda_adus')
         ->where('district',$district)->value('province');

        $category = null;
        $start_date = date('Y-m-d')  . ' 00:00:00';
        $end_date = date('Y-m-d')  . ' 23:59:00';

      $members=Motorcyclist::with('moto_detail','cooperative')->where('cooperative_id',$id)->orderBy('id','desc')->get();

//          if ($request->ajax()) {
//           return $members= DataTables::of($members1)
//             ->addIndexColumn()
//             //$model = App\User::with('posts');

//             ->addColumn('cooperative', function (Motorcyclist $members1) {  
            
//             $this->plate_number=$members1->moto_detail->plate_number ?? '-';
//             $this->owner_village=$members1->moto_detail->village ?? '-';
//             $this->owner_cell=$members1->moto_detail->cell ?? '-';              
//             $this->owner_sector=$members1->moto_detail->sector ?? '-';
//             $this->owner_district=$members1->moto_detail->district ?? '-';
//             $this->owner_province=$members1->moto_detail->province ?? '-';
//             $this->owner_telephone=$members1->moto_detail->telephone ?? '-';
//             $this->owner_name=$members1->moto_detail->name ?? '-';              
//             $this->cooperative_name=$members1->cooperative->name ?? '-';              
            
//             return $this->cooperative_name=$members1->cooperative->name ?? '-';})
//             ->addColumn('action', function ($members1) {
//                 return '<span class="dropdown"> 
//                     <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true"> 
//                         <i class="la la-ellipsis-h"></i> 
//                     </a> 
//                     <div class="dropdown-menu dropdown-menu-right">                    
                     
//                      <a class="dropdown-item" 

// data-id="'. $members1->id .'" data-name="'. $members1->name .'" 
// data-telephone="' . $members1->telephone . '"data-photo="' . $members1->photoExists() . '"
// data-province="'. $members1->province .'" data-district="'. $members1->district .'" 
// data-sector="'. $members1->sector .'" 
// data-cooperative_id="'. $members1->cooperative_id.'" data-cell="'. $members1->cell.'"
// data-code="'. $members1->code.'"
// data-village="'. $members1->village.'" data-zone="'. $members1->zone.'"
// data-id_number="'. $members1->id_number.'"data-permit_number="'. $members1->permit_number.'"
// data-share="'. $members1->share.'"data-gender="'. $members1->gender.'"
// data-owner="'. $members1->owner.'"
// data-owner="'. $members1->owner.'"
// data-card_number="'. $members1->card_number.'"
// data-cooperative_name="'. $this->cooperative_name.'"

// data-plate_number="'. $this->plate_number.'"
// data-owner_name="'. $this->owner_name.'"
// data-owner_telephone="'.$this->owner_telephone.'"
// data-owner_province="'. $this->owner_province.'"
// data-owner_district="'. $this->owner_district.'"
// data-owner_sector="'. $this->owner_sector.'"
// data-owner_cell="'. $this->owner_cell.'"
// data-owner_village="'. $this->owner_village.'"

// data-toggle="modal" data-target="#edit_member_modal"> 
// imyirondoro yose
//                         </a>                        
//                         <a class="dropdown-item" href="'.route('card',['id',$members1->id]) .'">
//                             ikarita
//                         </a> 
//                         <a class="dropdown-item" href="#"
//                            data-card_id="'.$members1->id.'" data-toggle="modal" data-target="#card_code_modal"> 
//                              ongeraho kode ku ikarita
//                         </a> 
//                          <a class="dropdown-item" href="#"
//                            data-destroy_id="'.$members1->id.'" data-toggle="modal" data-target="#delete_member_modal"> 
//                              siba
//                         </a> 
//                     </div> 
//                 </span> 
//                 ';
//             })
//             ->make(true);
//         }
return view('pages.cooperative',compact('members','cooperatives','province','district','cooperative'));
    }
}
