<?php

namespace App\Http\Controllers;
use App\Tip;
use Illuminate\Http\Request;
use App\Http\Requests\BonusRequest;

class BonusController extends Controller
{
    public function store(BonusRequest $request){
        $tip =new Tip();
        $tip->code=strtoupper($request->code);
        $tip->plate_number=strtoupper($request->plate_number);
        $tip->month=$request->month;
        $tip->save();
    return redirect()->back()->with('success', 'Done Successfully!');
        //return view('pages.bonus');
    }
    //
}
