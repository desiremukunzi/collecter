<?php

//DynamicDepdendent.php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class DynamicDependent extends Controller
{
    function index()
    {
        $country_list = DB::table('rwanda_adus')
            ->groupBy('province')
            ->get();
        return view('dynamic_dependent')->with('country_list', $country_list);
    }

}
