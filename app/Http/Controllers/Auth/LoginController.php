<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('pages.auth.login');
    }
    protected function authenticated(Request $request, $user)
    {
        if($user->cooperative_id!=NULL){
             return redirect('/cooperatives_list/'.$user->cooperative_id);
        }   
        else if($user->hasRole('editor')){
            return redirect('/country');
        }
        else if($user->hasRole('traffic')){
            return redirect('/search');
        }        
        else if($user->role!=NULL){
            return redirect('/');
        }
        else {
              return $this->logout($request);
        }
    }
}
