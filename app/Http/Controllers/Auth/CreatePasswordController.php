<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class CreatePasswordController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function index() {

        if (\request()->user()->password != 'password')
            return redirect()->route('index');

        return view('pages.auth.create-password');
    }

    public function store(Request $request) {

        $request->validate([
            'password' => 'required|confirmed|min:8',
        ]);

        $user = $request->user();

        $user->password = bcrypt($request->get('password'));
        $user->setRememberToken(Str::random(60));
        $user->save();

        return redirect()->route('index')->with('success', 'Password Created Successfully');
    }
}
