<?php

namespace App\Http\Controllers;

use App\Motorcyclist;
use App\RwandaADU;
use App\Cooperative;
use App\Sms_outbox;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class SmsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sms=Sms_outbox::latest()->get();
            if ($request->ajax()) {

          return DataTables::of($sms)
            ->addIndexColumn()
            //$model = App\User::with('posts');           
            ->addColumn('actions', function ($sms) {
                return '<span class="dropdown"> 
                    <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true"> 
                        <i class="la la-ellipsis-h"></i> 
                    </a> 
                    <div class="dropdown-menu dropdown-menu-right">                   
                    <a class="dropdown-item" 
data-sender="'. $sms->sender .'"  data-c_id="'. $sms->cooperative_id .'"  
                data-message="'. $sms->message .'"
                data-id="'. $sms->id .'" data-counter2="'. $sms->characters .'"

data-toggle="modal" data-target="#edit_sms_modal"> 
Hindura
                        </a>                        
                         <a class="dropdown-item" href="#"
                           data-destroy_id="'.$sms->id.'" data-toggle="modal" data-target="#delete_sms_modal"> 
                             siba
                        </a>
                        
                        </div> 
                </span>
                ';
            })
            ->rawColumns(['actions'])
            ->make(true);
        }

        return view('pages.sms',compact($sms));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function sms_destroy(Request $request)
    {
        Sms_outbox::where('id',$request->id)->delete();
        return redirect()->back()->with('success', 'ya sibwe neza !');
    }
    public function sms(Request $request)
    {
        $id=$request->cooperative_id;

        if(RwandaADU::where('district',$id)->exists())
        {
          $cooperatives=Cooperative::where('district',$id)->pluck('id');
          
          $phones=Motorcyclist::select('telephone','name','code','new_code')
          ->whereIn('cooperative_id',$cooperatives)
          ->whereRaw('LENGTH(telephone) = 10')
          ->where(function($query){
            $query->where('telephone','like','072%')
        ->orWhere('telephone','like','073%')->orWhere('telephone','like','078%');})
          ->orWhere('telephone','0788354222')
          ->orWhere('telephone','0788319169')
          ->groupBy('telephone')->get();
        }
        else if($id=='Bose'){
        $phones=Motorcyclist::select('telephone','name','code','new_code')
        ->whereRaw('LENGTH(telephone) = 10')
        ->where(function($query){
            $query->where('telephone','like','072%')->orWhere('telephone','like','073%')->orWhere('telephone','like','078%');})
        ->orWhere('telephone','0788354222')
        ->orWhere('telephone','0788319169')
        ->groupBy('telephone')->get(); 
        }

        else {     $phones=Motorcyclist::select('telephone','name','code','new_code')
        ->where('cooperative_id',$request->cooperative_id)
        ->whereRaw('LENGTH(telephone) = 10')
        ->where(function($query){
            $query->where('telephone','like','072%')->orWhere('telephone','like','073%')->orWhere('telephone','like','078%');})
        ->orWhere('telephone','0788354222')
        ->orWhere('telephone','0788319169')
        ->groupBy('telephone')->get();  
        }
        $s=Sms_outbox::orderBy('session','desc')->limit(1)->value('session');
        $session=$s+1;
        
        $recipients=$phones->count(); 

        foreach ($phones as $key => $phone) {    
        //$message=$phone->name.' ('.$phone->code.') '.$request->message; 
        $message=$phone->name.", kode yawe ".$phone->new_code." ".$request->message;
        sendSMS($phone->telephone,$message,$request->cooperative_id,$request->uwohereza,$session,$recipients,$request->characters,$request->message);

    }
        
       return redirect()->route('sms.index')->with('success', 'message yoherejwe neza !');
    }
}
