<?php
namespace App\Http\Controllers;
use App\Bpr_response;
use App\Payment;
use Auth;
use App\Services\BprPendingService;


class BprResponseController extends Controller
{
public function reloadFailed()
{
if(!Auth::user()->role==1)
return "Not Allowed";

$inc= new BprPendingService() ;
return $inc->__invoke();
}
}