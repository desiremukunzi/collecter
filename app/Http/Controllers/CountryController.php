<?php

namespace App\Http\Controllers;
use App\Motorcyclist;
use App\Payment;
use App\Tip;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        $category = null;
        $start_date = date('Y-m-d')  . ' 00:00:00';
        $end_date = date('Y-m-d')  . ' 23:59:00';

        if ($request->get('daterange')) {
            $dateRange = explode(' - ', $request->get('daterange'));
            $start_date = Carbon::createFromFormat('d/m/Y H:i:s', $dateRange[0] . ' 00:00:00')->format('Y-m-d H:i:s');
            $end_date = Carbon::createFromFormat('d/m/Y H:i:s', $dateRange[1] . ' 23:59:00')->format('Y-m-d H:i:s');

            //$category = $request->get('category') != 0 ? $request->get('category') : null;
        }

         $districts = DB::table('rwanda_adus')
         ->groupBy('district')
         ->get();

         // $transactions = PaymentTransaction::where(function ($q) use ($category) {
         //            if ($category != null) {
         //                return $q->where('fee_type_id', $category);
         //            }
         //            return $q->whereIn('fee_type_id', [1,2,3,4]);
         //        })
         //        ->whereBetween('created_at', [$start_date, $end_date])
         //        ->with(['feeType', 'doneBy'])
         //        ->latest()
         //        ->get();

        
       $ikarita=Payment::with('motorcyclist','cooperative')->where(['status'=>1,'fee_type'=>'Ikarita'])->whereBetween('created_at', [$start_date, $end_date])->latest()->get();
       $umusanzu=Payment::with('motorcyclist','cooperative')->where(['status'=>1,'fee_type'=>'Umusanzu'])->whereBetween('created_at', [$start_date, $end_date])->latest()->get();
       $ibarura=Payment::with('motorcyclist','cooperative')->where(['status'=>1,'fee_type'=>'Kubaruza moto'])->whereBetween('created_at', [$start_date, $end_date])->latest()->get();
       $parking=Payment::where(['status'=>1,'fee_type'=>'parking'])->whereBetween('created_at', [$start_date, $end_date])->latest()->get();
       $tips=Tip::with('motorcyclist')->whereBetween('created_at', [$start_date, $end_date])->latest()->get();
       
       $members=Motorcyclist::with('moto_detail')->orderBy('id','desc')->limit(50)->get();

        return view('pages.country',compact('districts','ikarita','umusanzu','ibarura','parking','members','tips'));
    }
}
