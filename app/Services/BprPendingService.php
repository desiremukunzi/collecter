<?php
namespace App\Services;
use App\Bpr_response;
use App\Payment;

class BprPendingService
{
public function __invoke()
{    

    $query0= Payment::with('cooperative','motorcyclist')
    //->whereBetween('created_at',['2021-03-07 00:00:00','2021-03-08 23:59:59'])
    ->where('fee_type','Umusanzu')
    ->where('status', '1')
    ->whereHas('cooperative',function($query){
      $query->whereNotNull('bpr_account');
    })->where(function($query){
     $query->whereNull('bprRefNo')->orWhere('bprRefNo','');
    })->orderBy('id','asc')->get();

    // $query1 =$query0->whereNull('bprRefNo')->get();
    // $query2 =$query0->where('bprRefNo', '')->get();
    //
    // $query = $query1->merge($query2);
    ?>
   <table border="1">
     <tr>
     <td>Index</td>
     <td>id</td>
     <td>amount</td>
     <td>BPR</td>
     <td>name</td>
     </tr>     
     <?php
    $sum=0;
    $index=1;
   //return $query0->take(1);
    foreach ($query0->take(100) as  $record) {
      $payment_id=$record->id;
      $amount = $record->amount;
      $cooperative_account = $record->cooperative->bpr_account;
      $name = $record->motorcyclist->name;
      $telephone = $record->motorcyclist->telephone;
      $cooperative = $record->cooperative->name;
      $description = " from " . $name."(".$telephone.")" . " to " . $cooperative; 
echo  '<tr>';
echo '<td>'.$index.'</td>'.'<td>'.$payment_id.'</td>'.'<td>'.$amount.'</td>'.'<td>'.$cooperative_account.'</td>'.'<td>'.$name.'</td>'.'</tr>';
$index++;

     $responseBpr = creditBpr($amount,$cooperative_account,$description,$payment_id);

      echo $statusCode = $responseBpr->statusCode;echo " ";
      $message = $responseBpr->message;
      $bprRefNo = $responseBpr->bprRefNo;

      if (Bpr_response::where('payment_id', $record->id)->exists()) {
        $id = Bpr_response::where('payment_id', $record->id)->value('id');
        $bpr_response = Bpr_response::find($id);
      } else {
        $bpr_response = new Bpr_response();
      }

      $bpr_response->payment_id = $record->id;
      $bpr_response->statusCode = $statusCode;
      $bpr_response->message = $message;
      $bpr_response->bprRefNo = $bprRefNo;
      $bpr_response->save();
      
      if(Payment::where('bprRefNo',$bprRefNo)->exists())
        return $bprRefNo;

      $updated=Payment::where('id', $record->id)->update(['bprRefNo' => $bprRefNo]);
      if($updated){
      echo $index.". ".$record->id." done ".$bprRefNo.'<br>';
      $sum=$sum+$amount;
    }
      else echo $record->id."not done".'<br>';
    }
    echo  '<tr>';
echo '<td colspan="7">'.$sum.'</td>'.'</tr>';

echo '</table>';
}     }

      ?>