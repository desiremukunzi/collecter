<?php

namespace App\Jobs;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

class SendSMS implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $message;
    protected $phone_numbers;

    /**
     * Create a new job instance.
     *
     * @param $message
     * @param Collection $phone_numbers
     */
    public function __construct($message, $phone_numbers)
    {
        $this->message = $message;
        $this->phone_numbers = $phone_numbers;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $username = config('custom.sms.username');
        $password = config('custom.sms.password');
        $source = 'Ngali FCMS';
        $message = $this->message;

        if ($this->phone_numbers->isEmpty()) {
            Log::debug('phone numbers empty. leaving ...');
            return;
        }

        $destination = $this->phone_numbers->map(function($telephone) {
            return '25' . $telephone;
        })->implode(', ');

        $client = new Client();
        try {
            $response = $client->request('GET',
                "http://api.rmlconnect.net/bulksms/bulksms?username=$username&password=$password&type=0&dlr=1&destination=$destination&source=$source&message=$message");

            Log::debug('response : ' . (string) $response->getBody());
        } catch (GuzzleException $e) {
            Log::critical("GuzzleException : Failed to Send SMS");
        }

    }
}
