
$(document).ready(function(){

 $('.dynamic').change(function(){
  if($(this).val() != '')
  {
   var select = $(this).attr("id");
   var value = $(this).val();
   var dependent = $(this).data('dependent');
   var _token = $('input[name="_token"]').val();
   $.ajax({
    url:"{{ route('dynamicdependent.fetch') }}",
    method:"POST",
    data:{select:select, value:value, _token:_token, dependent:dependent},
    success:function(result)
    {
     $('#'+dependent).html(result);
    }

   })
  }
 });

 $('#province').change(function(){
  $('#district').val('');
  $('#sector').val('');
 });

 $('#district').change(function(){
  $('#sector').val('');
 });


});


$('#edit_modal').on('show.bs.modal', function (event) {

  var button = $(event.relatedTarget)// Button that triggered the modal
  var recipient = button.data('recipient') // Extract info from data-* attributes
  var full_name = button.data('full_name') // Extract info from data-* attributes
  var email = button.data('email') // Extract info from data-* attributes
  var telephone = button.data('telephone') // Extract info from data-* attributes
  var role = button.data('role') // Extract info from data-* attributes
  var province = button.data('province') // Extract info from data-* attributes
  var district = button.data('district') // Extract info from data-* attributes
  var sector = button.data('sector') // Extract info from data-* attributes
  var id = button.data('id') // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this)
  modal.find('.modal-title').text('Edit  ' + full_name + ' Info')
  modal.find('.modal-body input').val(recipient)
  modal.find('.modal-body #full_name').val(full_name)
  modal.find('.modal-body #telephone').val(telephone)
  modal.find('.modal-body #email').val(email)
  modal.find('.modal-body #province').val(province)
  modal.find('.modal-body #district').val(district)
  modal.find('.modal-body #sector').val(sector)
  modal.find('.modal-body #id').val(id)


})

