$(document).ready(function () {

    $('.dynamic').change(function () {
        if ($(this).val() != '') {
            var select = $(this).attr("id");
            var value = $(this).val();
            var dependent = $(this).data('dependent');
            var _token = $('input[name="_token"]').val();
            $.ajax({
                url: "{{ route('dynamicdependent.fetch') }}",
                method: "POST",
                data: {select: select, value: value, _token: _token, dependent: dependent},
                success: function (result) {
                    $('#' + dependent).html(result);
                }

            })
        }
    });

   /* $('#province').change(function () {
        $('#district').val('');
        $('#sector').val('');
    });

    $('#district').change(function () {
        $('#sector').val('');
    });*/


});


$('#edit_modal').on('show.bs.modal', function (event) {

    var button = $(event.relatedTarget)// Button that triggered the modal
    var recipient = button.data('recipient') // Extract info from data-* attributes
    var acronym = button.data('acronym') // Extract info from data-* attributes
    var cooperative_name = button.data('cooperative_name') // Extract info from data-* attributes
    var district = button.data('district') // Extract info from data-* attributes
    var id = button.data('id') // Extract info from data-* attributes
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this)
    modal.find('.modal-title').text('Edit  ' + cooperative_name + ' Info')
    modal.find('.modal-body input').val(recipient)
    modal.find('.modal-body #cooperative_name').val(cooperative_name)
    modal.find('.modal-body #acronym').val(acronym)   
    modal.find('.modal-body #district').val(district)
    modal.find('.modal-body #id').val(id)
})

