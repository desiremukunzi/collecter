<?php
require_once 'index_moto_pay.php';

use App\Motari_moto;
use App\MotoPay;
use App\Motorcyclist;
use App\Payment;
use App\Tip;

// Reads the variables sent via POST from our gateway
$newRequest   = $_GET["newRequest"];
$msisdn = $_GET["msisdn"];
$input        = $_GET["input"];
$tel = substr($msisdn, 2, 10);

$o = "##*.Ongera";
$s = "#0.Subira inyuma";
$menu="1.Kwishyura#2.Ubwizigame#3.Tanga amakuru#4.Reba ko moto ibaruye#5.Umusanzu wishyuwe#6.Parking zishyuwe";
//$menu="1.Kwishyura#2.Guhinduza tel#3.Tanga amakuru#4.Reba ko moto ibaruye#5.Umusanzu wishyuwe#6.Parking zishyuwe#7.Kubaruza Moto";
$ukwezi="#Shyiramo umubare uhagarariye ukwezi#(urugero:9)#";
$purake="#Shyiramo purake ya moto#urugero: RA003B";
$shyiramoKode="Shyiramo kode yawe";
$codePurakeTel="Shyiramo purake ya moto cg kode yahawe na federation cg tel ibaruye";


if ($newRequest == "1") {
    $session = MotoPay::orderBy('session', 'desc')->limit(1)->value('session');
    $session = $session + 1;
    $level = 0;
    $sublevel1 = 0;

    if ($input == "797*1*4") {
        $message = $codePurakeTel;
        $data = array("action" => "FC", "message" => $message);
        SaveMotoPay($message, $input, $session, $msisdn, 2, 4);
    } else if (substr($input, 0, 7) == "797*1*1") {
        $message = $input;
        SaveMotoPay($message, $input, $session, $msisdn, $level, $sublevel1);
        $new_code = substr($input, 8, 5);
        $code = substr($input, 8, 10);
        $query = Motorcyclist::where('new_code', $new_code)->orWhere('code', $code)->first();
        $data = array("action" => "FB", "message" => $message);

        if (!empty($query)) {
            $name = $query->name;
            $telephone = $query->telephone;
            $message = $name . " " . $telephone . "#Shyiramo amafaranga";
            $data = array("action" => "FC", "message" => $message);
            SaveMotoPay($message, $input, $session, $msisdn, 3, 1);
            MotoPay::where('session', $session)->update(['code' => $code]);
        } else $data = array("action" => "FB", "message" => $code);
    } else {
        // $message = "welcome to MOPAY Application#1.FERWACOTAMO#2.MOPAY#3.Ishuri#4.Ngali#5.Equity Loan";
        // $data = array("action" => "FC", "message" => $message);
        // SaveMotoPay($message, $input, $session, $msisdn, $level, $sublevel1);
        // $message = "1.Kwishyura Motari#2.Guhinduza tel#3.Tanga amakuru#4.Reba ko moto ibaruye#5.Kwishyura ikarita#6.Kwishyura umusanzu#7.Umusanzu wishyuwe#8.Kubaruza Moto#9.Kwishyura parking#10.Parking zishyuwe";
        $message=$menu;
        $data = array("action" => "FC", "message" => $message);
        SaveMotoPay($message, $input, $session, $msisdn, "1", "1");
    }
} else if ($newRequest == "0") {
    $query = MotoPay::where('telephone', $msisdn)->orderBy('id', 'desc')->limit(1)->first();
    $session = $query->session;
    $level = $query->level;
    $sublevel1 = $query->sublevel1;


    if ($input == "0") {
        MotoPay::where('session', $session)->orderBy('id', 'desc')->limit(1)->delete();
        $message = MotoPay::where('session', $session)->orderBy('id', 'desc')->limit(1)->value('message');
        $data = array("action" => "FC", "message" => $message);
    } else if ($input != "*") {
        if ($level == "0") {
            if ($input == "1") {
                $message =$menu;
                $data = array("action" => "FC", "message" => $message);
                SaveMotoPay($message, $input, $session, $msisdn, "1", "1");
            } else if (($input == "2") || ($input == "3"))
                $data = UnderMaintenance();

            else $data = InvalidInput();
        } else if ($level == "1") {
            if ($input == "1") {
                //$message = "Shyiramo  MTN  kode ya Motari cg kode iri ku ikarita ye";
                $message="1.Motari#2.Ikarita#3.Umusanzu#4.Parking#5.Kubaruza Moto";
                $data = array("action" => "FC", "message" => $message);
                SaveMotoPay($message, $input, $session, $msisdn, 'kwishyura', 1);
               // SaveMotoPay($message, $input, $session, $msisdn, 2, 1);
            } 
            //else if (($input == "2") || ($input == "7") || ($sublevel1 == "kubaruzaMoto")) {
            else if ($input == "2") {
                //2 was guhinduza tel;
                // $message = $shyiramoKode;
                // $data=FC($message);
                // SaveMotoPay($message, $input, $session, $msisdn, 'ubwzgm', $input);
                $data=UnderMaintenance();
            } else if ($input == "3") {
                $message = "#Shyiramo purake ya moto cg kode iri ku ikarita ya FERWACOTAMO cg MTN Code #";
                $data = array("action" => "FC", "message" => $message);
                SaveMotoPay($message, $input, $session, $msisdn, 2, 3);
            } else if ($input == "4") {
                $message = $codePurakeTel;
                $data = array("action" => "FC", "message" => $message);
                SaveMotoPay($message, $input, $session, $msisdn, 2, 4);
            } 
            else if ($input == "5") {
                $message = $shyiramoKode;
                $data = array("action" => "FC", "message" => $message);
                SaveMotoPay($message, $input, $session, $msisdn, 2, "kurebaUmusanzu");
            } 
            else if ($input == "6") {
                $message = $purake;
                $data = array("action" => "FC", "message" => $message);
                SaveMotoPay($message, $input, $session, $msisdn, 2, 'kurebaParking');
            }             
            else $data = InvalidInput();
        }
        else if ($level == "ubwzgm"){
            $query =checkMotari($input);
            if(!$query->exists()){
                $data=Ntabonetse();
                
            }
            else{
                $message=$query->value('name').", Hitamo##1.Gushyiramo umubare w'ibanga watanze#2.Guhindura umubare w'ibanga";
                $motari_code=$query->value('new_code');
                $data=FC($message);
                SaveMotoPay($message, $input, $session, $msisdn,'pin',$motari_code);
            }

        //$message="1.Kureba ubwizigame#2.Kubikuza#0.Subira inyuma";
        }
        else if($level=="pin"){
            if($input==1){
            $message="Shyiramo umubare w'ibanga ";
                $data=FC($message);
                SaveMotoPay($message, $input, $session, $msisdn,'inputed_pin',0);
        
    }
else if($input==2)
{
    $query =checkMotari($sublevel1);
            if($query->value('telephone')!=$tel){
                $data=FB("Koresha nimero yawe ibaruye muri FERWACOTAMO cg uhamagare 0783675472 usabe ubufasha");                
            }
            else {
                $message="Shyiramo umubare w'ibanga mushya
                #(ugomba kuba ugizwe n'imibare itanu)";
                $data=FC($message);
                SaveMotoPay($message, $input, $session, $msisdn,"reset_pin",0);
                 }
}  
else $data=InvalidInput(); 
}
else if($level=="inputed_pin"){
    $motari_code=motariCode($msisdn,$session);
    $datas=savingInfo($motari_code,$input);
            $dat=$datas->json()["status"];
            
            if($dat==200){
                if($datas['data']['can_withdraw']==true)
                $msg="#wemerewe kubikuza,Niba ushaka kubikuza,#shyiramo umubare w'amafaranga";  
                $total_amount=$datas['data']['total_amount'];              
                $message=$datas['data']['name'].", umaze kwizigama ".$total_amount.$msg;
                //$data=FB($message);
                $data=FC($message);
                SaveMotoPay($message, $input, $session, $msisdn,"Kubikuza",$total_amount);

            }
        elseif($dat==401){
        $message="Umubare w'ibanga siwo".$s;
        $data=FC($message);
        }
        elseif($dat==404){
            $message="Nta mubare w'ibanga ufite,kuwubona hamagara 0783675472 usabe ubufasha";
            $data=FB($message);
        }
        elseif($dat==403){
            $message="Watse amafaranga aruta ubwizigame ufite";
            $data=FB($message);
        }
        else {
            $message=$dat;
            $data=FB($message);
        }
        //SaveMotoPay($message, $input, $session, $msisdn,"Kubikuza",$total_amount);
}
else if($level=="reset_pin"){
    $data0 = array("input" => $input);
            $validator = Validator::make($data0, [
                'input' => 'string|size:5'
            ]);
            if ($validator->fails())
            $data=FB("Umubare w'ibanga washyizemo si imibare itanu");  
            else{
                $message="Ongera ushyiremo umubare washyizemo mbere";
                $data=FC($message);
                SaveMotoPay($message, $input, $session, $msisdn,"update_pin",0);
            }
    }
    else if($level=="update_pin"){
        $pin = MotoPay::where(['telephone'=>$msisdn,'session'=>$session,'level'=>'update_pin'])->latest()->value('input');
        if($pin!=$input){
            $message="Umubare w'ibanga washyizemo ntusa n'uwa mbere";
            $data=FC($message); 
        }
        else{$motari_code = MotoPay::where(['telephone'=>$msisdn,'session'=>$session,'level'=>'pin'])->latest()->value('sublevel1');
        $message=resetPin($motari_code,$input);
        $data=FB($message);
        }

    }
        else if($level=="Kubikuza"){
            $data0 = array("input" => $input);
            $validator = Validator::make($data0, [
                'input' => 'integer|min:40'
            ]);
            if ($validator->fails()){
            $data=FB("Ntiwemerewe kubikuza munsi ya 40 FRW"); 
            } 
            else if($sublevel1<$input){
            $data=FB("Ntiwemerewe kubikuza amafaranga aruta ayo wizigamiye ".$sublevel1); 
            }
            else{
            $motari_code = MotoPay::where(['telephone'=>$msisdn,'session'=>$session,'level'=>'pin'])->latest()->value('sublevel1');
            $pin = MotoPay::where(['telephone'=>$msisdn,'session'=>$session,'level'=>'Kubikuza'])->latest()->value('input');
    
            $message=withdrawSaving($motari_code,$pin,$input);
            $data=FB("Amafaranga yoherejwe kuri nimero yawe,urayabona mu mwanya muto");
        }
        }
        else if ($level == "kwishyura") {
            if ($input == "1") {
                $message = "Shyiramo  kode iri ku ikarita ye";
                $data = array("action" => "FC", "message" => $message);
               SaveMotoPay($message, $input, $session, $msisdn, 2, $input);    
            }
         if ($input == "2") {
                $message = $shyiramoKode; 
                $data = array("action" => "FC", "message" => $message);
                SaveMotoPay($message, $input, $session, $msisdn, 2,'ikarita');
         }
         if ($input == "3") {
                $message = $shyiramoKode; 
                $data = array("action" => "FC", "message" => $message);
                SaveMotoPay($message, $input, $session, $msisdn, 2,'umusanzu');
         }
         if ($input == "4") {
                $message = $purake; 
                $data = array("action" => "FC", "message" => $message);
                SaveMotoPay($message, $input, $session, $msisdn,'ukwezi','kwishyuraParking');
         }
         if ($input == "5") {
                $message = $shyiramoKode; 
                $data = array("action" => "FC", "message" => $message);
                SaveMotoPay($message, $input, $session, $msisdn, 2,'kubaruza_moto');
         }         
        }
        else if ($level == "ukwezi") {
               $message = $ukwezi;
                $data = array("action" => "FC", "message" => $message);
                SaveMotoPay($message, $input, $session, $msisdn, 2, 'kwishyuraParking');
        }
        else if ($level == "2") {
            if ($sublevel1 == "1") {
                $messag = "#Shyiramo amafaranga #";
                SaveMotoPay($messag, $input, $session, $msisdn, 3, 1);
            } else if ($sublevel1 == "2") {
                $messag = "#Shyiramo iyindi nimero #";
                SaveMotoPay($messag, $input, $session, $msisdn, 3, 2);
            }
            if (($sublevel1 == "1") || ($sublevel1 == "2")) {
                $query = Motorcyclist::where('code', $input)->orWhere('new_code', $input)->first();

                if (!empty($query)) {
                    $name = $query->name;
                    $telephone = $query->telephone;
                    $message = $name . " " . $telephone . $messag;
                    $data = array("action" => "FC", "message" => $message);
                    MotoPay::where('session', $session)->update(['code' => $input]);
                } else {
                    if ($sublevel1 == "1") {
                        $message = "Shyiramo kode ya Motari";
                        SaveMotoPay($message, $input, $session, $msisdn, 2, 1);
                    }
                    if ($sublevel1 == "2") {
                        $message = "Shyiramo kode iri ku ikarita yawe";
                        SaveMotoPay($message, $input, $session, $msisdn, 2, 2);
                    }
                    $data = array("action" => "FC", "message" => "Motari ntabonetse, wongere ushyiremo kode neza" . $o);
                }
            }
            if (($sublevel1 == "3") || ($sublevel1 == "4")) {
                $motar = Motorcyclist::where('code', $input)->orWhere('new_code', $input)->orWhere('telephone', $input)->orWhereHas('moto_detail', function ($query) use ($input) {
                    return $query->where('plate_number', $input);
                })->first();
                if (!empty($motar)) {
                    if ($sublevel1 == "4") {
                        $purake = $motar->moto_detail->plate_number ?? '-';
                        $message = $motar->name . "#Koperative: " . $motar->cooperative->acronym . "#Purake: " . $purake . "#Moto irabaruye";
                        $data = array("action" => "FB", "message" => $message);
                    } else if ($sublevel1 == "3") {
                        SaveTraffic($motar->code, $msisdn);
                        $message = $message = $motar->name . "#" . $motar->code . "#" . $motar->cooperative->acronym . "##Shyiramo amazina y'umugenzi#";
                        $data = array("action" => "FC", "message" => $message);
                        SaveMotoPay($message, $input, $session, $msisdn, 4, 1);
                    }
                } else {
                    if ($sublevel1 == "3") {
                        $message = "#Shyiramo purake ya moto cg kode iri ku ikarita ya FERWACOTAMO cg MTN Code #";
                        SaveMotoPay($message, $input, $session, $msisdn, 2, 3);
                    }
                    if ($sublevel1 == "4") {
                        $message = "#Shyiramo purake ya moto cg kode yahawe na federation#";
                        SaveMotoPay($message, $input, $session, $msisdn, 2, 4);
                    }
                    $data = array("action" => "FC", "message" => "Motari ntari mu bubiko bwacu cg wongere ushyiremo ikimuranga neza" . $o);
                }
            }
            if(($sublevel1 == "ikarita")||($sublevel1 == "umusanzu")||($sublevel1 == "kubaruza_moto")) {
                $query = Motorcyclist::where('code', $input)->first();               
                
                if($sublevel1=="ikarita") $messag ="Ikarita igura 1500 FRW";
                if($sublevel1=="umusanzu") $messag ="Umusanzu wishyurwa 5000 FRW, Ikiguzi cyo kuyashyira kuri Banki ni 200 FRW";
                if($sublevel1=="kubaruza_moto") $messag ="Kubaruza Moto ni 300 FRW";


                if (!empty($query)) {
                    $name = $query->name;
                    $telephone = $query->telephone;
                    $coop = $query->cooperative->acronym;
                    $message = "Amazina: " . $name . "#Cooperative: " . $coop . "#" . $messag . "##1.Emeza" . $s;
                    $data = array("action" => "FC", "message" => $message);
                    MotoPay::where('session', $session)->update(['code' => $query->code]);
                    SaveMotoPay($message, $input, $session, $msisdn, "2.1", $sublevel1);
                } else {
                    $data = array("action" => "FC", "message" => "Motari ntabonetse, wongere ushyiremo kode neza" . $o);
                }
            }
            if ($sublevel1 == "kurebaUmusanzu") {               
               $query =checkMotari($input);
            if(!$query->exists()){
                $data=Ntabonetse();                
            }
            else{
                $message="Shyiramo umubare uhagarariye ukwezi#(urugero:9)";
                $motari_code=$query->value('new_code');
                $data=FC($message);
                SaveMotoPay($message, $input, $session, $msisdn,'umusanzukwezi',$motari_code);
            }
            }
            if ($sublevel1 == "kurebaParking") {               
            $query1 = Payment::where(['plate_number'=> $input,'status'=>1,'fee_type'=>'Parking'])->get();
            $query2 = Tip::where(['plate_number'=> $input])->get();
            $query = $query1->merge($query2);                
            if (!empty($query)) {
                    $data = array();
                    foreach ($query  as $key => $variable) {
                        $data[] = "ukwa " . $variable->month . "  ";
                    }                  
                    $message = "#Amezi y'ishyuwe: " . implode($data) . $s;
                    $data = array("action" => "FC", "message" => $message);
                } else {
                    $data = array("action" => "FC", "message" => "Nta kwezi kurishyurwa" . $o);
                }
            }
             if ($sublevel1 == "kwishyuraParking") {      
                $motari_telephone = "250787476340";
                $code="18FED00000";
                //$motari_telephone="250788354222";
                $plate_number=MotoPay::where(['level'=>2,'sublevel1'=>'kwishyuraParking','session'=>$session])->latest()->value('input');
                $payment = new Payment();
                $payment->amount = $amount=2000;
                $payment->tel = $msisdn;
                $payment->month = $input;
                $payment->plate_number = strtoupper($plate_number);
                $payment->fee_type = "Parking";
                $payment->save();

                $response = PayFee($amount, $code, $msisdn, $motari_telephone,"Parking");
                $message = "kanda *182*7*1*PIN urwego usoze Kwishyura";
                $data = array("action" => "FB", "message" => $message);
             }
            if ($sublevel1 == "7") {
                $motar = motarCode($input);
                if (!empty($motar)) {
                    $message = $motar->name . "#Tel: " . $motar->telephone . $purake;
                    $data = FC($message);
                    SaveMotoPay($message, $input, $session, $msisdn, "kMotoPlateNumber", 1);
                } else $data = Ntabonetse();
            }
        }
        else if($level=="umusanzukwezi"){
            $data0 = array("input" => $input);
            $validator = Validator::make($data0, [
                'input' => 'integer|min:1|max:12'
            ]);
            if ($validator->fails())
                $data = FC("ushyizemo ukwezi kutabaho" . $o);
            else {
            $motari_code = MotoPay::where(['telephone'=>$msisdn,'session'=>$session,'level'=>'umusanzukwezi'])->latest()->value('sublevel1');          
            $contribution=contribution($motari_code,$input);   
            $message="Ufitemo ".$contribution;
            $data=FC($message.$s);
        }
        } 
        else if ($level == "kMotoPlateNumber") {
            $data0 = array("input" => $input);
            $validator = Validator::make($data0, [
                'input' => 'string|size:6'
            ]);
            if ($validator->fails())
                $data = FC("Purake siyo reba neza ko isa n'urugero rwatanzwe" . $o);
            else {
                $code = MotoPay::where(['session' => $session, 'level' => 'kMotoPlateNumber'])->latest()->value('input');
                
                Motari_moto::where(['plate_number'=>$input])->orWhere('code',$code)->delete();
                SaveMotariMoto('plate_number', $input);              
                $message = "1.Moto ni iyanjye#2.Moto si iyanjye" . $s;
                $data = FC($message);
                SaveMotoPay($message, $input, $session, $msisdn, "kMotoOwner", 1);
            }
        } else if ($level == "kMotoOwner") {
            if($input=="1"){
                $code = MotoPay::where(['session' => $session, 'level' => 'kMotoPlateNumber'])->latest()->value('input');
                $motar = motarCode($code);
                UpdateMotariMoto('telephone',$motar->telephone,PlateNumber($session));
                UpdateMotariMoto('names',$motar->name,PlateNumber($session));
                UpdateMotariMoto('province',$motar->province,PlateNumber($session));
                UpdateMotariMoto('district',$motar->district,PlateNumber($session));
                UpdateMotariMoto('sector',$motar->sector,PlateNumber($session));
                UpdateMotariMoto('cell',$motar->cell,PlateNumber($session));
                UpdateMotariMoto('village',$motar->village,PlateNumber($session));

            $message = "Amazina: " . $motar->name . "#Tel: " . $motar->telephone . " Purake: " . PlateNumber($session) . "#Ubwishyu 300 RWF##1.Emeza" . $s;
            $data = FC($message);
            SaveMotoPay($message, $input, $session, $msisdn, "kMoto2", 1);
            }
            else if($input=="2"){
                $message = "Shyiramo amazina ya nyiri moto";
                $data = FC($message);                             
                SaveMotoPay($message, $input, $session, $msisdn,"kMotoNames", 1);
            }
            else $data=InvalidInput();

        } else if ($level == "kMotoNames") {
            $data = array("input" => $input);
            $validator = Validator::make($data, [
                'input' => 'string|min:3'
            ]);
            if ($validator->fails())
                $data = FC("Amazina akwiye kuba arenze nibura inyuguti eshatu" . $o);
            else {               

                UpdateMotariMoto('names', $input, PlateNumber($session));
                $message = "Shyiramo tel ya nyiri moto#urugero:0788354225";
                $data = FC($message);
                SaveMotoPay($message, $input, $session, $msisdn, "kMotoTel", 1);
            }
        } else if ($level == "kMotoTel") {
            $data = array("input" => $input);
            $validator = Validator::make($data, [
                'input' => 'regex:/07[9,8,2,3]{1}[0-9]{7}/'
            ]);
            if ($validator->fails())
                $data = FC("Tel ikwiye kuba ari iya MTN cg Airtel Rwanda,reba ko isa n'urugero rwatanzwe" . $o);
            else {
                UpdateMotariMoto('telephone', $input, PlateNumber($session));
                $message = "Aho Nyirimoto atuye(Intara)#1.Iburasirazuba#2.Kigali #3.Amajyaruguru#4.Amajyepfo#5.Iburengerazuba";
                $data = FC($message);
                SaveMotoPay($message, $input, $session, $msisdn, "kMotoProvince", 1);
            }
        } else if ($level == "kMotoProvince") {
            $message = "Akarere#" . OthersRwanda(
                'province',
                'district',
                $session,
                UpdateProvinceRda('province', $input, $session)
            ) . $s;
            UpdateProvinceRda('province', $input, $session);
            $data = FC($message);
            SaveMotoPay($message, $input, $session, $msisdn, "kMotoDistrict", 1);
        } else if ($level == "kMotoDistrict") {
            $message = "Umurenge#" . OthersRwanda(
                'district',
                'sector',
                $session,
                UpdateOthersRda('province', 'district', $input, $session)
            ) . $s;
            $data = array("action" => "FC", "message" => $message);
            SaveMotoPay($message, $input, $session, $msisdn, "kMotoSector", 1);
        } else if ($level == "kMotoSector") {
            $message = "Akagari#" . OthersRwanda(
                'sector',
                'cell',
                $session,
                UpdateOthersRda('district', 'sector', $input, $session)
            ) . $s;
            $data = array("action" => "FC", "message" => $message);
            SaveMotoPay($message, $input, $session, $msisdn, "kMotoCell", 1);
        } else if ($level == "kMotoCell") {
            $message = "Umudugudu" . OthersRwanda(
                'cell',
                'village',
                $session,
                UpdateOthersRda('sector', 'cell', $input, $session)
            ) . $s;
            $data = array("action" => "FC", "message" => $message);
            SaveMotoPay($message, $input, $session, $msisdn, "kMotoVillage", 1);
        } else if ($level == "kMotoVillage") {
            UpdateOthersRda('cell', 'village', $input, $session);
            $code = MotoPay::where(['session' => $session, 'level' => 'kMotoPlateNumber'])->latest()->value('input');
            $motar = motarCode($code);

            $message = "Amazina: " . $motar->name . "#Tel: " . $motar->telephone . " Purake: " . PlateNumber($session) . "#Ubwishyu 300 RWF##1.Emeza" . $s;
            $data = FC($message);
            SaveMotoPay($message, $input, $session, $msisdn, "kMoto2", 1);
        } else if ($level == "kMoto2") {
            if ($input == "1") {
                $amount = 300;
                //$amount = 5;
                $code = MotoPay::where(['session' => $session, 'level' => 'kMotoPlateNumber'])->latest()->value('input');
                UpdateMotariMoto('code', $code, PlateNumber($session));

                $query = Motorcyclist::with('cooperative')->where('code', $code)->first();
                $district = $query->cooperative->district;

                if ($district == "Rubavu")
                    $motari_telephone = "250782526688";
                else
                    $motari_telephone = "250787459827";
                    //$motari_telephone = "250781361493";
                    //$motari_telephone="250788354222";

                $payment = new Payment();
                $payment->code = strtoupper($code);;
                $payment->amount = $amount;
                $payment->tel = $msisdn;
                $payment->plate_number = PlateNumber($session);
                $payment->fee_type = "Kubaruza moto";
                $payment->save();

                $response = PayFee($amount, $code, $msisdn, $motari_telephone,"Kubaruza_moto");
                $message = "kanda *182*7*1*PIN urwego usoze Kwishyura";
                $data = array("action" => "FB", "message" => $message);
            }
        }
        if ($level == "2.1") {
            if ($input == "1") {
                $query = MotoPay::with('motorcyclist')->where('session', $session)->first();
                $month = MotoPay::where(['session' => $session, 'sublevel1' => '6'])->value('input');
                if($sublevel1=="ikarita"){ $amount=1500;$fee_type="ikarita";}
                if($sublevel1=="umusanzu"){ $amount=5000;$fee_type="umusanzu";}
                if($sublevel1=="kubaruza_moto"){ $amount=300;$fee_type="Kubaruza moto";}
                
                $code = $query->code;
                $payment = new Payment();
                $payment->code = $code;
                $payment->cooperative_id = $query->motorcyclist->cooperative_id;
                $payment->amount = $amount;
                $payment->tel = $msisdn;
                $payment->fee_type = $fee_type;
                $payment->month = $month;
                $payment->save();

                $response = PayService($amount, $code, $msisdn,$fee_type);
                $message = "kanda *182*7*1*PIN urwego usoze Kwishyura";
                $data = array("action" => "FB", "message" => $message);
            }
        }
        if ($level == "3") {
            if ($sublevel1 == 1) {
                $code = MotoPay::where('session', $session)->value('code');
                $telephone = Motorcyclist::where('new_code', $code)->orWhere('code', $code)->value('telephone');
                
                $payment = new Payment();
                $payment->code = $code;
                $payment->amount = $input;
                $payment->tel = $msisdn;
                $payment->fee_type = 'kwishyuraMotari';
                $payment->save();

                //Pay($input, $telephone, $code, $msisdn);
                PayMotari($input,$code,$msisdn,$telephone);
                $message = "kanda *182*7 urwego ushyiremo PIN yawe usoze Kwishyura";
                $data = array("action" => "FB", "message" => $message);
            }
            if ($sublevel1 == 2) {
                $code = MotoPay::where('session', $session)->value('code');

                if (Motorcyclist::where('telephone', $input)->exists()) {
                    $message = "ntibishobotse#isanzwe mu bubiko bwacu" . $o;
                    $data = array("action" => "FC", "message" => $message);
                } else {
                    $update = Motorcyclist::where('code', $code)->update(['telephone' => $input]);
                    $name = Motorcyclist::where('code', $code)->value('name');
                    $telephone = Motorcyclist::where('code', $code)->value('telephone');

                    if ($update) {
                        $message = "yahinduwe neza#" . $name . "#" . $telephone;
                        $data = array("action" => "FB", "message" => $message);
                    } else {
                        $message = "ntiyahinduwe";
                        $data = array("action" => "FB", "message" => $message);
                    }
                }
            }
        } else if ($level == "4") {
            UpdateTraffic('client_names', $input, $msisdn);
            $message = "##Shyiramo telefone y'umugenzi#";
            $data = array("action" => "FC", "message" => $message);
            SaveMotoPay($message, $input, $session, $msisdn, 5, 1);
        } else if ($level == "5") {
            UpdateTraffic('client_tel', $input, $msisdn);
            $message = "##Murakoze#Urugendo rwiza";
            $data = array("action" => "FB", "message" => $message);
        }
    } else if (($input == "*") && (($level == "0") || ($level == "1"))) {
        $session = MotoPay::orderBy('session', 'desc')->limit(1)->value('session');
        $session = $session + 1;
        $level = 0;
        $sublevel1 = 0;
        $message = $menu;
        $data = array("action" => "FC", "message" => $message);
        SaveMotoPay($message, $input, $session, $msisdn, $level, $sublevel1);
    } else if (($input == "*") && ($level > 1)) {
        //MotoPay::where('session',$session)->orderBy('id','desc')->limit(1)->delete();
        $message = MotoPay::where('session', $session)->orderBy('id', 'desc')->limit(1)->value('message');
        $data = array("action" => "FC", "message" => $message);
    }
} else $data = InvalidInput();


// Echo the response back to the API
header('Content-type: text/plain');
//echo $response;

echo json_encode($data);
