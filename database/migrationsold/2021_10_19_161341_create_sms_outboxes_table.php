<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmsOutboxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sms_outboxes', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->string('sender', 50)->nullable();
            $table->string('cooperative_id', 50);
            $table->text('message');
            $table->integer('session');
            $table->integer('recipients');
            $table->integer('characters');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sms_outboxes');
    }
}
