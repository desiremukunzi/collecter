<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_transactions', function (Blueprint $table) {
            $table->unsignedBigInteger('id');
            $table->unsignedBigInteger('motorcyclist_id')->nullable();
            $table->unsignedBigInteger('retailer_id')->nullable();
            $table->unsignedMediumInteger('total_amount')->nullable();
            $table->unsignedMediumInteger('discount_amount')->nullable();
            $table->unsignedMediumInteger('client_amount')->nullable();
            $table->unsignedMediumInteger('our_amount')->nullable();
            $table->unsignedMediumInteger('amount_to_pay')->nullable();
            $table->unsignedMediumInteger('retailer_amount')->nullable();
            $table->string('telephone', 20)->nullable();
            $table->longText('request_response_body')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_transactions');
    }
}
