<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMotoDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('moto_details', function (Blueprint $table) {
            $table->id();
            $table->string('name', 255);
            $table->string('telephone', 20)->nullable();
            $table->string('plate_number', 255)->nullable();
            $table->string('province', 255)->nullable();
            $table->string('district', 255)->nullable();
            $table->string('sector', 255)->nullable();
            $table->string('cell', 255)->nullable();
            $table->string('village', 255)->nullable();
            $table->unsignedBigInteger('motorcyclist_id')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('moto_details');
    }
}
