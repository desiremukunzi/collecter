<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMotorcyclistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('motorcyclists', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('cooperative_id');
            $table->string('code', 255)->nullable()->unique('code');
            $table->char('new_code', 5)->nullable()->unique('new_code');
            $table->string('photo', 50)->nullable();
            $table->string('name', 255)->nullable();
            $table->string('telephone', 255)->nullable();
            $table->string('card_number', 255)->nullable();
            $table->enum('card_payed', ['0', '1']);
            $table->string('id_number', 255)->nullable();
            $table->string('permit_number', 255)->nullable();
            $table->double('share')->nullable();
            $table->string('gender', 20)->nullable();
            $table->string('province', 255)->nullable();
            $table->string('district', 255)->nullable();
            $table->string('sector', 255)->nullable();
            $table->string('cell', 255)->nullable();
            $table->string('village', 255)->nullable();
            $table->string('zone', 255)->nullable();
            $table->string('owner', 255)->nullable();
            $table->string('vest_number', 255)->nullable();
            $table->string('airtel_number', 20)->nullable();
            $table->timestamp('vest_time')->nullable();
            $table->longText('files')->nullable();
            $table->string('file_nid', 20)->nullable();
            $table->text('file_pid')->nullable();
            $table->string('track_username', 50)->nullable();
            $table->string('track_password', 50)->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('motorcyclists');
    }
}
