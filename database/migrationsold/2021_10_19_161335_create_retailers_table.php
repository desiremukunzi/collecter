<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRetailersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('retailers', function (Blueprint $table) {
            $table->id();
            $table->string('name', 255);
            $table->unsignedDecimal('discount_percentage', 5, 2);
            $table->unsignedDecimal('client_percentage', 5, 2);
            $table->string('secret_code', 30)->unique('retailers_secret_code_unique');
            $table->unsignedBigInteger('balance')->default(0);
            $table->string('telephone', 20)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('retailers');
    }
}
