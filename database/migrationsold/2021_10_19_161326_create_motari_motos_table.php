<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMotariMotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('motari_motos', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->string('plate_number', 10);
            $table->string('code', 10);
            $table->string('names', 50)->nullable();
            $table->string('telephone', 10)->nullable();
            $table->string('province', 50)->nullable();
            $table->string('district', 50)->nullable();
            $table->string('sector', 50)->nullable();
            $table->string('cell', 50)->nullable();
            $table->string('village', 50)->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('motari_motos');
    }
}
