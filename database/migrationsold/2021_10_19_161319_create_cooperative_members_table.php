<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCooperativeMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cooperative_members', function (Blueprint $table) {
            $table->unsignedInteger('id');
            $table->string('code');
            $table->string('name');
            $table->double('card_number')->nullable();
            $table->string('photo')->nullable();
            $table->string('telephone');
            $table->string('id_number');
            $table->string('permit_number');
            $table->double('share');
            $table->string('sex');
            $table->string('province');
            $table->string('district');
            $table->string('sector');
            $table->string('cell');
            $table->string('village');
            $table->string('counter');
            $table->unsignedInteger('cooperative_id');
            $table->string('zone')->nullable();
            $table->string('owner');
            $table->integer('status')->default(0);
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cooperative_members');
    }
}
