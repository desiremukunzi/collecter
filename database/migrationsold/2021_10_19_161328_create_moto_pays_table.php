<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMotoPaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('moto_pays', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->text('message')->nullable();
            $table->text('input');
            $table->string('level', 50);
            $table->string('sublevel1', 50);
            $table->integer('session');
            $table->string('code', 50)->nullable();
            $table->string('telephone', 20)->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('moto_pays');
    }
}
