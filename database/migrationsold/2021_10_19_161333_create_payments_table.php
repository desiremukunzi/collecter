<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->string('code', 50);
            $table->string('tel', 15);
            $table->integer('cooperative_id')->nullable();
            $table->integer('amount');
            $table->integer('month')->nullable();
            $table->string('transaction', 50)->nullable();
            $table->string('trxId', 50)->nullable();
            $table->string('bprRefNo', 50)->nullable();
            $table->string('fee_type', 50)->default('packing');
            $table->string('plate_number', 10)->nullable();
            $table->boolean('status')->default(0);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->dateTime('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
