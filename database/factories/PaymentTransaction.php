<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\PaymentTransaction;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(PaymentTransaction::class, function (Faker $faker) {
    return [
        'fee_type_id' => '2',
        'name' => $faker->name,
        'tin_number' => $faker->randomDigit,
        'amount' => $faker->randomDigit,
        'province' => 'Kigali City',
        'district' => 'Gasabo',
        'sector' => 'Kacyiru',
        'cell' => 'Kamatamu',
        'telephone' => $faker->e164PhoneNumber,

        'user_id' => 'ccc9cc9a-0d3c-4260-ba50-50a826eef9ac'

    ];
});
