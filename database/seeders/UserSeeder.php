<?php
namespace Database\Seeders;
use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create(
            [
                'name' => env('SUPER_ADMIN_NAME'),
                'email' => env('SUPER_ADMIN_EMAIL'),
                'telephone' => env('SUPER_ADMIN_TELEPHONE'),
                'otp_status' => true,
                'password' => bcrypt(env('SUPER_ADMIN_PASSWORD')),
            ]
        );

        $user->assignRole(['admin']);
    }
}
