<?php
namespace Database\Seeders;
use App\Role;
use Illuminate\Database\Seeder;

class RolesAndPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Roles

        Role::firstorCreate(['name' => 'admin', 'display_name' => 'Administrator' ]);
        Role::firstorCreate(['name' => 'regional_coordinator', 'display_name' => 'Regional Coordinator']);
        Role::firstorCreate(['name' => 'district_leader', 'display_name' => 'District Leader']);
        Role::firstorCreate(['name' => 'fee_collector', 'display_name' => 'Fee Collector']);

    }
}
