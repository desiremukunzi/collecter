<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::prefix('v1')->group(function () {
    Route::post('login', 'API\AndroidController@login');
    Route::post('payment', 'API\AndroidController@payment');
    Route::get('check', 'API\AndroidController@check');
    Route::get('balance', 'API\AndroidController@balance');
    Route::post('payment/response', 'API\AndroidController@paymentResponse');
});

Route::get("/motocyclist/{id}", "API\MotocyclistController@index");
//Route::post("/motari", "API\MotocyclistController@update");
Route::get("/motari_vest/{id}/{vest}/{airtel}", "API\MotocyclistController@update");
Route::post("/motari_files", "API\MotocyclistController@update_files");

Route::get("/checkbalance/{id}", "API\CheckBalanceController@index");
Route::get("/Response/{id}", "API\ResponseController@index");
Route::POST('/motocyclist/momo', 'PaymentsController@momo')->name('motocyclist.momo');
Route::POST('/paymentResponse', 'PaymentsController@paymentResponse')->name('motocyclist.paymentResponse');
Route::post("/updateTel", "API\MotocyclistController@updateTel");
Route::get('cooperatives', 'API\MotocyclistController@cooperativesMembers');
Route::post('/motariMomo', 'PaymentsController@momo')->name('motari.momo');

//Route::group(['middleware'=>'cors'], function() {
Route::get('cooperatives_list', 'API\MotocyclistController@cooperatives');
Route::get("/motari/{id}", "API\MotocyclistController@motari");
//});

