<?php


Route::get('/', 'HomeController@index')->name('index');
Route::get('/token', 'UsersController@generateToken');
Route::get('/country','CountryController@index')->name('country.dashboard');


Auth::routes(['verify' => true]);

Route::get('create-password', 'Auth\CreatePasswordController@index')->name('create-password-index');
Route::post('create-password', 'Auth\CreatePasswordController@store')->name('create-password');

Route::group(['middleware'=>'auth'], function() {

Route::group(['middleware'=>'administrator','prefix'=>'users'],function () {
    Route::get('/', 'UsersController@index')->name('users.index');
    Route::post('/', 'UsersController@store')->name('users.store');
    Route::post('/update', 'UsersController@update')->name('users.update');
    Route::post('/updateFew', 'UsersController@updateFew')->name('users.updateFew');
    Route::delete('/{user_id}', 'UsersController@destroy')->name('users.destroy');
    Route::post('/{user_id}', 'UsersController@restore')->name('users.restore');

});

Route::post('/missed-card-payment', 'PaymentsController@missedCardPayment')->name('missed-card-payment');
Route::prefix('cooperatives')->group(function () {
    Route::get('/', 'cooperativesController@index')->name('cooperatives.index');    
    Route::post('/', 'cooperativesController@store')->name('cooperatives.store');
    Route::post('/update', 'cooperativesController@update')->name('cooperatives.update');
    Route::post('/destroy', 'cooperativesController@destroy')->name('cooperatives.destroy'); 
});
Route::prefix('bonus')->group(function () {
Route::post('/store', 'BonusController@store')->name('bonus.store');
});
Route::post('sendSms','SmsController@sms')->name('sms');
Route::post('sms_destroy','SmsController@sms_destroy')->name('sms_destroy');
Route::resource('sms','SmsController');


Route::get('/deleted_users', 'UsersController@deleted_users')->name('users.deleted');


Route::prefix('ajax')->group(function() {
    Route::get('users', 'UsersController@getUsers');
    Route::get('deleted_users', 'UsersController@getDeletedUsers');
    Route::get('districts', 'DataController@getDistricts');
    Route::get('sectors', 'DataController@getSectors');
    Route::get('cells', 'DataController@getCells');
    Route::get('villages', 'DataController@getVillages');
});
Route::get('/edit/{id}', 'UsersController@edit')->name('users_edit');


//Route::get('/login_page', function () { return view('pages.login');})->name('login_page');
//Route::get('/country','CountryController@index')->name('country');
Route::get('/district/{id}', 'DistrictController@index')->name('district');
Route::get('/province/{id}', 'ProvinceController@index')->name('province');
Route::get('/sector/{id}', 'SectorController@index')->name('sector');
Route::get('/cooperative/{id}', 'CooperativeController@index')->name('cooperative');
Route::get('/cooperatives_list/{id}', 'CooperativeController@cooperatives')->name('cooperatives');
Route::post('/update_cooperative', 'cooperativesController@update')->name('update_cooperative');
Route::post('/register_cooperative', 'cooperativesController@store')->name('register_cooperative');
//Route::get('/home', 'HomeController@index')->name('home');
Route::post('/update_user', 'UsersController@update')->name('update_user');
Route::post('/register_user', 'UsersController@create')->name('register_user');
Route::get('/password_generate/{id}', 'UsersController@password_generate')->name('password_generate');
Route::post('/password_update', 'UsersController@password_update')->name('password_update');
//Route::post('dynamic_dependent/fetch', 'UsersController@fetch')->name('dynamicdependent.fetch');

//Route::get('/test', 'PaymentsController@test')->name('cooperatives');

Route::resource('motorcyclist', 'MotorcyclistController');
Route::post('/motorcyclist_update', 'MotorcyclistController@update')->name('motorcyclist_update');
Route::post('/motorcyclist_delete', 'MotorcyclistController@destroy')->name('motorcyclist_delete');

Route::get('/card/{type}/{id}', 'MotorcyclistController@card')->name('card');

// Route::get('/cooperative_cards/{id}', 'MotorcyclistController@cooperative_cards')->name('cooperative_cards');
Route::post('/card_id', 'MotorcyclistController@card_id')->name('card_id');
Route::get('/reloadFailed', 'BprResponseController@reloadFailed')->name('reloadFailed');
});
Route::view('/search', 'pages.search')->name('search');
Route::any("/motari", "API\MotocyclistController@info")->name('info');

// Route::prefix('v1')->group(function () {    
//     Route::post('validate', 'API\MotocyclistController@motari_equity');
// });



