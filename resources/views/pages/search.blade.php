<!DOCTYPE html>
<html>
<head>
  <title>search</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
  
  <link href="{{ asset('message.css')}}" rel="stylesheet" type="text/css"/>

</head>
<body class="bg-light">
  <div class="container container-table bg-default">
    <div class="row vertical-center-row justify-content-center">
          @include('include.messages')  


 <form method="post" action="{{route('info')}}">
  @csrf  
<div class="card">
  <div class="card-body">
  <div class="card-title text-center"><h3>SHAKA MOTARI</h3></div>
   <div class="input-group text-center col-md-auto">  
    <input type="text" class="form-control" placeholder="shyiramo kode cg tel cg purake" name="identifier">
    <div class="input-group-append">
     <button class="btn btn-info" type="submit"><i class="fa fa-search"></i></button>
    </div>
  </div>
</div>
  </div>
 </form>
</div>
</div>
 
</body>
</html>

           
        
                
 

                    