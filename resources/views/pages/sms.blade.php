@extends('layouts.master')
@section('title', 'sms')
@section('content')
<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
</div>
<!-- end:: Subheader -->
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <!--Begin::Dashboard 3-->
    
    <!--Begin::Section-->
    <div class="row justify-content-center">
        <div class="col-md-auto">
            <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile justify-content-center">
                            <div
                            class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                            <div class="kt-portlet__head-label">
                        <center>
                        <button type="button" class="btn btn-primary text-center" data-c_id="Bose" data-origin="all" data-toggle="modal" data-target="#sms_modal">
                        <i class="la la-envelope"></i>
                        SMS to All
                        </button>  
                    </center>
                        
                    </div>

</div>
<div class="kt-portlet__body">
                    
                    <table class="table table-striped table-condensed table-hover table-checkable"
                        id="data-table-sms">
                        <thead class="bg bg-light">
                            <tr>
                                <th>#</th>
                                <th>Uwohereje</th>
                                <th>Abakira</th>
                                <th>Abakira Umubare</th>
                                <th>Ubutumwa</th>
                                <th>Umubare w'inyuguti</th>
                                <th>Igihe</th>
                                <th width="100px">Ibikorwa</th>
                            </tr>
                        </thead>
                    </table>
                    <!--end: Datatable -->
                </div>
            </div>
        </div>
    </div>
</div>

@include('include.edit_sms_modal')
@include('include.delete_sms_modal')
@endsection
{{-- 
@push('end_scripts')
@include('include.edit_cooperative_modal_js')
@include('include.sms_modal_js')
@endpush --}}
@push('end_scripts')
<script type="text/javascript">
$(function () {
var table = $('#data-table-sms').DataTable({
processing: true,
serverSide: true,
ajax: "{{ route('sms.index') }}",
columns: [
{data: 'DT_RowIndex', name: 'DT_RowIndex'},
{data: 'sender', name: 'sender'},
{data: 'cooperative_id', name: 'cooperative_id'},
{data: 'recipients', name: 'recipients'},
{data: 'message', name: 'message'},
{data: 'characters', name: 'characters'},
{data: 'updated_at', name: 'updated_at'},
{data: 'actions', name: 'actions', orderable: false, searchable: false},
]
});
});
</script>
@endpush