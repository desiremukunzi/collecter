@extends('layouts.master')

@section('title', 'Country Level')

@section('content')

    @include('include.subheader')
    @push('styles')
<style>
/* Style the tab */
.tab {
overflow: hidden;
/*border: 1px solid #00f;*/
/*background-color: #f1f1f1;*/
}
/* Style the buttons inside the tab */
.tab button {
background-color: inherit;
float: left;
border: none;
outline: none;
cursor: pointer;
padding: 7px 8px;
transition: 0.3s;
font-size: 18px;
}
/* Change background color of buttons on hover */
.tab button:hover {
background-color: #ddd;
}
/* Create an active/current tablink class */
.tab button.active {
background-color: #ccf;
}
/* Style the tab content */
.tabcontent {
display: none;
/*padding: 6px 12px;
border: 1px solid #ccc;
border-top: none; */
}
</style>
@endpush

    <!-- begin:: Content -->
    <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
        <!--Begin::Dashboard 3-->
        <div class="row">
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-12">
                        <div>
                            <div class="col-md-15">
                                <div class="kt-portlet kt-portlet--height-fluid-half kt-portlet--border-bottom-brand">
                                    <div class="kt-portlet__body kt-portlet__body--fluid">
                                        <div class="kt-widget26">
                                            <div class="kt-widget26__content">
                                                <a href="#" style="color: #a7abc3">
                                                    
                                                    <span class="kt-widget26__desc"><a href="#">{{$group="Kigali City"}}</a></span>
                                                      <span class="kt-widget26__number" style="color: #646c9a">{{count_members('province',$group)}}</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-15">
                                <div class="kt-portlet kt-portlet--height-fluid-half kt-portlet--border-bottom-success">
                                    <div class="kt-portlet__body kt-portlet__body--fluid">
                                        <div class="kt-widget26">
                                            <div class="kt-widget26__content">
                                                <a href="#" style="color: #a7abc3">
                                                   
                                                    <span class="kt-widget26__desc"><a
                                                            href="#">{{$group="Northern Province"}}</a></span>
                                                            <span class="kt-widget26__number" style="color: #646c9a">{{count_members('province',$group)}}</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-15">
                                <div class="kt-portlet kt-portlet--height-fluid-half kt-portlet--border-bottom-warning">
                                    <div class="kt-portlet__body kt-portlet__body--fluid">
                                        <div class="kt-widget26">
                                            <div class="kt-widget26__content">
                                                <a href="#" style="color: #a7abc3">
                                                    
                                                    <span class="kt-widget26__desc"><a
                                                            href="#">{{$group="Southern Province"}}</a></span>
                                                            <span class="kt-widget26__number" style="color: #646c9a">{{count_members('province',$group)}}</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-15">
                                <div class="kt-portlet kt-portlet--height-fluid-half kt-portlet--border-bottom-danger">
                                    <div class="kt-portlet__body kt-portlet__body--fluid">
                                        <div class="kt-widget26">
                                            <div class="kt-widget26__content">
                                                <a href="#" style="color: #a7abc3">
                                                    
                                                    <span class="kt-widget26__desc"><a
                                                            href="#">{{$group='Eastern Province'}}</a></span>
                                                            <span class="kt-widget26__number" style="color: #646c9a">{{count_members('province',$group)}}</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-15">
                                <div class="kt-portlet kt-portlet--height-fluid-half kt-portlet--border-bottom-info">
                                    <div class="kt-portlet__body kt-portlet__body--fluid">
                                        <div class="kt-widget26">
                                            <div class="kt-widget26__content">
                                                <a href="#" style="color: #a7abc3">
                                                   
                                                    <span class="kt-widget26__desc"><a
                                                            href="#">{{$group='Western Province'}}</a></span>
                                                            <span class="kt-widget26__number" style="color: #646c9a">{{count_members('province',$group)}}</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    
                <!--Begin::Section-->
{{--                 @include('include.dataintables')
 --}}
<!-- transactions--> 



<div class="row">
    <div class="col-md-12">
         @include('include.dataintable')
            </div>
        </div>
    </div>





<!-- end -->
            
            <div class="col-md-3">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                <center>uturere</center>
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <!--begin::Timeline 1-->
                        <div class="kt-list-timeline">
                            <div class="kt-list-timeline__items">
                                @foreach($districts as $district)
                                    <div class="kt-list-timeline__item">
                                        <span class="kt-list-timeline__badge {{ randomTimeLineBadge() }}"></span>   

                                        <span class="kt-list-timeline__text"><a href="#" data-c_id=" {{$district->district}} " data-origin="district" data-toggle="modal" data-target="#sms_modal"><i class="la la-envelope"></i></a>
                                        </span>

                                        <span class="kt-list-timeline__text align-left"><a
                                                href="/cooperative/{{$district->district}}">{{$group_district=$district->district}}</a>
                                                <span class="pull-right">{{count_members('district',$group_district)}}</span>
                                            </span>
                                                
                                            
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <!--end::Timeline 1-->
                        <div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"><br>
                            @if(Auth::user()->role==1)
            <a href="{{route('reloadFailed')}}"><center>Reload failed({{countBprFailed()}})</center></a>
            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end:: Content -->
@endsection

@push('end_scripts')
    @include('include.sms_modal_js') 


{{--     @include('include.datatable_actions_members')   
 --}}   
@endpush
