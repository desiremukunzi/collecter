<!DOCTYPE html>
<html>
    <head>
        <title>Info</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <link href="{{ asset('message.css')}}" rel="stylesheet" type="text/css"/>
    </head>
    <style>
        .table-borderless{
            border:none !important;
        }
    
    </style>
    <body class="bg-light">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 md-offset-2">
                    @include('include.messages')
                    <div class="card">
                        <div class="card-header bg-default">
                            <a class="btn btn-light pull-left" href="{{url()->previous()}}">Back</a>
                            @auth
                            @unlessrole('traffic')
                            {{-- <a class="btn btn-light pull-left" href="{{route('country.dashboard')}}">Back</a>
                            --}}
                            <a class="btn btn-light pull-left " data-id="{{$datas->id}}" data-name="{{$datas->name}}"
                                data-telephone="{{$datas->telephone}}" data-photo="{{$datas->photoExists()}}"
                                data-province="{{$datas->province}}" data-district="{{$datas->district}}"
                                data-sector="{{$datas->sector}}"
                                data-cooperative_id="{{$datas->cooperative_id}}" data-cell="{{$datas->cell}}"
                                data-code="{{$datas->code}}"
                                data-village="{{$datas->village}}" data-zone="{{$datas->zone}}"
                                data-id_number="{{$datas->id_number}}" data-permit_number="{{$datas->permit_number}}"
                                data-share="{{$datas->share}}" data-gender="{{$datas->gender}}"
                                data-owner="{{$datas->owner}}"
                                data-owner="{{$datas->owner}}"
                                data-card_number="{{$datas->card_number}}"
                                data-cooperative_name="{{$datas->cooperative->cooperative_name}}"
                                data-plate_number="{{$datas->moto_detail->plate_number ?? ''}}"
                                data-owner_name="{{$datas->moto_detail->name ?? ''}}"
                                data-owner_telephone="{{$datas->moto_detail->telephone ?? ''}}"
                                data-owner_province="{{$datas->moto_detail->province ?? ''}}"
                                data-owner_district="{{$datas->moto_detail->district ?? ''}}"
                                data-owner_sector="{{$datas->moto_detail->sector ?? ''}}"
                                data-owner_cell="{{$datas->moto_detail->cell ?? ''}}"
                                data-owner_village="{{$datas->moto_detail->village ?? ''}}"
                                data-toggle="modal" data-target="#edit_member_modal">Edit
                            </a>
                            @endunlessrole
                            @endauth
                            <h3><center>{{$datas->name}}</center></h3>
                        </div>
                        <div class="card-body">
                            <table class="table  table-hover">
                                <tr>
                                    <td>
                                        <center><img src="{{$datas->photoExists()}}" alt="" style="max-width: 100%;
                                        max-height: 100%;border-radius: 25px; width: 100px;
                                        height: 100px; " /></center>
                                        <div class="my-1 justify-content-center">
                                            <div style="vertical-align: bottom; text-align: center">
                                                <ul class="list-inline">
                                                    <li>
                                                        <badge class="badge badge-info ">
                                                        <i class="glyphicon glyphicon-phone"></i>
                                                        <span>{{$datas->telephone}}</span>
                                                        </badge>
                                                    </li>
                                                    <li><badge class="badge badge-info">
                                                        <i class="glyphicon glyphicon-qrcode"></i>
                                                        <span>{{$datas->code}}</span>
                                                        </badge>
                                                    </li>
                                                    <li><badge class="badge badge-info">
                                                        <i class="glyphicon glyphicon-road"></i>
                                                        <span>{{$datas->moto_detail->plate_number ?? '-'}}</span>
                                                        </badge>
                                                    </li>
                                                </ul>
                                            </div>
                                            
                                        </td>
                                        <td>
                                            <table class="table table-hover table-borderless">
                                                <tr>
                                                    <td><strong>INTARA</strong></td>
                                                    <td>: {{$datas->province}}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>AKARERE</strong></td>
                                                    <td>: {{$datas->district}}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>UMURENGE</strong></td>
                                                    <td>: {{$datas->sector}}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>AKAGARI</strong></td>
                                                    <td>: {{$datas->cell}}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>UMUDUGUDU</strong></td>
                                                    <td>: {{$datas->village}}</td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <table class="table table-hover table-borderless">
                                                
                                                <tr>
                                                    <td><strong>KOPERATIVE</strong></td>
                                                    <td>: {{$datas->cooperative->acronym}}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>ZONE</strong></td>
                                                    <td>: {{$datas->zone ?? '-'}}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>INDANGAMUNTU</strong></td>
                                                    <td>: {{$datas->id_number}}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>PERIMI</strong></td>
                                                    <td>: {{$datas->permit_number}}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>NYIRIMOTO</strong></td>
                                                    <td>: {{$owner=$datas->owner}}</td>
                                                </tr>
                                            </table>
                                            
                                        </td>
                                    </tr>
                                    @if($owner=='Sinjye')
                                    <tr>
                                        <td colspan="2">
                                            <table class="table table-hover table-borderless">
                                                <tr>
                                                    <td><strong>NYIRIMOTO</strong></td>
                                                    <td>: {{$datas->moto_detail->name ?? '-'}}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>TEL</strong></td>
                                                    <td>: {{$datas->moto_detail->telephone ?? '-'}}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>PROVINCE</strong></td>
                                                    <td>: {{$datas->moto_detail->province ?? '-'}}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>DISTRICT</strong></td>
                                                    <td>: {{$datas->moto_detail->district ?? '-'}}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>SECTOR</strong></td>
                                                    <td>: {{$datas->moto_detail->sector ?? '-'}}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>CELL</strong></td>
                                                    <td>: {{$datas->moto_detail->cell ?? '-'}}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>VILLAGE</strong></td>
                                                    <td>: {{$datas->moto_detail->village ?? '-'}}</td>
                                                </tr>
                                            </table>
                                            
                                        </td>
                                    </tr>
                                    @endif
                                    @auth
                                    <tr>
                                        <td colspan="2"><strong><center>Kureba aho moto iri</center></strong><br>
                                            @if($datas->track_username!=NULL)
                                            <center><a href="https://www.whatsgps.com" target="_blank">Kanda hano</a> Username ukoreshe:  {{$datas->track_username}}, Password ukoreshe: {{$datas->track_password}}</center>
                                            @else
                                            <center>GPS ntirashyirwa kuri moto</center>
                                            @endif
                                        </td>
                                    </tr>
                                    @endauth
                                </table>
                            </div>
                            <div class="card-footer text-center">{{date('Y')}}&nbsp;&copy;&nbsp;FERWACOTAMO</div>
                        </div>
                    </div>
                </div>
            </div>
            @include('include.edit_member_modal')
            @include('include.location')
        </body>
    </html>