    @extends('layouts.master')
    @section('title', 'Users2')
    @push('styles')
    <style type="text/css">
        .select2-container--default .select2-selection--single .select2-selection__rendered {
            background: #e1e3ec;
        }
        .select2-container {
            margin-right: 10px;
        }
        .btn.btn-clean i {
            color: #2dc7bf;
        }
    </style>
    @endpush
    @section('content')
    <!-- begin:: Subheader -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
    </div>
    <!-- end:: Subheader -->
    <!-- begin:: Content -->
    <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
        <!--Begin::Dashboard 3-->
        <div class="row">
            <div class="col-md-12">
                @include('include.messages')
                <!--Begin::Section-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                            <div
                            class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                            <div class="kt-portlet__head-label">
                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#kt_modal_4">
                                <i class="flaticon2-plus-1"></i>
                                Add New User
                            </button>
{{--                             @include('include.add_user_modal')
 --}}            <!--begin::Edit Modal-->
            <div class="modal fade" id="edit_modal" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">test</h5>
                        <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <!--begin::Form-->
                    <form class="kt-form kt-form--label-right" method="POST"
                    action="{{route('update_user')}}">
                    @csrf
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <label>Full Name2:</label>
                            <input type="hidden" name="id" id="id"
                            class="form-control"
                            placeholder="Enter full name">
                            <div class="kt-input-icon">
                                <input type="text" name="name" id="full_name"
                                class="form-control"
                                placeholder="Enter full name">
                                <span
                                class="kt-input-icon__icon kt-input-icon__icon--right"><span><i
                                    class="flaticon2-user"></i></span></span>
                                </div>
                                <span class="form-text text-muted">Please enter your full name</span>
                            </div>
                            <div class="col-lg-6">
                                <label class="">Phone Number:</label>
                                <div class="kt-input-icon">
                                    <input type="text" name="telephone" id="telephone"
                                    class="form-control"
                                    placeholder="Enter phone number">
                                    <span
                                    class="kt-input-icon__icon kt-input-icon__icon--right"><span><i
                                        class="flaticon2-phone"></i></span></span>
                                    </div>
                                    <span class="form-text text-muted">Please enter your phone number</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label>E-mail:</label>
                                    <div class="kt-input-icon">
                                        <input type="email" name="email" id="email"
                                        class="form-control"
                                        placeholder="Enter your email address">
                                        <span
                                        class="kt-input-icon__icon kt-input-icon__icon--right"><span><i
                                            class="flaticon2-email"></i></span></span>
                                        </div>
                                        <span class="form-text text-muted">Please enter your email address</span>
                                    </div>
                                    <div class="col-lg-6">
                                        <label class="">Role:</label>
                                        <select class="form-control kt-selectpicker"
                                        name="role">
                                        @foreach($roles as $role)
                                        <option>{{$role->name}}</option>
                                        @endforeach
                                    </select>
                                    <span class="form-text text-muted">Please select your role</span>
                                </div>
                            </div>
                            <b>Deployment : </b>
                            <br clear="left"><br>
                            <div class="form-group row">
                                <div class="col-lg-4">
                                    <label class="">Province:</label>
                                    <select name="province" id="province"
                                    class="form-control kt-selectpicker dynamic"
                                    data-dependent="district">
                                    <option value="" disabled="disabled">Select
                                        province
                                    </option>
                                    @foreach($province_list as $province)
                                    <option
                                    value="{{ $province->province}}">{{ $province->province }}</option>
                                    @endforeach
                                </select>
                                <span class="form-text text-muted">Please select your province</span>
                            </div>
                            <div class="col-lg-4">
                                <label class="">District :</label>
                                <select name="district" id="district"
                                class="form-control kt-selectpicker dynamic"
                                data-dependent="sector">
                                <option value="" disabled="disabled">Select
                                    district
                                </option>
                            </select>
                            <span class="form-text text-muted">Please select your district</span>
                        </div>
                        <div class="col-lg-4">
                            <label class="">Sector :</label>
                            <select name="sector" id="sector"
                            class="form-control kt-selectpicker">
                            <option value="" disabled="disabled">Select sector
                            </option>
                        </select>
                        <span class="form-text text-muted">Please select your sector</span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary"
                data-dismiss="modal">Close
            </button>
            <input type="submit" name="update " value="Update User"
            class="btn btn-primary">
        </form>
    </div>
</div>
</div>
</div>
{{-- end edit modal --}}
</div>
<div class="kt-portlet__head-toolbar kt-grid__item--fluid">
    <div class="kt-portlet__head-actions">
        
        
        <form class="form-inline">
            <select class="form-control kt-select2" name="param">
                <option>All Roles</option>
                <option>Administrators</option>
                <option>Regional Coordinators</option>
                <option>District Leaders</option>
                <option>Fee Collectors</option>
            </select>
            <select class="form-control kt-select2" name="param">
                <option>All Provinces</option>
                <option>Transport Fee</option>
                <option>Market Fee</option>
                <option>Fine Fee</option>
            </select>
            <select class="form-control kt-select2" name="param">
                <option>All Districts</option>
                <option>Transport Fee</option>
                <option>Market Fee</option>
                <option>Fine Fee</option>
            </select>
            <select class="form-control kt-select2" name="param">
                <option>All Sectors</option>
                <option>Transport Fee</option>
                <option>Market Fee</option>
                <option>Fine Fee</option>
            </select>
            <a href="#" class="btn btn-primary">
                <i class="flaticon2-refresh"></i>
                Filter
            </a>
        </form>
    </div>
</div>
</div>
<div class="kt-portlet__body">
    
    <table class="table table-striped- table-bordered table-hover table-checkable"
    id="kt_table_1_2">
    <thead>
        <tr>
            <th>#</th>
            <th>Names</th>
            <th>Role</th>
            <th>Phone</th>
            <th>E-mail</th>
            <th>Deployment</th>
            <th>Edit</th>
        </tr>
    </thead>
    <tbody>
        @php $index=1 @endphp
        @foreach($users as $user)
        @php $id=$user->id @endphp
        <tr>
            <td style="text-align: center;">{{$index}}</td>
            <td>{{$name=$user->name}}</td>
            <td>{{$role=$user->roles()->pluck('name')->implode(' ')}}</td>
            <td>{{$telephone=$user->telephone}}</td>
            <td>{{$email=$user->email}}</td>
            <td>{{$province=$user->province.','.$district=$user->district.','.$sector=$user->sector}}</td>
            <td>
                <button type="button" class="btn btn-primary"
                data-full_name="{{$name}}" data-telephone="{{$telephone}}"
                data-email="{{$email}}" data-role="{{$role}}"
                data-province="{{$province}}" data-district="{{$district}}"
                data-sector="{{$sector}}" data-id="{{$id}}"
                data-toggle="modal" data-target="#edit_modal">Edit
            </button>
        </td>
    </tr>
    @php $index++ @endphp
    @endforeach
</tbody>
</table>
<!--end: Datatable -->
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- end:: Content -->
@endsection
@push('end_scripts')

<script>
    $(document).ready(function () {
        let roleSelectHTML = $("#role_select");
        let provinceHTML = $("#province");
        let districtHTML = $("#district");
        let sectorHTML = $("#sector");
                            // role change affecting deplyoyment
                            roleSelectHTML.change(function () {
                                let val = $(this).val();
                                switch (val) {
                                    case 'admin':
                            // if administrator disable all deployment fields
                            districtHTML.html('');
                            sectorHTML.html('');
                            provinceHTML.attr('disabled', true);
                            districtHTML.attr('disabled', true);
                            sectorHTML.attr('disabled', true);
                            break;
                            case 'regional_coordinator':
                            // if regional coordinator enable province only
                            provinceHTML.attr('disabled', false);
                            districtHTML.html('');
                            sectorHTML.attr('');
                            districtHTML.attr('disabled', true);
                            sectorHTML.attr('disabled', true);
                            break;
                            case 'district_leader':
                            // if regional coordinator enable province and district only
                            provinceHTML.attr('disabled', false);
                            districtHTML.attr('disabled', false);
                            sectorHTML.html('');
                            sectorHTML.attr('disabled', true);
                            break;
                            case 'fee_collector':
                            // if administrator disable all deployment fields
                            provinceHTML.attr('disabled', false);
                            districtHTML.attr('disabled', false);
                            sectorHTML.attr('disabled', false);
                            break;
                        }
                            // reset deployment form
                            provinceHTML.val('');
                            districtHTML.html('');
                            sectorHTML.html('');
                        });
                            // province change
                            provinceHTML.change(function () {
                            // empty districts
                            districtHTML.html('');
                            sectorHTML.html('');
                            // check if role is district leader or fee collector
                            if (roleSelectHTML.val() !== 'district_leader' && roleSelectHTML.val() !== 'fee_collector')
                                return;
                            districtHTML.html('<option value="">Please wait ...</option>');
                            // get districts from the server
                            $.get('/ajax/districts/?province=' + $(this).val(), function (data, status) {
                                districtHTML.html('');
                                districtHTML.html('<option value="">-- Select Here ---</option>');
                                if (status !== 'success')
                                    return;
                                for (let i = 0; i < data.length; i++) {
                                    districtHTML.append('<option>' + data[i].name + '</option>');
                                }
                            })
                        });
                            // District Change
                            districtHTML.change(function () {
                            // empty sectors
                            sectorHTML.html('');
                            if (roleSelectHTML.val() !== 'fee_collector')
                                return;
                            sectorHTML.html('<option value="">Please wait ...</option>');
                            // get sectors from the server
                            $.get('/ajax/sectors/?district=' + $(this).val(), function (data, status) {
                                sectorHTML.html('');
                                sectorHTML.html('<option value="">-- Select Here ---</option>');
                                if (status !== 'success')
                                    return;
                                for (let i = 0; i < data.length; i++) {
                                    sectorHTML.append('<option>' + data[i].name + '</option>');
                                }
                            })
                        });
                            $('.kt-select2').select2();
                            var table = $('#kt_table_1_2');
                            // begin first table
                            table.DataTable({
                                responsive: true,
                                columnDefs: [
                                {
                                    targets: -1,
                                    title: 'Actions',
                                    orderable: false,
                                    render: function (data, type, full, meta) {
                                        return `
                                        <span class="dropdown">
                                        <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
                                        <i class="la la-ellipsis-h"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="#"><i class="la la-edit"></i> Edit Details</a>
                                        <a class="dropdown-item" href="#"><i class="la la-leaf"></i> Update Status</a>
                                        <a class="dropdown-item" href="#"><i class="la la-print"></i> Generate Report</a>
                                        </div>
                                        </span>
                                        <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                                        <i class="la la-edit"></i>
                                        </a>`;
                                    },
                                }
                                ],
                                initComplete: function () {
                                    $("#kt_table_1_2_length").prepend('<div class="dropdown dropdown-inline"><button type="button" class="btn btn-label-success btn-sm btn-bold dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="flaticon-more-1"></i> </button> <div class="dropdown-menu dropdown-menu-right"> <ul class="kt-nav"> <li class="kt-nav__section kt-nav__section--first"> <span class="kt-nav__section-text">Export Tools</span> </li> <li class="kt-nav__item"> <a href="#" class="kt-nav__link" id="export_print"> <i class="kt-nav__link-icon la la-print"></i> <span class="kt-nav__link-text">Print</span> </a> </li> <li class="kt-nav__item"> <a href="#" class="kt-nav__link" id="export_copy"> <i class="kt-nav__link-icon la la-copy"></i> <span class="kt-nav__link-text">Copy</span> </a> </li> <li class="kt-nav__item"> <a href="#" class="kt-nav__link" id="export_excel"> <i class="kt-nav__link-icon la la-file-excel-o"></i> <span class="kt-nav__link-text">Excel</span> </a> </li> <li class="kt-nav__item"> <a href="#" class="kt-nav__link" id="export_csv"> <i class="kt-nav__link-icon la la-file-text-o"></i> <span class="kt-nav__link-text">CSV</span> </a> </li> <li class="kt-nav__item"> <a href="#" class="kt-nav__link" id="export_pdf"> <i class="kt-nav__link-icon la la-file-pdf-o"></i> <span class="kt-nav__link-text">PDF</span> </a> </li> </ul> </div> </div> &nbsp;&nbsp;&nbsp;')
                                }
                            });
                        });
                    </script>
                    @endpush
                    