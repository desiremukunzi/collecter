<!DOCTYPE html>
<html>
<head>
    <title>Print Card</title>
    <style> 
@media print {

    @page {size: legal landscape;margin: 0;}
    #print {page-break-before: always;height: 100%; width: 100%;} 
    #content {page-break-before: always;height: 100%; width: 100%;font-size: 30px;} 

    
}
table td.shrink {
    white-space:nowrap
}
table td.expand {
    width: 99%
}

    </style>
</head>
<body onload="window.print()" onafterprint="javascript:history.go(-1)">
  @foreach($datas as $data)
    <div id="print" style="background-image:url({{url('photo/background/bg.png')}});
/*      position:fixed;
*/      margin-top:0px;
        background-size: cover;
        margin-left:0px;
        width: 1344px;
        height: 810px;">  
 
  
<div id="content" style="
      width: 80%;
      height: 50%;
      top: 35%;
      margin: 0 auto;
      position: relative;
      font-family: Arial, Helvetica, sans-serif;font-weight: bold;">

<table border="0" width="100%" height="100%">
    <tr>
        <td width="20%"><img src="/photo/{{$data->photo}}" style="max-width: 100%;
    max-height: 100%;"></td>
    <td width="1%"></td>
        <td width="79%">
            <table border="0" width="100%" height="100%">
                <tr>
                    <td colspan="2"><center>{{$data->cooperative->district}}</center></td>
                </tr>
                <tr>
                    <td width="20%">Names</td><td>: {{$data->name}}</td>
                </tr>
                <tr>
                <td width="20%">Cooperative</td><td>: {{$data->cooperative->acronym}}</td>
               </tr>
               <tr>
                    <td width="20%">Kode</td><td>: {{$data->code}}</td>
               </tr> 
               <tr>
                    <td width="20%">Izarangira</td><td>: {{'06/2022'}}</td>
               </tr>
               <tr>
               </tr>
               <tr>                    
               </tr>            
            </table>
        </td>
    </tr>
</table>
</div>
            </div>
            @endforeach
</body>
</html>

           
        
                
 

                    