@extends('layouts.master')
@section('content')
@include('include.subheader')

@include('include.filter')

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <!--Begin::Dashboard 3-->
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="kt-portlet kt-portlet--height-fluid">
                                <div class="kt-widget14">
                                    <div class="kt-widget14__header kt-margin-b-30">
                                        <h3 class="kt-widget14__title">
                                        Daily Transactions
                                        </h3>
                                        <span class="kt-widget14__desc">
                                            Check out each collumn for more details
                                        </span>
                                    </div>
                                    <div class="kt-widget14__chart" style="height:120px;">
                                        <canvas id="kt_chart_daily_sales_2"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="kt-portlet kt-portlet--height-fluid">
                                <div class="kt-portlet__head">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title">
                                        Payment Statistics
                                        </h3>
                                    </div>
                                </div>
                                <div class="kt-portlet__body kt-portlet__body--fluid">
                                    <div class="kt-widget12">
                                        <div class="kt-widget12__chart" style="height:250px;">
                                            <canvas id="kt_chart_order_statistics_2"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end:: Widgets/Inbound Bandwidth-->
                            <div class="kt-space-20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                Districts
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                            <!--begin::Timeline 1-->
                            <div class="kt-list-timeline">
                                <div class="kt-list-timeline__items">
                                    @foreach($districts as $district)
                                    <div class="kt-list-timeline__item">
                                        <span class="kt-list-timeline__badge kt-list-timeline__badge--success"></span>
                                        <span class="kt-list-timeline__text"><a href="/district/{{$district->district}}">{{$district->district}}</a></span>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            <!--end::Timeline 1-->
                            <div class="kt-separator kt-separator--space-lg kt-separator--border-dashed">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Begin::Section-->
            <div class="row">
                <div class="col-md-12">
                    <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                        <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                Latest Transactions
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                            <!--begin: Datatable -->
                            <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1_2">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Type</th>
                                        <th>Location</th>
                                        <th>Name</th>
                                        <th>TIN</th>
                                        <td>Description</td>
                                        <th>Amount</th>
                                        <th>Status</th>
                                        <th>Done By</th>
                                        <th>Date</th>
                                    </tr>
                                </thead>
                                <tbody> @foreach($transactions as $key => $transaction)
                                    <tr>
                                        <td style="text-align: center;">{{ ($key + 1) }}</td>
                                        <td>{{ $transaction->fee_type_id }}</td>
                                        <td>{{ $transaction->district }}, {{ $transaction->sector }}</td>
                                        <td>{{ $transaction->name }}</td>
                                        <td>{{ $transaction->tin_number }}</td>
                                        <td>{{ $transaction->description }}</td>
                                        <td>{{ number_format($transaction->amount) }} Rwf</td>
                                        <td>{{ $transaction->status }}</td>
                                        <td>{{ $transaction->doneBy ? $transaction->doneBy->name : null }}</td>
                                        <td>{{ $transaction->created_at->format('d/m/Y H:i') }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <!--end: Datatable -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end:: Content -->
</div>
</div>
</div>
@endsection
@push('end_scripts')
@include('include.date_graph_datatable')
@endpush