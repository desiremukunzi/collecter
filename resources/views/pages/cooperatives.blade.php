    @extends('layouts.master')
    @section('title', 'Cooperatives')
    {{-- @push('styles')
    <style type="text/css">
        .select2-container--default .select2-selection--single .select2-selection__rendered {
            background: #e1e3ec;
        }
        .select2-container {
            margin-right: 10px;
        }
        .btn.btn-clean i {
            color: #2dc7bf;
        }
    </style>
    @endpush --}}
    @section('content')
    <!-- begin:: Subheader -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
    </div>
    <!-- end:: Subheader -->
    <!-- begin:: Content -->
    <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
        <!--Begin::Dashboard 3-->
        <div class="row">
            <div class="col-md-12">
                <!--Begin::Section-->
                <div class="row">
                    <div class="col-md-7">
                        <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                            <div
                            class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                            <div class="kt-portlet__head-label">
                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#add_cooperative_modal">
                                <i class="flaticon2-plus-1"></i>
                                Ongeramo koperative
                                </button>
                           
            
</div>

</div>
<div class="kt-portlet__body">
    
    <table class="table table-striped- table-bordered table-hover table-checkable"
    id="data-table-cooperative">
    <thead>
        <tr>
            <th>#</th>
            <th>Izina</th>
            <th>Impine</th>
            <th>Akarere</th>            
{{--             <th>BPR Account</th>            
 --}}            <th width="100px">Ibikorwa</th>                         
        </tr>
    </thead>
    
</table>
<!--end: Datatable -->
</div>
</div>
</div>

<div class="col-md-5">
                <div class="kt-portlet" style="height:805px;overflow:scroll;">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <right><h3 class="kt-portlet__head-title">
                                                            <span class="kt-list-timeline__text">Amakarita yo gusohora&nbsp;&nbsp;<span>{{allCard_number()}}</span></span>
                                                        </h3></right>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <!--begin::Timeline 1-->
                        <div class="kt-list-timeline">
                            <div class="kt-list-timeline__items">
                                @foreach($amakarita as $cooperative)
                                    <div class="kt-list-timeline__item">
                                        <span class="kt-list-timeline__badge {{ randomTimeLineBadge() }}"></span>
                                        <span class="kt-list-timeline__text">@if(Auth::user()->role==1)
                                                <a href="{{route('card',['cooperative_id',$cooperative->id])}}">@endif{{$cooperative->name}}</a>
                                                <span class="pull-right">{{card_number($cooperative->id)}}</span></span>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <!--end::Timeline 1-->
                        <div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
                    </div>
                </div>
            </div>

</div>
</div>
</div>
</div>
@include('include.edit_cooperative_modal')
@include('include.add_cooperative_modal')
@include('include.delete_cooperative_modal') 
@include('include.add_member_modal')
@include('include.location') 

@endsection

@push('end_scripts')
@include('include.sms_modal_js') 
<script type="text/javascript">
  $(function () {
    
    var table = $('#data-table-cooperative').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{route('cooperatives.index')}}",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'name', name: 'name'},
            {data: 'acronym', name: 'acronym'},
            {data: 'district', name: 'district'},
            // {data: 'bpr_account', name: 'bpr_account'},
            {data: 'actions', name: 'actions', orderable: false, searchable: false},
        ]
    });
    
  });
</script>

{{-- <script src="{{ asset('js/app.js')}}"></script>
<script src="{{ asset('js/cooperatives.js')}}"></script> --}}
@endpush