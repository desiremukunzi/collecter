@extends('layouts.master')
@section('content')
@include('include.subheader')
@include('include.filter')
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <!--Begin::Dashboard 3-->
    <div class="row">
        <div class="col-md-7">       
         <!--Begin::Section-->
          @include('include.dataintables_district')

        </div>
        <div class="col-md-5">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                Cooperatives
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <!--begin::Timeline 1-->
                        <div class="kt-list-timeline">
                            <div class="kt-list-timeline__items">
                                @foreach($cooperatives as $cooperative)
                                    <div class="kt-list-timeline__item">
                                        <span class="kt-list-timeline__badge {{ randomTimeLineBadge() }}"></span>
                                        <span class="kt-list-timeline__text"><a
                                                href="/cooperatives_list/{{$group_cooperative=$cooperative->id}}">{{$cooperative->name}}</a><span class="pull-right">{{count_members('cooperative_id',$group_cooperative)}}</span></span>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <!--end::Timeline 1-->
                        <div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
                    </div>
                </div>
            </div>
    </div>
</div>
<!-- end:: Content -->
</div>
</div>
</div>

@endsection
@push('end_scripts')
{{-- @include('include.date_graph_datatable')
 --}}{{-- @include('include.datatable_actions_members') --}}

{{-- @include('include.edit_member_modal_js') 
 --}}



{{-- <script>
    $('#members').hide();
    
    $('#member').click(function() {
    $('#transactions').hide();
    $('#members').show();
    
});
    $('#transaction').click(function() {
    $('#members').hide();
    $('#transactions').show();
    
});
</script> --}}

@endpush