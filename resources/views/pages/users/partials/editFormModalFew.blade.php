<div class="modal fade" id="editFormModalFew" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit User Information</h5>
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                </button>
            </div>
            <!--begin::Form-->
            <form class="kt-form kt-form--label-right" method="POST" action="{{ route('users.updateFew') }}">
                @csrf
                <div class="modal-body">
                    <input type="hidden" name="id" id="id">
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <label>Amazina:</label>
                            <div class="kt-input-icon">
                                <input type="text" name="name" class="form-control"
                                       placeholder="Enter full name" id="name" required>
                                <span
                                    class="kt-input-icon__icon kt-input-icon__icon--right"><span><i
                                            class="flaticon2-user"></i></span></span>
                            </div>
                            <span class="form-text text-muted">Amazina yose</span>
                        </div>
                        <div class="col-lg-6">
                            <label class="">Tel:</label>
                            <div class="kt-input-icon">
                                <input type="text" name="telephone"
                                       class="form-control"
                                       placeholder="Tel : 078XXXXXXX" id="telephone" maxlength="10">
                                <span
                                    class="kt-input-icon__icon kt-input-icon__icon--right"><span><i
                                            class="flaticon2-phone"></i></span></span>
                            </div>
                            <span class="form-text text-muted">Nimero ya telefoni</span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <label>E-mail:</label>
                            <div class="kt-input-icon">
                                <input type="email" name="email"
                                       class="form-control"
                                       placeholder="Enter your email address" id="email">
                                <span
                                    class="kt-input-icon__icon kt-input-icon__icon--right"><span><i
                                            class="flaticon2-email"></i></span></span>
                            </div>
                            <span class="form-text text-muted">
                            Email yawe</span>
                        </div>
                        <div class="col-lg-6">
                            <label class="">Current-Password:</label>
                                <input type="password" class="form-control"  name="current-password">

                            <span class="form-text text-muted">Password usanzwe ukoresha</span>
                        </div>
                    </div>                    
                    <div class="form-group row">
                        <div class="col-lg-6">
                         <label>New-Password:</label>
                          <input type="password" class="form-control"  name="password">
                          <span class="form-text text-muted">Password Nshya</span>
                        </div>
                        <div class="col-lg-6">
                         <label>Retype-Password:</label>
                            <input type="password" class="form-control" name="password_confirmation">
                            <span class="form-text text-muted">Password Nshya na none</span>
                        </div>                        
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary"
                            data-dismiss="modal">Close
                    </button>
                    <button class="btn btn-info">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>

   @push('end_scripts')
   <script>

    $('#editFormModalFew').on('show.bs.modal', function (event) {

    var button = $(event.relatedTarget)// Button that triggered the modal
    var id = button.data('id') 
    var name = button.data('name') 
    var email = button.data('email') 
    var telephone = button.data('telephone') 
   
    var modal = $(this)
   
    modal.find('.modal-body #id').val(id)
    modal.find('.modal-body #name').val(name)
    modal.find('.modal-body #email').val(email)
    modal.find('.modal-body #telephone').val(telephone)
    
    });
   </script>        
  @endpush
