<div class="kt-portlet__head-toolbar kt-grid__item--fluid">
    <div class="kt-portlet__head-actions">
        <form class="form-inline">
            <select class="form-control users-filter" id="users-filter-role" name="param">
                <option value="">All Roles</option>
                @foreach($roles as $role)
                    <option value="{{ $role->name }}">{{$role->display_name}}s</option>
                @endforeach
            </select>
            <select class="form-control users-filter" id="users-filter-province" name="param">
                <option value="">All Provinces</option>
                @foreach($provinces as $province)
                    <option>{{ $province->name }}</option>
                @endforeach
            </select>
            <select class="form-control users-filter" id="users-filter-district" name="param">
                <option value="">All Districts</option>
            </select>
            <select class="form-control users-filter" id="users-filter-sector" name="param">
                <option value="">All Sectors</option>
            </select>
            <a href="#" class="btn btn-primary" id="users-table-filter">
                <i class="flaticon2-refresh"></i>
                Filter
            </a>
        </form>
    </div>
</div>


@push('end_scripts')
    <script>
        $(document).ready(function () {

            let roleSelectHTML = $("#users-filter-role");

            let provinceHTML = $("#users-filter-province");
            let districtHTML = $("#users-filter-district");
            let sectorHTML = $("#users-filter-sector");

            // province change
            provinceHTML.change(function () {

                // empty districts
                districtHTML.html('');
                sectorHTML.html('');


                districtHTML.html('<option value="">Please wait ...</option>');

                // get districts from the server
                $.get('/ajax/districts/?province=' + $(this).val(), function (data, status) {

                    districtHTML.html('');
                    districtHTML.html('<option value="">-- Select Here ---</option>');

                    if (status !== 'success')
                        return;

                    for (let i = 0; i < data.length; i++) {
                        districtHTML.append('<option>' + data[i].name + '</option>');
                    }
                })
            });

            // District Change
            districtHTML.change(function () {

                // empty sectors
                sectorHTML.html('');

                sectorHTML.html('<option value="">Please wait ...</option>');

                // get sectors from the server
                $.get('/ajax/sectors/?district=' + $(this).val(), function (data, status) {

                    sectorHTML.html('');
                    sectorHTML.html('<option value="">-- Select Here ---</option>');

                    if (status !== 'success')
                        return;

                    for (let i = 0; i < data.length; i++) {
                        sectorHTML.append('<option>' + data[i].name + '</option>');
                    }
                })

            });


            $("#users-table-filter").click(function() {

                let districtValue = districtHTML.val() === null ? '' : districtHTML.val();
                let sectorValue = sectorHTML.val() === null ? '' : sectorHTML.val();

                $('#kt_table_1_2').DataTable()
                    .ajax.url('/ajax/users?role='+ roleSelectHTML.val() +'&province=' + provinceHTML.val() + '&district=' + districtValue + '&sector=' + sectorValue)
                    .load();
            });
        });
    </script>
@endpush
