@extends('layouts.master')

@section('title', 'Users')

@push('styles')
    <link href="{{ asset('assets/vendors/general/morris.js/morris.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/vendors/general/sweetalert2/dist/sweetalert2.css')}}" rel="stylesheet"
          type="text/css"/>
    <style type="text/css">

        .users-filter {
            background: #e1e3ec;
            margin-right: 7px;
        }

        .select2-container {
            margin-right: 10px;
        }


        .btn.btn-clean i {
            color: #2dc7bf;
        }

    </style>

@endpush

@section('content')
    <!-- begin:: Subheader -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
    </div>
    <!-- end:: Subheader -->
    <!-- begin:: Content -->
    <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
        <!--Begin::Dashboard 3-->
        <div class="row">
            <div class="col-md-12">
{{--             @include('include.messages')
 --}}            <!--Begin::Section-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                            <div
                                class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                                <div class="kt-portlet__head-label">
                                    <button type="button" class="btn btn-primary" data-toggle="modal"
                                            data-target="#add_user_modal">
                                        <i class="flaticon2-plus-1"></i>
                                        Add New User
                                    </button>
                <span class="dropdown"> 
                    <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true"> 
                        <i class="la la-ellipsis-h"></i> 
                    </a> 
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="{{route('users.index')}}">
                            <i class="la la-lock"></i> Active Users
                        </a>                         
                         <a class="dropdown-item" href="{{route('users.deleted')}}">
                         <i class="la la-ellipsis-h"></i>Deleted Users 
                         </a> 
                    </div> 
                </span> 

                                    <!--begin::Modal-->
@include('include.add_user_modal')                                    
@include('include.edit_user_modal')                                    
                                    <!-- end modal -->
                                </div>
{{--                                 @include('pages.users.partials.filter')
 --}}                            </div>
                            <div class="kt-portlet__body">
                                <!--begin: Datatable -->
                                <!-- <div class="kt-datatable" id="kt_datatable_latest_orders"></div> -->
                                <!--begin: Datatable -->
                                <table class="table table-striped- table-bordered table-hover table-checkable"
                                       id="kt_table_1_2">
                                    <thead>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th>Name</th>
                                            <th>Role</th>
                                            <th>Phone</th>
                                            <th>E-mail</th>
{{--                                             <th>Deployment Location</th>
 --}}                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    {{--<tbody>
                                        @foreach($users as $key => $user)
                                            <tr>
                                                <td class="text-center">{{ ($key + 1) }}</td>
                                                <td>{{ $user->name }}</td>
                                                <td>{{ $user->roles()->pluck('display_name')->implode(', ')}}</td>
                                                <td>{{ $user->telephone}}</td>
                                                <td>{{ $user->email}}</td>
                                                <td>{{ $user->deployment }}</td>
                                                <td>
                                                    --}}{{--<button type="button" class="btn btn-primary"
                                                            data-full_name="{{$name}}" data-telephone="{{$telephone}}"
                                                            data-email="{{$email}}" data-role="{{$role}}"
                                                            data-province="{{$province}}" data-district="{{$district}}"
                                                            data-sector="{{$sector}}" data-id="{{$id}}"
                                                            data-toggle="modal" data-target="#edit_modal">Edit
                                                    </button>--}}{{--
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>--}}
                                </table>
                                <!--end: Datatable -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end:: Content -->
@endsection

@push('scripts')
    <script src="{{ asset('assets/vendors/general/sweetalert2/dist/sweetalert2.min.js')}}"></script>
    <script src="{{ asset('assets/vendors/custom/js/vendors/sweetalert2.init.js')}}"></script>
@endpush


@push('end_scripts')
    {{--    <script src="{{ asset('js/app.js')}}"></script>--}}
{{--    <script src="{{ asset('js/users.js')}}"></script>--}}
    <script>
        $(document).ready(function() {

            let table = $('#kt_table_1_2');
            var pathname = window.location.pathname;
console.log(pathname);
            // begin first table
            table.DataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                ajax:'ajax'+ pathname,
                columns: [
                    {data: 'DT_RowIndex', searchable: false, orderable: false, className: 'text-center'},
                    {data: 'name', name: 'name'},
                    {data: 'role', name: 'role', orderable: false, searchable: false },
                    {data: 'telephone', name: 'telephone'},
                    {data: 'email', name: 'email', render: function(data,type,full,meta) { return data !==  null ? '<a class="normal-link" href="mailto:'+ data +'" target="_blank">'+ data +'</a>' : '-' }},
                    // {data: 'deployment', name: 'deployment', orderable: false, searchable: false,},
                    {data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center'}
                ],
                columnDefs: [],
                "aLengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
                "iDisplayLength": 25,
                initComplete: function() {


                    $("#kt_table_1_2_length").prepend('<div class="dropdown dropdown-inline"><button type="button" class="btn btn-label-success btn-sm btn-bold dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="flaticon-more-1"></i> </button> <div class="dropdown-menu dropdown-menu-right"> <ul class="kt-nav"> <li class="kt-nav__section kt-nav__section--first"> <span class="kt-nav__section-text">Export Tools</span> </li> <li class="kt-nav__item"> <a href="#" class="kt-nav__link" id="export_print"> <i class="kt-nav__link-icon la la-print"></i> <span class="kt-nav__link-text">Print</span> </a> </li> <li class="kt-nav__item"> <a href="#" class="kt-nav__link" id="export_copy"> <i class="kt-nav__link-icon la la-copy"></i> <span class="kt-nav__link-text">Copy</span> </a> </li> <li class="kt-nav__item"> <a href="#" class="kt-nav__link" id="export_excel"> <i class="kt-nav__link-icon la la-file-excel-o"></i> <span class="kt-nav__link-text">Excel</span> </a> </li> <li class="kt-nav__item"> <a href="#" class="kt-nav__link" id="export_csv"> <i class="kt-nav__link-icon la la-file-text-o"></i> <span class="kt-nav__link-text">CSV</span> </a> </li> <li class="kt-nav__item"> <a href="#" class="kt-nav__link" id="export_pdf"> <i class="kt-nav__link-icon la la-file-pdf-o"></i> <span class="kt-nav__link-text">PDF</span> </a> </li> </ul> </div> </div> &nbsp;&nbsp;&nbsp;') }

            });





        });

        function deactivateUser(user_id) {
            swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, deactivate!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then(function(result){
                if (result.value) {
                    document.getElementById('deactivate-' + user_id).submit();
                    swal.fire(
                        'Deactivated!',
                        'Your user has been deactivated.',
                        'success'
                    )
                    // result.dismiss can be 'cancel', 'overlay',
                    // 'close', and 'timer'
                } else if (result.dismiss === 'cancel') {
                    swal.fire(
                        'Cancelled',
                        'Your imaginary user is safe :)',
                        'error'
                    )
                }
            });
        }

        function restoreUser(user_id) {
            swal.fire({
                title: 'Are you sure?',
                text: "This user will get all former privilleges !",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, Restore!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then(function(result){
                if (result.value) {
                    document.getElementById('restore-' + user_id).submit();
                    swal.fire(
                        'Restored!',
                        'Your user has been restored.',
                        'success'
                    )
                    // result.dismiss can be 'cancel', 'overlay',
                    // 'close', and 'timer'
                } else if (result.dismiss === 'cancel') {
                    swal.fire(
                        'Cancelled',
                        'Your imaginary user is safe :)',
                        'error'
                    )
                }
            });
        }
    </script>
@endpush
