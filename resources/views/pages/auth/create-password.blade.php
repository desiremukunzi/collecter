@extends('layouts.auth')

@section('content')

    <div class="kt-login__right">
        <div class="kt-login__wrapper">
            <div class="kt-login__signin">
                <div class="kt-login__head">
                    <h3 class="kt-login__title">Create Your Password</h3>
                </div>
                <div class="kt-login__form">

                    @if (session('verified'))
                        <div class="alert alert-success" role="alert">
                            Your email is verified successfully!
                        </div>
                    @endif

                    @if(count($errors) > 0)
                        @foreach($errors->all() as $error)
                            <div class="alert alert-danger">
                                {{$error}}
                            </div>
                        @endforeach
                    @endif

                    <form class="kt-form" method="POST" action="{{ route('create-password') }}">
                        @csrf
                        <div class="form-group">
                            <input placeholder="Password" id="password" type="password"
                                   class="form-control @error('password') is-invalid @enderror"
                                   name="password" required autocomplete="new-password">

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input placeholder="Confirm Password" id="password-confirmation" type="password"
                                   class="form-control"
                                   name="password_confirmation" required autocomplete="new-password">

                        </div>
                        <div class="kt-login__actions">
                            <input type="submit" class="btn btn-brand btn-pill btn-elevate" name=""
                                   value="Continue">

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
