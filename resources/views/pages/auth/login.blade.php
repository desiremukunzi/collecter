@extends('layouts.auth')

@section('content')

    <div class="kt-login__right">
        <div class="kt-login__wrapper">
            <div class="kt-login__signin">
                <div class="kt-login__head">
                    <h3 class="kt-login__title">Login To Your Account</h3>
                </div>
                <div class="kt-login__form">
                    @include('include.messages')

                    <form class="kt-form" method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group">
                            <input placeholder="Email or Username" id="email" type="email"
                                   class="form-control @error('email') is-invalid @enderror" name="email"
                                   value="{{ old('email') }}" required autocomplete="email" autofocus>

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input placeholder="Password" id="password" type="password"
                                   class="form-control @error('password') is-invalid @enderror"
                                   name="password" required autocomplete="current-password">

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="row kt-login__extra">
                            <div class="col kt-align-left">
                                <label class="kt-checkbox">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="remember"
                                               id="remember" {{ old('remember') ? 'checked' : '' }}>

                                        <label class="form-check-label" for="remember">
                                            {{ __('Remember Me') }}
                                        </label>
                                    </div>
                                </label>
                            </div>
                            <div class="col kt-align-right">

                                @if (Route::has('password.request'))
                                    <a class="kt-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                        <div class="kt-login__actions">
                            <input type="submit" class="btn btn-brand btn-pill btn-elevate" name=""
                                   value="Sign In">

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
