<div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><i
                class="flaticon2-plus-1"></i> Add new Member</h5>
                <button type="button" class="close" data-dismiss="modal"
                aria-label="Close">
                </button>
            </div>
           {{--  wire:submit.prevent="saveMotorcyclist" --}}             
            <form class="kt-form kt-form--label-right"enctype="multipart/form-data" method="POST"
                    action="{{route('motorcyclist.store')}}">
                    @csrf
                    
            <div class="modal-body">
                <input type="hidden" class="form-control" name="cooperative_id" value="{{$cooperative_id}}">
                <!--begin::Form-->
        @if($success) 
        <div class="alert alert-success">
          <div class="alert-text">{{'Well saved'}}</div>
        </div>
        @endif
               
                    <div class="form-group row">
                         <div class="col-lg-4">
                            
                            {{-- <input type="text" wire:model="cooperative_id" name="cooperative_id" id="cooperative_id"> --}}
                            <label class="">Ifoto:</label>
                            <input type="file" wire:model.lazy="photo" name="photo" 
                        class="form-control">
                        @include('include.error_msg', ['value' => 'photo'])                                        
                        </div>
                        <div class="col-lg-4">
                                <label class="">Telefone:</label>
                                <div class="kt-input-icon">
                                    <input type="text" size="10" wire:model.lazy="telephone" name="telephone"
                                    class="form-control"
                                    placeholder="Enter phone number">
                                    {{-- <span
                                        class="kt-input-icon__icon kt-input-icon__icon--right"><span><i
                                        class="flaticon2-phone"></i></span></span> --}}
                                    @include('include.error_msg', ['value' => 'telephone'])                                        
                                    </div>
                                    
                                </div>
                                    
                        <div class="col-lg-4">
                            <label>Amazina y'umumotari :</label>
                            <div class="kt-input-icon">
                                <input type="text" class="form-control" wire:model.lazy="name"
                                placeholder="Amazina yose y'umumotari" name="name">
                                    {{-- @error('name') <span class="error alert alert-solid-danger">{{ $message }}</span> 
                                    @enderror --}}
                            @include('include.error_msg', ['value' => 'name'])                                        

                            </div>
                            </div>                            
                            </div>
                            <div class="form-group row">
                                 <div class="col-lg-4">
                                    <label>Igitsina:</label>
                                    <div class="kt-input-icon">
                                        <select wire:model="gender" name="gender"
                                            class="form-control kt-selectpicker" required>
                                            <option @if(old('gender')=="Gabo") selected @endif value="Gabo">Gabo</option>
                                            <option @if(old('gender')=="Gore") selected @endif value="Gore">Gore</option>
                                           
                                        </select>
                                        @include('include.error_msg', ['value' => 'gender'])
                                    </div>
                                </div>
                                 <div class="col-lg-4">
                                        <label class="">Irangamuntu:</label>
                                        <input type="number" wire:model.lazy="id_number" name="id_number"
                                    class="form-control"
                                    placeholder="Irangamuntu">
                                    @include('include.error_msg', ['value' => 'id_number'])
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="">Perimi:</label>
                                        <input type="number" value="{{old("permit_number")}}" wire:model="permit_number" name="permit_number"
                                    class="form-control"
                                    placeholder="perimi">
                                @include('include.error_msg', ['value' => 'permit_number'])                                        

                                    </div>
                                    </div>

                                    <div class="form-group row">
                                    <div class="col-lg-4">
                                    <label class="">Purake:</label>
                                    <input type="text" wire:model="plate_number" name="plate_number" 
                                    class="form-control"
                                    placeholder="Purake">
                                @include('include.error_msg', ['value' => 'plate_number'])                                        

                                    </div>
                                    <div class="col-lg-4">
                                    <label>Nyiri Moto:</label>
                                    <div class="kt-input-icon">
                                        <select wire:model="owner" name="owner" id="owner_change"
                                            class="form-control kt-selectpicker" required>
                                            <option @if(old('owner')=="Sinjye") selected @endif value="Sinjye">Sinjye</option>
                                            <option @if(old('owner')=="Njyewe") selected @endif value="Njyewe">Njyewe</option>
                                        </select>
                                @include('include.error_msg', ['value' => 'owner'])
                                    </div>
                                </div>
                                    <div class="col-lg-4">
                                        <label class="">Umugabane:</label>
                                        <input type="number" value="{{old("share")}}" wire:model="share" name="share"
                                    class="form-control"
                                    placeholder="umugabane">
                                @include('include.error_msg', ['value' => 'share'])
                                    </div>
                                </div>                                
                                <div class="form-group row">
                                   <div class="col-lg-4">    
                                    <label class="">Province:</label>
                                    <select wire:model="province" name="province" 
                                    class="form-control kt-selectpicker"
                                    >                                 
                                <option value="" selected disabled="disabled">Select
                                    province
                                </option>
                                    @foreach(provinces() as $province)
                                    <option
                                    value="{{ $province->name}}">{{ $province->name }}</option>
                                    @endforeach
                                </select>                               
                            </div>
                            <div class="col-lg-4">
                                <label class="">District :</label>
                                <select wire:model="district" name="district"
                                class="form-control kt-selectpicker"
                                >  
                                
                                @foreach($distr_owner as $data)
                                @php($select="select")
                                    <option
                                    value="{{ $value=$data->name ?? $select}}">{{ $data->name ?? $select}}</option>
                                @endforeach
                          </select>                                                    
                        </div>
                        <div class="col-lg-4">
                            <label class="">Sector :</label>
                            <select wire:model="sector" name="sector"
                            class="form-control kt-selectpicker">
                            @foreach($sector_owner as $data)
                                @php($select="select")
                                    <option
                                    value="{{ $data->name ?? $select}}">{{ $data->name ?? $select}}</option>
                            @endforeach
                            </select>
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="col-lg-4">
                                <label class="">Cell :</label>
                                <select wire:model="cell" name="cell" 
                                class="form-control kt-selectpicker">
                                @foreach($cell_owner as $data)
                                @php($select="select")
                                    <option
                                    value="{{ $data->name ?? $select}}">{{ $data->name ?? $select}}</option>
                                @endforeach
                            </select>                            
                        </div> 

                        <div class="col-lg-4">
                                <label class="">Village :</label>
                                <select wire:model="village" name="village"
                                class="form-control kt-selectpicker "
                                >
                                @foreach($village_owner as $data)
                                @php($select="select")
                                    <option
                                    value="{{ $data->name ?? $select}}">{{ $data->name ?? $select}}</option>
                                @endforeach
                            </select>                            
                        </div> 
                        
                        <div class="col-lg-4">
                        <label class="">Zone:</label>
                        <input type="text" wire:model.lazy="zone" name="zone" value="{{old("zone")}}"
                        class="form-control"
                        placeholder="zone">
                        @include('include.error_msg', ['value' => 'zone'])
                        </div>
                        </div>
                        @if($owner=="Sinjye")
                                <b>Umwirondoro wa nyiri Moto : </b>
                                <br clear="left"><br>
                                <div class="form-group row">
                                    <div class="col-lg-6">
                                    <label class="">Amazina:</label>
                                    <div class="kt-input-icon">
                                    <input type="text" wire:model="nyirimoto_names" name="nyirimoto_names" class="form-control"
                                placeholder="Amazina ya nyirimoto">
                            @include('include.error_msg', ['value' => 'nyirimoto_names'])

                                </div>
                                    </div>
                                    <div class="col-lg-6">
                                <label class="">Telefone:</label>
                                <div class="kt-input-icon">
                                    <input type="text" wire:model="nyirimoto_telephone" name="nyirimoto_telephone"
                                    class="form-control"
                                    placeholder="Enter phone number">
                            @include('include.error_msg', ['value' => 'nyirimoto_telephone'])
                                 </div>                                    
                                </div>
                                
                            </div>
                            <div class="form-group row">
                                    <div class="col-lg-6">    
                                    <label class="">Province:</label>
                                    <select wire:model="province2" name="province2" 
                                    class="form-control kt-selectpicker"
                                    >                                 
                                <option value="" selected disabled="disabled">Select
                                    province
                                </option>
                                    @foreach(provinces() as $province)
                                    <option
                                    value="{{ $province->name}}">{{ $province->name }}</option>
                                    @endforeach
                                </select>
                               
                            </div>
                            <div class="col-lg-6">
                                <label class="">District :</label>
                                <select wire:model="district2" name="district2"
                                class="form-control kt-selectpicker"
                                >                                  
                                @foreach($distr_owner2 as $data)
                                @php($select="select")
                                    <option
                                    value="{{ $value=$data->name ?? $select}}">{{ $data->name ?? $select}}</option>
                                @endforeach
                             </select>                            
                        </div>
                        </div>
                        <div class="form-group row">
                        <div class="col-lg-4">
                            <label class="">Sector :</label>
                            <select wire:model="sector2" name="sector2"
                            class="form-control kt-selectpicker">
                            @foreach($sector_owner2 as $data)
                                @php($select="select")
                                    <option
                                    value="{{ $data->name ?? $select}}">{{ $data->name ?? $select}}</option>
                            @endforeach
                            </select>
                                    </div>
                           <div class="col-lg-4">
                                <label class="">Cell :</label>
                                <select wire:model="cell2" name="cell2" 
                                class="form-control kt-selectpicker">
                                @foreach($cell_owner2 as $data)
                                @php($select="select")
                                    <option
                                    value="{{ $data->name ?? $select}}">{{ $data->name ?? $select}}</option>
                                @endforeach
                            </select>                            
                        </div> 

                        <div class="col-lg-4">
                                <label class="">Village :</label>
                                <select wire:model="village2" name="village2"
                                class="form-control kt-selectpicker "
                                >
                                @foreach($village_owner2 as $data)
                                @php($select="select")
                                    <option
                                    value="{{ $data->name ?? $select}}">{{ $data->name ?? $select}}</option>
                                @endforeach
                            </select>                            
                        </div>                              
                                </div>
                                @endif


                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary"
                                data-dismiss="modal">Close
                                </button>
                                <input type="submit" name="save" value="Emeza"
                                class="btn btn-primary">                            
                        </div>
                        </form>
                    </div>
                </div>