<div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
            <div
        class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
        <div class="tab">
          <button class="tablinks" onclick="openTab(event, 'Ikarita')" id="defaultOpen">Ikarita</button>
          <button class="tablinks" onclick="openTab(event, 'Umusanzu')">Umusanzu</button>          
          <button class="tablinks" onclick="openTab(event, 'Ibarura')">Ibarura</button>          
          <button class="tablinks" onclick="openTab(event, 'Parking')">Parking</button>          
          <button class="tablinks" onclick="openTab(event, 'Bonus')">Bonus</button>          
        </div>
      </div>
        <div class="kt-portlet__body tabcontent" id="Ikarita" style="height:1100px;overflow:scroll;">
        <table class="table table-hover table-condensed"
                    id="data-table-ikarita">
          <thead style="background-color: #ccf">
                        <tr>
                            <th>#</th>
                            <th>Code</th>
                            <th>Names</th>
                            <th>Cooperative</th>
                            <th>tel</th>
                            <th>Amount</th>
                            <th>Done At</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($ikarita as $key => $transaction)
                        <tr>
                            <td>{{ $loop->iteration}}</td>
                            <td>{{ $transaction->code }}</td>
                            <td>{{ $transaction->motorcyclist->name??'-' }}</td>
                            <td>{{ $transaction->cooperative->name??'-' }}</td>
                            <td>{{ $transaction->tel }}</td>
                            <td>{{ $transaction->amount }}</td>
                            <td>{{ $transaction->created_at }}</td>
                            
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <!--end: Datatable -->

        <form method="post" action="{{route('missed-card-payment')}}">
          @csrf          
          <div class="form-inline pull-center">
          <input type="text" class="form-control mr-1" required placeholder="Motari Code" name="code">
          <input type="text" class="form-control mr-1" required placeholder="Transaction Id" name="trxId">
          <input type="submit" name="" value="Save" class="btn btn-primary">
      </div>
        </form>
        </div>

            
            <div class="kt-portlet__body tabcontent" id="Umusanzu">
                    <table class="table table-hover table-condensed"
                      id="data-table-umusanzu">
                      <thead style="background-color: #ccf">
                        <tr>
                                        <th>#</th>
                                        <th>Kode</th>
                                        <th>Amazina</th>
                                        <th>Koperative</th>
                                        <th>tel</th>
                                        <th>Ukwezi</th>
                                        <th>Amount</th>
                                        <th>BPR Ref No</th>
                                        <th>Done At</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($umusanzu as $key => $transaction)
                                    <tr>
                                        <td>{{ $loop->iteration}}</td>
                                        <td>{{ $transaction->code }}</td>
                                        <td>{{ $transaction->motorcyclist->name??'-' }}</td>
                                        <td>{{ $transaction->cooperative->name??'-' }}</td>
                                        <td>{{ $transaction->tel }}</td>
                                        <td>{{ $transaction->month }}</td>
                                        <td>{{ $transaction->amount }}</td>
                                        <td>{{ $transaction->bprRefNo }}</td>
                                        <td>{{ $transaction->created_at }}</td>
                                        
                                    </tr>
                        @endforeach
                      </tbody>
                    </table>
                    <!--end: Datatable -->
                  </div>
            <div class="kt-portlet__body tabcontent" id="Ibarura">
                    <table class="table table-hover table-condensed"
                      id="data-table-Ibarura">
                      <thead style="background-color: #ccf">
                        <tr>
                                        <th>#</th>
                                        <th>Kode</th>
                                        <th>Amazina</th>
                                        <th>Amafaranga</th>
{{--                                         <th>Purake</th>
 --}}                                        <th>Igihe</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($ibarura as $key => $transaction)
                                    <tr>
                                        <td>{{ $loop->iteration}}</td>
                                        <td>{{ $transaction->code }}</td>
                                        <td>{{ $transaction->motorcyclist->name??'-' }}</td>
                                        <td>{{ $transaction->amount }}</td>
{{--                                         <td>{{ $transaction->plate_number }}</td>
 --}}                                        <td>{{ $transaction->created_at }}</td>                                        
                                    </tr>
                        @endforeach
                      </tbody>
                    </table>
                    <!--end: Datatable -->
                  </div> 
            <div class="kt-portlet__body tabcontent" id="Parking">
                    <table class="table table-hover table-condensed"
                      id="data-table-Parking">
                      <thead style="background-color: #ccf">
                        <tr>
                            <th>#</th>                            
                            <th>Purake</th>
                            <th>tel</th>
                            <th>Ukwezi</th>
                            <th>Amount</th>
                            <th>Done At</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($parking as $key => $transaction)
                                    <tr>
                                        <td>{{ $loop->iteration}}</td>
                                        <td>{{ $transaction->plate_number }}</td>
                                        <td>{{ $transaction->tel }}</td>
                                        <td>{{ $transaction->month }}</td>
                                        <td>{{ $transaction->amount }}</td>
                                        <td>{{ $transaction->created_at }}</td>
                                        
                                    </tr>
                        @endforeach
                      </tbody>
                    </table>
                    <!--end: Datatable -->
                  
            
                    </div>
                    <div class="kt-portlet__body tabcontent" id="Bonus">
                    <table class="table table-hover table-condensed"
                      id="data-table-Bonus">
                      <thead style="background-color: #ccf">
                        <tr>
                            <th>#</th>                            
                            <th>Purake</th>
                            <th>Code</th>
{{--                             <th>tel</th>
 --}}                            <th>Ukwezi</th>
                            <th>Done At</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($tips as $key => $transaction)
                                    <tr>
                                        <td>{{ $loop->iteration}}</td>
                                        <td>{{ $transaction->plate_number }}</td>
                                        <td>{{ $transaction->code }}</td>
{{--                                         <td>{{ $transaction->motorcyclist->telephone ?? '-' }}</td>
 --}}                                        <td>{{ $transaction->month }}</td>
                                        <td>{{ $transaction->created_at }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                    </table>
                    <!--end: Datatable -->
                  
            
                    </div>
                </div>

@push('end_scripts')                 
<script>
$(document).ready(function() {
    $('#data-table-ikarita').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
</script>
<script>
$(document).ready(function() {
    $('#data-table-umusanzu').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
</script>
<script>
$(document).ready(function() {
    $('#data-table-Ibarura').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
</script>
<script>
$(document).ready(function() {
    $('#data-table-Parking').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
</script>
<script>
$(document).ready(function() {
    $('#data-table-Bonus').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
</script>
<script>
  function openTab(evt, tabName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
  tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
  tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(tabName).style.display = "block";
  evt.currentTarget.className += " active";
  }
  // Get the element with id="defaultOpen" and click on it
  document.getElementById("defaultOpen").click();
  </script>
@endpush
