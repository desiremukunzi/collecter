

<script type="text/javascript">

  $(function () {    
    var table = $('#data-table').DataTable({
         destroy: true,
        processing: true,
        serverSide: true,
        dom: 'Bfrtip',
         buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        ajax: "/cooperatives_list/{{request()->segment(2) ?? $group_cooperatives ?? '-'}}",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'code', name: 'code'},
            {data: 'new_code', name: 'new_code'},
            {data: 'name', name: 'name'},            
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
    
  });
</script>