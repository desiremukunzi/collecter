<!--begin::Modal-->
<div class="modal fade" id="card_code_modal" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><i
                class="flaticon2-plus-1"></i> Shyiramo kode y'ikarita</h5>
                <button type="button" class="close" data-dismiss="modal"
                aria-label="Close">
                </button>
            </div>
             <form class="kt-form kt-form--label-right" method="POST"
                    action="{{route('card_id')}}">
                    @csrf
            <div class="modal-body">
              <input type="hidden" name="id" id="card_id">
              <label class="">Kode:</label>
              <input type="text" name="code" class="form-control"/>
            </div>
           
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary"
                data-dismiss="modal">Funga
                </button>
                <input type="submit" name="save" value="Bika"
                class="btn btn-primary">                            
        </div>
        </form>
                    </div>
                </div>
            </div>
 @push('end_scripts')
   <script>
    $('#card_code_modal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget)// Button that triggered the modal
    var card_id = button.data('card_id') 
    var modal = $(this)
   
    modal.find('.modal-body #card_id').val(card_id)
    });
   </script>       
  @endpush 
        
   