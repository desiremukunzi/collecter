<div class="modal fade" id="edit-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit User Information</h5>
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                </button>
            </div>
            <!--begin::Form-->
            <form class="kt-form kt-form--label-right" method="POST" action="{{ route('users.update') }}">
                @csrf
                <div class="modal-body">
                    <input type="hidden" name="id" id="id">
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <label>Full Name:</label>
                            <div class="kt-input-icon">
                                <input type="text" name="name" class="form-control"
                                       placeholder="Enter full name" id="name" required>
                                <span
                                    class="kt-input-icon__icon kt-input-icon__icon--right"><span><i
                                            class="flaticon2-user"></i></span></span>
                            </div>
                            <span class="form-text text-muted">Please enter your full name</span>
                        </div>
                        <div class="col-lg-6">
                            <label class="">Phone Number:</label>
                            <div class="kt-input-icon">
                                <input type="text" name="telephone"
                                       class="form-control"
                                       placeholder="Tel : 078XXXXXXX" id="telephone" maxlength="10">
                                <span
                                    class="kt-input-icon__icon kt-input-icon__icon--right"><span><i
                                            class="flaticon2-phone"></i></span></span>
                            </div>
                            <span class="form-text text-muted">Please enter your phone number</span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <label>E-mail:</label>
                            <div class="kt-input-icon">
                                <input type="email" name="email"
                                       class="form-control"
                                       placeholder="Enter your email address" id="email">
                                <span
                                    class="kt-input-icon__icon kt-input-icon__icon--right"><span><i
                                            class="flaticon2-email"></i></span></span>
                            </div>
                            <span class="form-text text-muted">Please enter your email address</span>
                        </div>
                        <div class="col-lg-6">
                            <label class="">Role:</label>
                            <select class="form-control" name="role" id="role-edit" required>
                                <option value="">-- Select Here --</option>
                                @foreach($roles as $role)
                                    <option value="{{ $role->name }}">{{$role->display_name}}</option>
                                @endforeach
                            </select>
                            <span class="form-text text-muted">Please select your role</span>
                        </div>
                    </div>
                    <div class="form-group row">
               <div class="col-lg-4">
                <label>Current Password:</label>
                <div class="kt-input-icon">
                    <input type="password" name="current-password"
                    class="form-control"
                    placeholder="Current Password">                    
                    </div>
                    <span class="form-text text-muted">your current password</span>
                </div>
                <div class="col-lg-4">
                <label>New Password:</label>
                <div class="kt-input-icon">
                    <input type="password" name="password"
                    class="form-control"
                    placeholder="Enter Password">                    
                    </div>
                    <span class="form-text text-muted">your new password</span>
                </div>
                <div class="col-lg-4">
                    <label class="">Retype Password:</label>
                    <input type="password" class="form-control"
                    name="password_confirmation" placeholder="Retype Password">                    
                <span class="form-text text-muted">Please Retype Password</span>
            </div>
        </div>    

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary"
                            data-dismiss="modal">Close
                    </button>
                    <button class="btn btn-info">Update User</button>
                </div>
            </form>
        </div>
    </div>
</div>

@push('end_scripts')
    <script>
        $(document).ready(function() {
            let roleSelectHTML = $("#role-edit");

            let provinceHTML = $("#province-edit");
            let districtHTML = $("#district-edit");
            let sectorHTML = $("#sector-edit");

            let district;
            let sector;

            let modal = $('#edit-modal');
            modal.on('show.bs.modal', function (event) {

                let button = $(event.relatedTarget);

                let name = button.data('name');
                let email = button.data('email');
                let telephone = button.data('telephone');
                let role = button.data('role');
                let province = button.data('province');
                district = button.data('district');
                sector = button.data('sector');
                let id = button.data('id');

                modal.find('.modal-title').text('Edit  ' + name + ' Info :');
                modal.find('.modal-body #name').val(name);
                modal.find('.modal-body #telephone').val(telephone);
                modal.find('.modal-body #email').val(email);
                modal.find('.modal-body #role-edit').val(role);

                roleSelectHTML.trigger('change');

                modal.find('.modal-body #province-edit').val(province);

                provinceHTML.trigger('change');

                modal.find('.modal-body #id').val(id);
            });


            // role change affecting deployment
            roleSelectHTML.change(function() {

                let val = $(this).val();

                switch (val) {
                    case 'admin':

                        // if administrator disable all deployment fields
                        districtHTML.html('');
                        sectorHTML.html('');

                        provinceHTML.attr('disabled', true);
                        districtHTML.attr('disabled', true);
                        sectorHTML.attr('disabled', true);
                        break;

                    case 'regional_coordinator':
                        // if regional coordinator enable province only
                        provinceHTML.attr('disabled', false);

                        districtHTML.html('');
                        sectorHTML.attr('');

                        districtHTML.attr('disabled', true);
                        sectorHTML.attr('disabled', true);
                        break;

                    case 'district_leader':
                        // if regional coordinator enable province and district only
                        provinceHTML.attr('disabled', false);
                        districtHTML.attr('disabled', false);

                        sectorHTML.html('');
                        sectorHTML.attr('disabled', true);
                        break;

                    case 'fee_collector':
                        // if administrator disable all deployment fields
                        provinceHTML.attr('disabled', false);
                        districtHTML.attr('disabled', false);
                        sectorHTML.attr('disabled', false);
                        break;
                }

                // reset deployment form
                provinceHTML.val('');
                districtHTML.html('');
                sectorHTML.html('');
            });


            // province change
            provinceHTML.change(function() {

                // empty districts
                districtHTML.html('');
                sectorHTML.html('');

                // check if role is district leader or fee collector
                if (roleSelectHTML.val() !== 'district_leader' && roleSelectHTML.val() !== 'fee_collector')
                    return;

                districtHTML.html('<option value="">Please wait ...</option>');

                // get districts from the server
                $.get('/ajax/districts/?province=' + $(this).val(), function (data, status) {

                    districtHTML.html('');
                    districtHTML.html('<option value="">-- Select Here ---</option>');

                    if (status !== 'success')
                        return;

                    for (let i = 0; i < data.length; i++) {
                        districtHTML.append('<option>'+ data[i].name +'</option>');
                    }

                    modal.find('.modal-body #district-edit').val(district);

                    districtHTML.trigger('change');
                })
            });


            // District Change
            districtHTML.change(function() {

                // empty sectors
                sectorHTML.html('');

                if (roleSelectHTML.val() !== 'fee_collector')
                    return;

                sectorHTML.html('<option value="">Please wait ...</option>');

                // get sectors from the server
                $.get('/ajax/sectors/?district=' + $(this).val(), function (data, status) {

                    sectorHTML.html('');
                    sectorHTML.html('<option value="">-- Select Here ---</option>');

                    if (status !== 'success')
                        return;

                    for (let i = 0; i < data.length; i++) {
                        sectorHTML.append('<option>'+ data[i].name +'</option>');
                    }

                    modal.find('.modal-body #sector-edit').val(sector);
                })

            });

        });
    </script>
@endpush
