<!--begin::Modal-->
<div class="modal fade" id="edit_member_modal" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><i
                class="flaticon2-plus-1"></i>Edit Member</h5>
                <button type="button" class="close" data-dismiss="modal"
                aria-label="Close">
                </button>
            </div>
             <form class="kt-form kt-form--label-right" enctype="multipart/form-data" method="POST"
                    action="{{route('motorcyclist_update')}}">
                    @csrf
            <div class="modal-body">
                <!--begin::Form-->               
                         {{-- @livewire('cooperative-livewire') --}}
                    <div class="form-group row">
                         <div class="col-lg-4">
                            {{-- @isset($cooperative)
                            <input type="hidden" name="cooperative_id" value="{{cooperative_id($cooperative)}}">
                            @endisset --}}
                            <input type="hidden" name="id" id="id">

                            <label class="">Ifoto:</label>
                            <input type="file" name="photo">  
                            <img src="" width="50px" id="image" height="50px" />
                        
                            <span class="form-text text-muted">Hitamo Ifoto</span>
                        </div>
                                    
                        <div class="col-lg-4">
                            <label>Amazina y'umumotari :</label>
                            <div class="kt-input-icon">
                                <input type="text" name="names" id="name" class="form-control"
                                placeholder="Amazina yose y'umumotari">
                                <span
                                    class="kt-input-icon__icon kt-input-icon__icon--right"><span><i
                                    class="flaticon2-user"></i></span></span>
                                </div>
                                <span class="form-text text-muted">Injiza amazina y'umumotari</span>
                            </div>
                            <div class="col-lg-4">
                                <label class="">Telefone:</label>
                                <div class="kt-input-icon">
                                    <input type="text" name="telephone" id="telephone"
                                    class="form-control"
                                    placeholder="Enter phone number">
                                    <span
                                        class="kt-input-icon__icon kt-input-icon__icon--right"><span><i
                                        class="flaticon2-phone"></i></span></span>
                                    </div>
                                    <span class="form-text text-muted">Shyiramo nimero ya telefone</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                 <div class="col-lg-4">
                                    <label>Igitsina:</label>
                                    <div class="kt-input-icon">
                                        <select name="gender" id="gender" 
                                            class="form-control kt-selectpicker"  required>
                                            <option value="Gabo">Gabo</option>
                                            <option value="Gore">Gore</option>
                                           
                                        </select>
                                        <span
                                            class="kt-input-icon__icon kt-input-icon__icon--right"><span><i
                                            class="flaticon2-email"></i></span></span>
                                        </div>
                                        <span class="form-text text-muted">Igitsina</span>
                                    </div>
                                 <div class="col-lg-4">
                                        <label class="">Irangamuntu:</label>
                                        <input type="number" id="id_number" size="16" name="id_number"
                                    class="form-control"
                                    placeholder="Irangamuntu">
                                        <span class="form-text text-muted">nimero y'indangamuntu</span>
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="">Perimi:</label>
                                        <input type="number" id="permit_number" size="16" name="permit_number"
                                    class="form-control"
                                    placeholder="perimi">
                                        <span class="form-text text-muted">nimero ya perimi</span>
                                    </div>
                                    </div>

                                    <div class="form-group row">
                                    <div class="col-lg-6">
                                        
                                        <label class="">Cooperative:</label>
                                        <select name="cooperative_id" id="cooperative_id"
                                    class="form-control kt-selectpicker"
                                    >                                    
                                    @foreach(cooperatives() as $cooperative)
                                    <option
                                    value="{{ $cooperative->id}}">{{ $cooperative->name }}</option>
                                    @endforeach
                                </select>
                                <span class="form-text text-muted">select coresponding cooperative</span>
                            </div>
                            <div class="col-lg-6">
                                <label class="">Card Number :</label>
                                <input type="text" name="card_number" class="form-control" id="card_number">
                            <span class="form-text text-muted">Input Card Number</span>
                        </div>
                        {{-- <div class="col-lg-4">
                                <label class="">Card Payed :</label>
                                <input type="text" name="card_payed" class="form-control" id="card_payed">
                            <span class="form-text text-muted">If payed input 1</span>
                        </div>  --}}                      
                        </div>

                                    <div class="form-group row">
                                    <div class="col-lg-4">
                                    <label class="">Purake:</label>
                                    <input type="text"  name="plate_number" id="plate_number"
                                    class="form-control"
                                    placeholder="Purake">
                                        <span class="form-text text-muted">Shyiramo Purake</span>
                                    </div>
                                    <div class="col-lg-4">
                                    <label>Nyiri Moto:</label>
                                    <div class="kt-input-icon">
                                        <select name="owner" id="owner" 
                                            class="form-control kt-selectpicker" required>
                                            <option value="Njyewe">Njyewe</option>
                                            <option value="Sinjye">Sinjye</option>
                                           
                                        </select>
                                        <span
                                            class="kt-input-icon__icon kt-input-icon__icon--right"><span><i
                                            class="flaticon2-email"></i></span></span>
                                        </div>
                                        <span class="form-text text-muted">Nyiri Moto</span>
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="">Umugabane:</label>
                                        <input type="number" name="share" id="share"
                                    class="form-control"
                                    placeholder="umugabane">
                                        <span class="form-text text-muted">umugabane wawe</span>
                                    </div>
                                </div>                                
                                <div class="form-group row">
                                    <div class="col-lg-4">
                                        
                                        <label class="">Province:</label>
                                        <select name="province" id="province"
                                    class="form-control kt-selectpicker dynamic"
                                    data-dependent="district">
                                    <option value="" disabled="disabled">Select
                                        province
                                    </option>
                                    @foreach(provinces() as $province)
                                    <option
                                    value="{{ $province->name}}">{{ $province->name }}</option>
                                    @endforeach
                                </select>
                                <span class="form-text text-muted">Please select your province</span>
                            </div>
                            <div class="col-lg-4">
                                <label class="">District :</label>
                                <select name="district" id="district"
                                class="form-control kt-selectpicker dynamic"
                                data-dependent="sector">
                                <option value="" disabled="disabled">Select
                                    district
                                </option>
                                 @foreach(all_districts() as $item)
                                    <option
                                    value="{{ $item->name}}">{{ $item->name }}</option>
                                    @endforeach
                            </select>
                            <span class="form-text text-muted">Please select your district</span>
                        </div>
                        <div class="col-lg-4">
                            <label class="">Sector :</label>
                            <select name="sector" id="sector"
                            class="form-control kt-selectpicker dynamic" data-dependent="cell">
                            <option value="" disabled="disabled">Select sector
                            </option>
                            @foreach(all_sectors() as $item)
                                    <option
                                    value="{{ $item->name}}">{{ $item->name }}</option>
                                    @endforeach
                        </select>
                            </div>
                        </div>
                        <div class="form-group row">
                           <div class="col-lg-4">
                                <label class="">Cell :</label>
                                <select name="cell" id="cell"
                                class="form-control kt-selectpicker dynamic"
                                data-dependent="village">
                                <option value="" disabled="disabled">Select
                                    cell
                                </option>
                                @foreach(all_cells() as $item)
                                    <option
                                    value="{{ $item->name}}">{{ $item->name }}</option>
                                    @endforeach
                            </select>
                            
                        </div>
                            <div class="col-lg-4">
                                <label class="">Village :</label>
                                <select name="village" id="village"
                                class="form-control kt-selectpicker "
                                >
                                <option value="" disabled="disabled">Select
                                    village
                                </option>
                                @foreach(all_villages() as $item)
                                    <option
                                    value="{{ $item->name}}">{{ $item->name }}</option>
                                    @endforeach
                            </select>
                            
                        </div>
                        
                        <div class="col-lg-4">
                        <label class="">Zone:</label>
                        <input type="text" name="zone" id="zone"
                        class="form-control"
                        placeholder="zone">
                            <span class="form-text text-muted">zone ubarizwamo</span>
                        </div>
                        </div>
                        <div class="form-group row">
                                    <div class="col-lg-6">
                                    <label class="">Track Username:</label>
                                    <div class="kt-input-icon">
                                    <input type="text" name="track_username" id="track_username" class="form-control">
                                    </div>
                                    </div>
                                    <div class="col-lg-6">
                                <label class="">Track Password:</label>
                                <div class="kt-input-icon">
                                    <input type="text" name="track_password" id="track_password"
                                    class="form-control">                                    
                                    </div>                                    
                                </div>
                                
                            </div>
                                <b>Umwirondoro wa nyiri Moto : </b>
                                <br clear="left"><br>
                                <div class="form-group row">
                                    <div class="col-lg-6">
                                    <label class="">Amazina:</label>
                                    <div class="kt-input-icon">
                                    <input type="text" name="nyirimoto_names" id="owner_name" class="form-control"
                                placeholder="Amazina ya nyirimoto">
                                <span
                                    class="kt-input-icon__icon kt-input-icon__icon--right"><span><i
                                    class="flaticon2-user"></i></span></span>
                                </div>
                                    </div>
                                    <div class="col-lg-6">
                                <label class="">Telefone:</label>
                                <div class="kt-input-icon">
                                    <input type="text" name="nyirimoto_telephone" id="owner_telephone"
                                    class="form-control"
                                    placeholder="Enter phone number">
                                    <span
                                        class="kt-input-icon__icon kt-input-icon__icon--right"><span><i
                                        class="flaticon2-phone"></i></span></span>
                                    </div>                                    
                                </div>
                                
                            </div>
                            <div class="form-group row">
                                    <div class="col-lg-6">    
                                    <label class="">Province:</label>
                                    <select name="province2" id="province2"
                                    class="form-control kt-selectpicker dynamic2"
                                    data-dependent="district2">
                                    <option value="" disabled="disabled">Select
                                        province
                                    </option>
                                    @foreach(provinces() as $province)
                                    <option
                                    value="{{ $province->name}}">{{ $province->name }}</option>
                                    @endforeach
                                </select>
                                <span class="form-text text-muted">Please select your province</span>
                            </div>
                            <div class="col-lg-6">
                                <label class="">District :</label>
                                <select name="district2" id="district2"
                                class="form-control kt-selectpicker dynamic2"
                                data-dependent="sector2">
                                <option value="" disabled="disabled">Select
                                    district
                                </option>
                                @foreach(all_districts() as $item)
                                    <option
                                    value="{{ $item->name}}">{{ $item->name }}</option>
                                    @endforeach
                            </select>
                            <span class="form-text text-muted">Please select your district</span>
                        </div>
                        </div>
                        <div class="form-group row">
                        <div class="col-lg-4">
                            <label class="">Sector :</label>
                            <select name="sector2" id="sector2"
                            class="form-control kt-selectpicker dynamic2" data-dependent="cell2">
                            <option value="" disabled="disabled">Select sector
                            </option>
                            @foreach(all_sectors() as $item)
                                    <option
                                    value="{{ $item->name}}">{{ $item->name }}</option>
                                    @endforeach
                        </select>
                                    </div>
                           <div class="col-lg-4">
                                <label class="">Cell :</label>
                                <select name="cell2" id="cell2"
                                class="form-control kt-selectpicker dynamic2"
                                data-dependent="village2">
                                <option value="" disabled="disabled">Select
                                    cell
                                </option>
                                @foreach(all_cells() as $item)
                                    <option
                                    value="{{ $item->name}}">{{ $item->name }}</option>
                                    @endforeach
                            </select>                            
                        </div> 

                        <div class="col-lg-4">
                                <label class="">Village :</label>
                                <select name="village2" id="village2"
                                class="form-control kt-selectpicker "
                                >
                                <option value="" disabled="disabled">Select
                                    village
                                </option>
                                @foreach(all_villages() as $item)
                                    <option
                                    value="{{ $item->name}}">{{ $item->name }}</option>
                                    @endforeach
                            </select>                            
                        </div>                              
                                </div>


                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary"
                                data-dismiss="modal">Close
                                </button>
                                <input type="submit" name="save" value="update"
                                class="btn btn-primary">                            
                        </div>
                        </form>
                    </div>
                </div>
            </div>
 
<script>
    $(document).ready(function () {
        let ownerSelectHTML = $("#owner");
        let n_amazina = $("#owner_name");
        let n_telephone = $("#owner_telephone");
        let provinceHTML = $("#province2");
        let districtHTML = $("#district2");
        let sectorHTML = $("#sector2");
        let cellHTML = $("#cell2");
        let villageHTML = $("#village2");
                            // owner change affecting deplyoyment
                            ownerSelectHTML.change(function () {
                                let val = $(this).val();
                                if(val=='Njyewe'){
                            n_amazina.attr('disabled', true);
                            n_telephone.attr('disabled', true);
                            provinceHTML.attr('disabled', true);
                            districtHTML.attr('disabled', true);
                            sectorHTML.attr('disabled', true);
                            cellHTML.attr('disabled', true);
                            villageHTML.attr('disabled', true);
                        }
                        if(val=='Sinjye'){
                            n_amazina.attr('disabled', false);
                            n_telephone.attr('disabled', false);
                            provinceHTML.attr('disabled', false);
                            districtHTML.attr('disabled', false);
                            sectorHTML.attr('disabled', false);
                            cellHTML.attr('disabled', false);
                            villageHTML.attr('disabled', false);
                        }
                        });
                        });
  </script>  
  @include('include.edit_member_modal_js') 
   