@error($value)
    <span class="error text-danger">
        <small>{{ $message }}</small>
    </span>
@enderror