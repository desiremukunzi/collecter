 <!--begin::Modal-->
                            <div class="modal fade" id="add_cooperative_modal" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel"><i
                                            class="flaticon2-plus-1"></i> Add new cooperative</h5>
                                            <button type="button" class="close" data-dismiss="modal"
                                            aria-label="Close">
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <!--begin::Form-->
                                        <form class="kt-form kt-form--label-right" method="POST"
                                        action="{{ route('register_cooperative') }}">
                                        @csrf
                                        <div class="form-group row">
                                            <div class="col-lg-3">
                                                <label>Cooperative Name:</label>
                                                <div class="kt-input-icon">
                                                    <input type="text" name="cooperative_name" class="form-control"
                                                    placeholder="Cooperative name">                                                   
                                                    </div>
                                                    <span class="form-text text-muted">Cooperative Name</span>
                                                </div>
                            <div class="col-lg-3">
                                <label class="">Acronym:</label>
                                <div class="kt-input-icon">
                                    <input type="text" name="acronym"
                                    class="form-control"
                                    placeholder="Enter Acronym">                                   
                                    </div>
                                    <span class="form-text text-muted">Enter Acronym</span>
                                </div>
                                 <div class="col-lg-3">
                                <label class="">BPR Account:</label>
                                <div class="kt-input-icon">
                                    <input type="text" name="bpr_account"
                                    class="form-control"
                                    placeholder="BPR_account">
                                    
                                    </div>
                                    <span class="form-text text-muted">Please enter BPR Account</span>
                                </div>                             
                                <div class="col-lg-3">
                                    <label class="">District:</label>
                                    <select name="district" id="district"
                                    class="form-control kt-selectpicker" required>
                                    <option value="">-- Select Here --</option>
                                    @foreach($districts as $district)
                                    <option
                                    value="{{ $district->district}}">{{ $district->district }}</option>
                                    @endforeach
                                </select>
                            </div>                     
                        </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"
                                    data-dismiss="modal">Close
                                </button>
                                <input type="submit" name="save" value="Add cooperative"
                                class="btn btn-primary">
                            </form>
                        </div>
                    </div>
                </div>
            </div>