<!--begin::Modal-->
                            <div class="modal fade" id="add_user_modal" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel"><i
                                            class="flaticon2-plus-1"></i> Add new user</h5>
                                            <button type="button" class="close" data-dismiss="modal"
                                            aria-label="Close">
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <!--begin::Form-->
                                        <form class="kt-form kt-form--label-right" method="POST"
                                       action="{{ route('users.store') }}">
                                        @csrf
<div class="form-group row">
    <div class="col-lg-6">
        <label>Full Name:</label>
        <div class="kt-input-icon">
            <input type="text" name="name" class="form-control"
            placeholder="Enter full name">
            <span
            class="kt-input-icon__icon kt-input-icon__icon--right"><span><i
                class="flaticon2-user"></i></span></span>
            </div>
            <span class="form-text text-muted">Please enter your full name</span>
        </div>
        <div class="col-lg-6">
            <label class="">Phone Number:</label>
            <div class="kt-input-icon">
                <input type="text" name="telephone"
                class="form-control"
                placeholder="Enter phone number">
                <span
                class="kt-input-icon__icon kt-input-icon__icon--right"><span><i
                    class="flaticon2-phone"></i></span></span>
                </div>
                <span class="form-text text-muted">Please enter your phone number</span>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-lg-6">
                <label>E-mail:</label>
                <div class="kt-input-icon">
                    <input type="email" name="email"
                    class="form-control"
                    placeholder="Enter your email address">
                    <span
                    class="kt-input-icon__icon kt-input-icon__icon--right"><span><i
                        class="flaticon2-email"></i></span></span>
                    </div>
                    <span class="form-text text-muted">Please enter your email address</span>
                </div>
                <div class="col-lg-6">
                    <label class="">Role:</label>
                    <select class="form-control kt-selectpicker"
                    name="role" id="role_select" required>
                    <option value="">-- Select Here --</option>
                    @foreach($roles as $role)
                    <option
                    value="{{ $role->name }}">{{$role->display_name}}</option>
                    @endforeach
                </select>
                <span class="form-text text-muted">Please select your role</span>
            </div>
        </div>
         <div class="form-group row">
            <div class="col-lg-6">
                <label>Password:</label>
                <div class="kt-input-icon">
                    <input type="password" name="password"
                    class="form-control"
                    placeholder="Enter Password" required>                    
                    </div>
                    <span class="form-text text-muted">Please enter your password</span>
                </div>
                <div class="col-lg-6">
                    <label class="">Retype Password:</label>
                    <input type="password" class="form-control"
                    name="password_confirmation" required>                    
                <span class="form-text text-muted">Please Retype Password</span>
            </div>
        </div>       
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary"
            data-dismiss="modal">Close
        </button>
        <input type="submit" name="save" value="Add User"
        class="btn btn-primary">
    </form>
                        </div>
                    </div>
                </div>
            </div>