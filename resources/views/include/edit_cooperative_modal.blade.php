<!--begin::Edit Modal-->
            <div class="modal fade" id="edit_cooperative_modal" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                        <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <!--begin::Form-->
                    <form class="kt-form kt-form--label-right" method="POST"
                    action="{{route('update_cooperative')}}">
                    @csrf
                    <div class="form-group row">
                        <div class="col-lg-3">
                            <label>Cooperative Name:</label>
                            <input type="hidden" name="id" id="id_edit"
                            class="form-control"
                            placeholder="Enter cooperative name">
                            <div class="kt-input-icon">
                                <input type="text" name="cooperative_name" id="name_edit"
                                class="form-control"
                                placeholder="Enter cooperative name">                                
                                </div>
                                <span class="form-text text-muted">Please enter cooperative name</span>
                            </div>
                            <div class="col-lg-3">
                                <label class="">Acronym:</label>
                                <div class="kt-input-icon">
                                    <input type="text" name="acronym" id="acronym_edit"
                                    class="form-control"
                                    placeholder="Acronym">
                                </div>
                                <span class="form-text text-muted">Please enter Acronym</span>
                                </div> 
                                <div class="col-lg-3">
                                <label class="">BPR Account:</label>
                                <div class="kt-input-icon">
                                    <input type="text" name="bpr_account" id="bpr_account_edit"
                                    class="form-control"
                                    placeholder="Enter corresponding bpr_account">
                                    <span
                                    class="kt-input-icon__icon kt-input-icon__icon--right"><span><i
                                        class="flaticon2-bank"></i></span></span>
                                    </div>
                                    <span class="form-text text-muted">Please enter bpr Account</span>
                                </div>                                             
                                <div class="col-lg-3">
                                    <label class="">district:</label>
                                    <select name="district" id="district_edit"
                                    class="form-control kt-selectpicker dynamic"
                                    data-dependent="district">
                                    <option value="" disabled="disabled">Select
                                        district
                                    </option>
                                    @foreach($districts as $district)
                                    <option
                                    value="{{ $district->district}}">{{ $district->district }}</option>
                                    @endforeach
                                </select>
                                <span class="form-text text-muted">Please select district</span>
                            </div>                            
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary"
                data-dismiss="modal">Close
            </button>
            <input type="submit" name="update " value="Update cooperative"
            class="btn btn-primary">
        </form>
    </div>
</div>
</div>
</div>
{{-- end edit modal --}}
@push('end_scripts')
<script>    
    $('#edit_cooperative_modal').on('show.bs.modal', function (event) {

    var button = $(event.relatedTarget)// Button that triggered the modal
    var id = button.data('id_edit') 
    var name = button.data('name_edit') 
    var acronym = button.data('acronym_edit') 
    var district = button.data('district_edit')    
    var bpr_account = button.data('bpr_account_edit')    
      
    console.log('testing')
    console.log(bpr_account)
    var modal = $(this)
    modal.find('.modal-title').text(name + ' Info')
    modal.find('.modal-body #id_edit').val(id)
    modal.find('.modal-body #name_edit').val(name)
    modal.find('.modal-body #acronym_edit').val(acronym)   
    modal.find('.modal-body #district_edit').val(district)   
    modal.find('.modal-body #bpr_account_edit').val(bpr_account)   
    });
   </script>  
   @endpush