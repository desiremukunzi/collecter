<div class="row">
    <div class="col-md-12">
        <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
            <div
                class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
{{--                     <a href="#transactions" id="transaction" class="mr-5">Latest Transactions</a>
 --}}                    @isset($cooperative)</h3>
 @php $cooperative_id=cooperative_id($cooperative) ?? $cooperative @endphp
                                        <!-- <h3 class="kt-portlet__head-title">
                                        <a href="#members" id="member" class="mr-5">Members</a>
                                        </h3> -->
                                        
                                        <a href="#" data-cooperative_id="{{$cooperative_id}}" class="btn btn-light" data-toggle="modal" data-target="#add_member_modal">
                                            <span class="badge badge-light"><i class="flaticon2-plus-1"></i></span>
                                                    umunyamuryango</a>
                                                                  
                    <a href="{{route('card',['cooperative_id',$cooperative_id])}}" class="btn btn-light">
                    Amakarita <span class="badge badge-light">{{card_number(cooperative_id($cooperative))}}</span>
                    </a>
                                        @endisset
                </div>
            </div>
            
            {{-- <div class="kt-portlet__body" id="transactions">
                <!--begin: Datatable -->
                <!-- <div class="kt-datatable" id="kt_datatable_latest_orders"></div> -->
                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable"
                    id="kt_table_1_2">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Type</th>
                            <th>Code</th>
                            <th>Names</th>
                            <th>Cooperative</th>
                            <th>tel</th>
                            <th>Amount</th>
                            <th>Done At</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($transactions as $key => $transaction)
                        <tr>
                            <td>{{ $loop->iteration}}</td>
                            <td>1</td>
                            <td>{{ $transaction->code }}</td>
                            <td>{{ $transaction->name }}</td>
                            <td>{{ $transaction->cooperative }}</td>
                            <td>{{ $transaction->tel }}</td>
                            <td>{{ $transaction->amount }}</td>
                            <td>{{ $transaction->created_at }}</td>
                            
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <!--end: Datatable -->
            </div> --}}
            <div class="kt-portlet__body" id="members">
                <!--begin: Datatable -->
                <!-- <div class="kt-datatable" id="kt_datatable_latest_orders"></div> -->
                <!--begin: Datatable -->
                <table class="table table-striped- table-bordered table-hover table-checkable"
                    id="data-table-cooperative">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Code</th>
                            <th>name</th>  
                            <th>Tel</th>
                            <th>National Id</th>
                            <th width="100px">actions</th>                         
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($members as $members1 )
                        <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$members1->code}}</td>
                        <td>{{$members1->name}}</td>
                        <td>{{$members1->telephone}}</td>
                        <td>{{$members1->id_number}}</td>
                        <td><?php 
                        $owner=$members1->owner;
                        $plate_number=$members1->moto_detail->plate_number ?? '-'; 
                        $owner_name=$members1->moto_detail->name ?? '-'; 
                        $owner_telephone=$members1->moto_detail->telephone ?? '-'; 
                        $owner_province= $members1->moto_detail->province ?? '-'; 
                        $owner_district=$members1->moto_detail->district ?? '-'; 
                        $owner_sector=$members1->moto_detail->sector ?? '-'; 
                        $owner_cell=$members1->moto_detail->cell ?? '-'; 
                        $owner_village=$members1->moto_detail->village ?? '-'; 
                        
echo '<span class="dropdown"> 
                    <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true"> 
                        <i class="la la-ellipsis-h"></i> 
                    </a> 
                    <div class="dropdown-menu dropdown-menu-right">                    
                     
                     <a class="dropdown-item" 

data-id="'. $members1->id .'" data-name="'. $members1->name .'" 
data-telephone="' . $members1->telephone . '"data-photo="' . $members1->photoExists() . '"
data-province="'. $members1->province .'" data-district="'. $members1->district .'" 
data-sector="'. $members1->sector .'" 
data-cooperative_id="'. $members1->cooperative_id.'" data-cell="'. $members1->cell.'"
data-code="'. $members1->code.'"
data-village="'. $members1->village.'" data-zone="'. $members1->zone.'"
data-id_number="'. $members1->id_number.'"data-permit_number="'. $members1->permit_number.'"
data-share="'. $members1->share.'"data-gender="'. $members1->gender.'"
data-track_username="'. $members1->track_username.'"
data-track_password="'. $members1->track_password.'"
data-card_number="'. $members1->card_number.'"
data-owner="'. $members1->owner.'"
data-cooperative_name="'. $members1->cooperative->cooperative_name.'"

data-plate_number="'. $plate_number.'"
data-owner_name="'.$owner_name .'"
data-owner_telephone="'.$owner_telephone.'"
data-owner_province="'. $owner_province.'"
data-owner_district="'.$owner_district.'"
data-owner_sector="'.$owner_sector .'"
data-owner_cell="'.$owner_cell.'"
data-owner_village="'.$owner_village.'"

data-toggle="modal" data-target="#edit_member_modal"> 
imyirondoro yose
                        </a>                        
                        <a class="dropdown-item" href="'.route('card',['id',$members1->id]) .'">
                            ikarita
                        </a> 
                        <a class="dropdown-item" href="#"
                           data-card_id="'.$members1->id.'" data-toggle="modal" data-target="#card_code_modal"> 
                             ongeraho kode ku ikarita
                        </a> 
                         <a class="dropdown-item" href="#"
                           data-destroy_id="'.$members1->id.'" data-toggle="modal" data-target="#delete_member_modal"> 
                             siba
                        </a> 
                    </div> 
                </span> 
                ';?>
                </td>
                </tr>                          
                        @endforeach
                        
                    </tbody>
                </table>
                <!--end: Datatable -->
            </div>
        </div>
    </div>
</div>

{{-- @include('include.view_member_modal')
 --}}
@include('include.edit_member_modal')
@include('include.add_member_modal')
@include('include.delete_member_modal')
@include('include.card_code_modal')
@include('include.location') 
<script>
$(document).ready(function() {
    $('#data-table-cooperative').DataTable( {
        dom: 'Bfrtip',
        buttons: [{
      extend: 'excelHtml5',
      customizeData: function(data) {
        for(var i = 0; i < data.body.length; i++) {
          for(var j = 0; j < data.body[i].length; j++) {
            data.body[i][j] = '\u200C' + data.body[i][j];
          }
        }
      },      
      orientation: 'landscape'
    }],

        
    } );
} );
</script>