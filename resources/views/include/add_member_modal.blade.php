<!--begin::Modal-->
<div class="modal fade" id="add_member_modal" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    @livewire('motorcyclist-form')
            </div>
    
   <script>
    $(document).ready(function () {
        let ownerSelectHTML = $("#owner_change");
        let n_amazina = $("#n_amazina");
        let n_telephone = $("#n_telephone");
        let provinceHTML = $("#province12");
        let districtHTML = $("#district12");
        let sectorHTML = $("#sector12");
        let cellHTML = $("#cell12");
        let villageHTML = $("#village12");
                            // owner change affecting deplyoyment
                            ownerSelectHTML.change(function () {
                                let val = $(this).val();
                                if(val=='Njyewe'){
                            n_amazina.attr('disabled', true);
                            n_telephone.attr('disabled', true);
                            provinceHTML.attr('disabled', true);
                            districtHTML.attr('disabled', true);
                            sectorHTML.attr('disabled', true);
                            cellHTML.attr('disabled', true);
                            villageHTML.attr('disabled', true);
                        }
                        if(val=='Sinjye'){
                            n_amazina.attr('disabled', false);
                            n_telephone.attr('disabled', false);
                            provinceHTML.attr('disabled', false);
                            districtHTML.attr('disabled', false);
                            sectorHTML.attr('disabled', false);
                            cellHTML.attr('disabled', false);
                            villageHTML.attr('disabled', false);
                        }
                        });
                        });
  </script>
  
  @push('end_scripts')
  <script>
    $('#add_member_modal').on('show.bs.modal', function (event) {

    var button = $(event.relatedTarget)// Button that triggered the modal
    var cooperative_id = button.data('cooperative_id') 
    console.log('testing')
    console.log(cooperative_id)
    var modal = $(this)
   
    modal.find('.modal-body #cooperative_id').val(cooperative_id)
    
    });
   </script>  
   @endpush
                
