<!--begin::Modal-->
<div class="modal fade" id="add_bonus_modal" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><i
                class="flaticon2-plus-1"></i> Bonus</h5>
                <button type="button" class="close" data-dismiss="modal"
                aria-label="Close">
                </button>
            </div>
             <form class="kt-form kt-form--label-right"  method="POST"
                    action="{{route('bonus.store')}}">
                    @csrf
            <div class="modal-body">
                <!--begin::Form-->               
                    <div class="form-group row">                   
                            <label>Kode ya Motari:</label>
                            <div class="kt-input-icon">
                                <input type="text" name="code" required class="form-control"
                                placeholder="kode">                                
                                </div>
                         <label>Purake:</label>
                            <div class="kt-input-icon">
                                <input type="text" name="plate_number" required class="form-control"
                                placeholder="Purake">                                
                                </div>
                            <label>Hitamo Ukwezi:</label>
                            <div class="kt-input-icon">
                                 <select name="month" class="form-control kt-selectpicker" required>
                                         <option>1</option>
                                         <option>2</option>
                                         <option>3</option>
                                         <option>4</option>
                                         <option>5</option>
                                         <option>6</option>
                                         <option>7</option>
                                         <option>8</option>
                                         <option>9</option>
                                         <option>10</option>
                                         <option>11</option>
                                         <option>12</option>                                           
                                 </select>                            
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary"
                                data-dismiss="modal">Close
                                </button>
                                <input type="submit" name="save" value="Emeza"
                                class="btn btn-primary">                            
                        </div>
                        </form>
                    </div>
                </div>
            </div>
    
   <script>
    $(document).ready(function () {
        let ownerSelectHTML = $("#owner_change");
        let n_amazina = $("#n_amazina");
        let n_telephone = $("#n_telephone");
        let provinceHTML = $("#province12");
        let districtHTML = $("#district12");
        let sectorHTML = $("#sector12");
        let cellHTML = $("#cell12");
        let villageHTML = $("#village12");
                            // owner change affecting deplyoyment
                            ownerSelectHTML.change(function () {
                                let val = $(this).val();
                                if(val=='Njyewe'){
                            n_amazina.attr('disabled', true);
                            n_telephone.attr('disabled', true);
                            provinceHTML.attr('disabled', true);
                            districtHTML.attr('disabled', true);
                            sectorHTML.attr('disabled', true);
                            cellHTML.attr('disabled', true);
                            villageHTML.attr('disabled', true);
                        }
                        if(val=='Sinjye'){
                            n_amazina.attr('disabled', false);
                            n_telephone.attr('disabled', false);
                            provinceHTML.attr('disabled', false);
                            districtHTML.attr('disabled', false);
                            sectorHTML.attr('disabled', false);
                            cellHTML.attr('disabled', false);
                            villageHTML.attr('disabled', false);
                        }
                        });
                        });
  </script>
  
  @push('end_scripts')
  <script>
    $('#add_member_modal').on('show.bs.modal', function (event) {

    var button = $(event.relatedTarget)// Button that triggered the modal
    var cooperative_id = button.data('cooperative_id') 
    console.log('testing')
    console.log(cooperative_id)
    var modal = $(this)
   
    modal.find('.modal-body #cooperative_id').val(cooperative_id)
    
    });
   </script>  
   @endpush
                
