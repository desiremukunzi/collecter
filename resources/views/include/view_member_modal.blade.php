<!--begin::Modal-->
<div class="modal fade" id="view_member_modal" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><i
                class="flaticon2-plus-1"></i> View Member</h5>
                <button type="button" class="close" data-dismiss="modal"
                aria-label="Close">
                </button>
            </div>             
            <div class="modal-body">
                <!--begin::Form-->               
                    <div class="row">
                         <div class="col-lg-4">                             
                            <img src="" width="50px" id="image" height="50px" />
                        </div>                                    
                        <div class="col-lg-4">
                            <p id="name" class="text-md-left"></p>
                            <p id="gender" class="text-md-left"></p>
                            <p id="telephone" class="text-md-left"></p>
                            <p id="id_number" class="text-md-left"></p>
                        </div>
                        <div class="col-lg-4">
                         <p id="plate_number" class="text-md-left"></p>
                            <p id="owner" class="p1"></p>
                            {{-- <input type="text" id="owner" name=""> --}}
                            <p id="share" class="text-md-left"></p>
                         <p id="permit_number" class="text-md-left"></p>
                        </div>
                            </div>                                        
                                <div class="row">
                                    <div class="col-lg-4">
                                        
                                        <label class="">Province:</label>
                                        <select name="province" id="province"
                                    class="form-control kt-selectpicker dynamic"
                                    data-dependent="district">
                                    <option value="" disabled="disabled">Select
                                        province
                                    </option>
                                    @foreach(provinces() as $province)
                                    <option
                                    value="{{ $province->name}}">{{ $province->name }}</option>
                                    @endforeach
                                </select>
                                <span class="form-text text-muted">Please select your province</span>
                            </div>
                            <div class="col-lg-4">
                                <label class="">District :</label>
                                <select name="district" id="district"
                                class="form-control kt-selectpicker dynamic"
                                data-dependent="sector">
                                <option value="" disabled="disabled">Select
                                    district
                                </option>
                            </select>
                            <span class="form-text text-muted">Please select your district</span>
                        </div>
                        <div class="col-lg-4">
                            <label class="">Sector :</label>
                            <select name="sector" id="sector"
                            class="form-control kt-selectpicker dynamic" data-dependent="cell">
                            <option value="" disabled="disabled">Select sector
                            </option>
                        </select>
                            </div>
                        </div>
                        <div class="row">
                           <div class="col-lg-4">
                                <label class="">Cell :</label>
                                <select name="cell" id="cell"
                                class="form-control kt-selectpicker dynamic"
                                data-dependent="village">
                                <option value="" disabled="disabled">Select
                                    cell
                                </option>
                            </select>
                            
                        </div>
                            <div class="col-lg-4">
                                <label class="">Village :</label>
                                <select name="village" id="village"
                                class="form-control kt-selectpicker "
                                >
                                <option value="" disabled="disabled">Select
                                    village
                                </option>
                            </select>
                            
                        </div>
                        
                        <div class="col-lg-4">
                        <label class="">Zone:</label>
                        <input type="text" name="zone" id="zone"
                        class="form-control"
                        placeholder="zone">
                            <span class="form-text text-muted">zone ubarizwamo</span>
                        </div>
                        </div>
                                <b>Umwirondoro wa nyiri Moto : </b>
                                <br clear="left"><br>
                                <div class="row">
                                    <div class="col-lg-6">
                                    <label class="">Amazina:</label>
                                    <div class="kt-input-icon">
                                    <input type="text" name="nyirimoto_names" id="owner" class="form-control"
                                placeholder="Amazina ya nyirimoto">
                                <span
                                    class="kt-input-icon__icon kt-input-icon__icon--right"><span><i
                                    class="flaticon2-user"></i></span></span>
                                </div>
                                    </div>
                                    <div class="col-lg-6">
                                <label class="">Telefone:</label>
                                <div class="kt-input-icon">
                                    <input type="text" name="nyirimoto_telephone" id="owner_telephone"
                                    class="form-control"
                                    placeholder="Enter phone number">
                                    <span
                                        class="kt-input-icon__icon kt-input-icon__icon--right"><span><i
                                        class="flaticon2-phone"></i></span></span>
                                    </div>                                    
                                </div>
                                
                            </div>
                            <div class="row">
                                    <div class="col-lg-6">    
                                    <label class="">Province:</label>
                                    <select name="province2" id="province2"
                                    class="form-control kt-selectpicker dynamic2"
                                    data-dependent="district2">
                                    <option value="" disabled="disabled">Select
                                        province
                                    </option>
                                    @foreach(provinces() as $province)
                                    <option
                                    value="{{ $province->name}}">{{ $province->name }}</option>
                                    @endforeach
                                </select>
                                <span class="form-text text-muted">Please select your province</span>
                            </div>
                            <div class="col-lg-6">
                                <label class="">District :</label>
                                <select name="district2" id="district2"
                                class="form-control kt-selectpicker dynamic2"
                                data-dependent="sector2">
                                <option value="" disabled="disabled">Select
                                    district
                                </option>
                            </select>
                            <span class="form-text text-muted">Please select your district</span>
                        </div>
                        </div>
                        <div class="row">
                        <div class="col-lg-4">
                            <label class="">Sector :</label>
                            <select name="sector2" id="sector2"
                            class="form-control kt-selectpicker dynamic2" data-dependent="cell2">
                            <option value="" disabled="disabled">Select sector
                            </option>
                        </select>
                                    </div>
                           <div class="col-lg-4">
                                <label class="">Cell :</label>
                                <select name="cell2" id="cell2"
                                class="form-control kt-selectpicker dynamic2"
                                data-dependent="village2">
                                <option value="" disabled="disabled">Select
                                    cell
                                </option>
                            </select>                            
                        </div> 

                        <div class="col-lg-4">
                                <label class="">Village :</label>
                                <select name="village2" id="village2"
                                class="form-control kt-selectpicker "
                                >
                                <option value="" disabled="disabled">Select
                                    village
                                </option>
                            </select>                            
                        </div>                              
                                </div>


                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary"
                                data-dismiss="modal">Close
                                </button>
                            </div>
                    </div>
                </div>
            </div>
 
