<script>
    $(document).ready(function () {
        let provinceHTML = $("#province");
        let districtHTML = $("#district");
        let sectorHTML = $("#sector");
        let cellHTML = $("#cell");
        let villageHTML = $("#village");

        let provinceHTML2 = $("#province2");
        let districtHTML2 = $("#district2");
        let sectorHTML2 = $("#sector2");
        let cellHTML2 = $("#cell2");
        let villageHTML2 = $("#village2");
                            
                            // province change
                            provinceHTML.change(function () {
                            // empty districts
                            districtHTML.html('');
                            sectorHTML.html('');
                            
                            districtHTML.html('<option value="">Please wait ...</option>');
                            // get districts from the server
                            $.get('/ajax/districts/?province=' + $(this).val(), function (data, status) {
                                districtHTML.html('');
                                districtHTML.html('<option value="">-- Select Here ---</option>');
                                if (status !== 'success')
                                    return;
                                for (let i = 0; i < data.length; i++) {
                                    districtHTML.append('<option>' + data[i].name + '</option>');
                                }
                            })
                        });
                            // District Change
                            districtHTML.change(function () {
                            // empty sectors
                            sectorHTML.html('');
                           
                            sectorHTML.html('<option value="">Please wait ...</option>');
                            // get sectors from the server
                            $.get('/ajax/sectors/?district=' + $(this).val(), function (data, status) {
                                sectorHTML.html('');
                                sectorHTML.html('<option value="">-- Select Here ---</option>');
                                if (status !== 'success')
                                    return;
                                for (let i = 0; i < data.length; i++) {
                                    sectorHTML.append('<option>' + data[i].name + '</option>');
                                }
                            })
                        });

                            sectorHTML.change(function () {
                            // empty sectors
                            cellHTML.html('');
                           
                            cellHTML.html('<option value="">Please wait ...</option>');
                            // get cells from the server
                            $.get('/ajax/cells/?sector=' + $(this).val(), function (data, status) {
                                cellHTML.html('');
                                cellHTML.html('<option value="">-- Select Here ---</option>');
                                if (status !== 'success')
                                    return;
                                for (let i = 0; i < data.length; i++) {
                                    cellHTML.append('<option>' + data[i].name + '</option>');
                                }
                            })
                        });

                            cellHTML.change(function () {
                            // empty sectors
                            villageHTML.html('');
                           
                            villageHTML.html('<option value="">Please wait ...</option>');
                            // get villages from the server
                            $.get('/ajax/villages/?cell=' + $(this).val(), function (data, status) {
                                villageHTML.html('');
                                villageHTML.html('<option value="">-- Select Here ---</option>');
                                if (status !== 'success')
                                    return;
                                for (let i = 0; i < data.length; i++) {
                                    villageHTML.append('<option>' + data[i].name + '</option>');
                                }
                            })
                        });

                            //second(2) location

                             // province change
                            provinceHTML2.change(function () {
                            // empty districts
                            districtHTML2.html('');
                            sectorHTML2.html('');
                            
                            districtHTML2.html('<option value="">Please wait ...</option>');
                            // get districts from the server
                            $.get('/ajax/districts/?province=' + $(this).val(), function (data, status) {
                                districtHTML2.html('');
                                districtHTML2.html('<option value="">-- Select Here ---</option>');
                                if (status !== 'success')
                                    return;
                                for (let i = 0; i < data.length; i++) {
                                    districtHTML2.append('<option>' + data[i].name + '</option>');
                                }
                            })
                        });
                            // District Change
                            districtHTML2.change(function () {
                            // empty sectors
                            sectorHTML2.html('');
                           
                            sectorHTML2.html('<option value="">Please wait ...</option>');
                            // get sectors from the server
                            $.get('/ajax/sectors/?district=' + $(this).val(), function (data, status) {
                                sectorHTML2.html('');
                                sectorHTML2.html('<option value="">-- Select Here ---</option>');
                                if (status !== 'success')
                                    return;
                                for (let i = 0; i < data.length; i++) {
                                    sectorHTML2.append('<option>' + data[i].name + '</option>');
                                }
                            })
                        });

                            sectorHTML2.change(function () {
                            // empty sectors
                            cellHTML2.html('');
                           
                            cellHTML2.html('<option value="">Please wait ...</option>');
                            // get cells from the server
                            $.get('/ajax/cells/?sector=' + $(this).val(), function (data, status) {
                                cellHTML2.html('');
                                cellHTML2.html('<option value="">-- Select Here ---</option>');
                                if (status !== 'success')
                                    return;
                                for (let i = 0; i < data.length; i++) {
                                    cellHTML2.append('<option>' + data[i].name + '</option>');
                                }
                            })
                        });

                            cellHTML2.change(function () {
                            // empty sectors
                            villageHTML2.html('');
                           
                            villageHTML2.html('<option value="">Please wait ...</option>');
                            // get villages from the server
                            $.get('/ajax/villages/?cell=' + $(this).val(), function (data, status) {
                                villageHTML2.html('');
                                villageHTML2.html('<option value="">-- Select Here ---</option>');
                                if (status !== 'success')
                                    return;
                                for (let i = 0; i < data.length; i++) {
                                    villageHTML2.append('<option>' + data[i].name + '</option>');
                                }
                            })
                        });

        let province1HTML = $("#province1");
        let district1HTML = $("#district1");
        let sector1HTML = $("#sector1");
        let cell1HTML = $("#cell1");
        let village1HTML = $("#village1");

        let province1HTML2 = $("#province12");
        let district1HTML2 = $("#district12");
        let sector1HTML2 = $("#sector12");
        let cell1HTML2 = $("#cell12");
        let village1HTML2 = $("#village12");
                            
                            // province1 change
                            province1HTML.change(function () {
                            // empty district1s
                            district1HTML.html('');
                            sector1HTML.html('');
                            
                            district1HTML.html('<option value="">Please wait ...</option>');
                            // get district1s from the server
                            $.get('/ajax/districts/?province=' + $(this).val(), function (data, status) {
                                district1HTML.html('');
                                district1HTML.html('<option value="">-- Select Here ---</option>');
                                if (status !== 'success')
                                    return;
                                for (let i = 0; i < data.length; i++) {
                                    district1HTML.append('<option>' + data[i].name + '</option>');
                                }
                            })
                        });
                            // District1 Change
                            district1HTML.change(function () {
                            // empty sector1s
                            sector1HTML.html('');
                           
                            sector1HTML.html('<option value="">Please wait ...</option>');
                            // get sector1s from the server
                            $.get('/ajax/sectors/?district=' + $(this).val(), function (data, status) {
                                sector1HTML.html('');
                                sector1HTML.html('<option value="">-- Select Here ---</option>');
                                if (status !== 'success')
                                    return;
                                for (let i = 0; i < data.length; i++) {
                                    sector1HTML.append('<option>' + data[i].name + '</option>');
                                }
                            })
                        });

                            sector1HTML.change(function () {
                            // empty sector1s
                            cell1HTML.html('');
                           
                            cell1HTML.html('<option value="">Please wait ...</option>');
                            // get cell1s from the server
                            $.get('/ajax/cells/?sector=' + $(this).val(), function (data, status) {
                                cell1HTML.html('');
                                cell1HTML.html('<option value="">-- Select Here ---</option>');
                                if (status !== 'success')
                                    return;
                                for (let i = 0; i < data.length; i++) {
                                    cell1HTML.append('<option>' + data[i].name + '</option>');
                                }
                            })
                        });

                            cell1HTML.change(function () {
                            // empty sector1s
                            village1HTML.html('');
                           
                            village1HTML.html('<option value="">Please wait ...</option>');
                            // get village1s from the server
                            $.get('/ajax/villages/?cell=' + $(this).val(), function (data, status) {
                                village1HTML.html('');
                                village1HTML.html('<option value="">-- Select Here ---</option>');
                                if (status !== 'success')
                                    return;
                                for (let i = 0; i < data.length; i++) {
                                    village1HTML.append('<option>' + data[i].name + '</option>');
                                }
                            })
                        });

                            //second(2) location

                             // province1 change
                            province1HTML2.change(function () {
                            // empty district1s
                            district1HTML2.html('');
                            sector1HTML2.html('');
                            
                            district1HTML2.html('<option value="">Please wait ...</option>');
                            // get district1s from the server
                            $.get('/ajax/districts/?province=' + $(this).val(), function (data, status) {
                                district1HTML2.html('');
                                district1HTML2.html('<option value="">-- Select Here ---</option>');
                                if (status !== 'success')
                                    return;
                                for (let i = 0; i < data.length; i++) {
                                    district1HTML2.append('<option>' + data[i].name + '</option>');
                                }
                            })
                        });
                            // District1 Change
                            district1HTML2.change(function () {
                            // empty sector1s
                            sector1HTML2.html('');
                           
                            sector1HTML2.html('<option value="">Please wait ...</option>');
                            // get sector1s from the server
                            $.get('/ajax/sectors/?district=' + $(this).val(), function (data, status) {
                                sector1HTML2.html('');
                                sector1HTML2.html('<option value="">-- Select Here ---</option>');
                                if (status !== 'success')
                                    return;
                                for (let i = 0; i < data.length; i++) {
                                    sector1HTML2.append('<option>' + data[i].name + '</option>');
                                }
                            })
                        });

                            sector1HTML2.change(function () {
                            // empty sector1s
                            cell1HTML2.html('');
                           
                            cell1HTML2.html('<option value="">Please wait ...</option>');
                            // get cell1s from the server
                            $.get('/ajax/cells/?sector=' + $(this).val(), function (data, status) {
                                cell1HTML2.html('');
                                cell1HTML2.html('<option value="">-- Select Here ---</option>');
                                if (status !== 'success')
                                    return;
                                for (let i = 0; i < data.length; i++) {
                                    cell1HTML2.append('<option>' + data[i].name + '</option>');
                                }
                            })
                        });

                            cell1HTML2.change(function () {
                            // empty sector1s
                            village1HTML2.html('');
                           
                            village1HTML2.html('<option value="">Please wait ...</option>');
                            // get village1s from the server
                            $.get('/ajax/villages/?cell=' + $(this).val(), function (data, status) {
                                village1HTML2.html('');
                                village1HTML2.html('<option value="">-- Select Here ---</option>');
                                if (status !== 'success')
                                    return;
                                for (let i = 0; i < data.length; i++) {
                                    village1HTML2.append('<option>' + data[i].name + '</option>');
                                }
                            })
                        });
                            
                        });


                    </script>   