<style type="text/css">
  .no-border {
    border: 0;
    box-shadow: none;
}
</style>
<!--begin::Modal-->
<div class="modal fade" id="edit_sms_modal" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><i
                class="flaticon2-plus-1"></i> Ubutumwa bugufi</h5>
                <button type="button" class="close" data-dismiss="modal"
                aria-label="Close">
                </button>
            </div>
            <form class="kt-form kt-form--label-right" method="POST"
                    action="{{route('sms')}}">
                    @csrf
            <div class="modal-body">
              <input type="hidden" name="cooperative_id" id="c_id">              
              <label class="">Uwohereza:</label>
              <input type="text" name="uwohereza" id="sender" required value="{{old("uwohereza")}}" class="form-control"/>             

              <label class="">Message:</label>
              <textarea onkeyup="textCounter2(this,'counter2');" cols="5" id="message" rows="3" name="message" value="{{old("message")}}" class="form-control"></textarea>
              <br>

             <center>
              <label class="">Umubare w'inyuguti : </label>
                <input size="2" id="counter2" type="text" name="characters" readonly class="no-border" />
             </center>
            </div>
           
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary"
                data-dismiss="modal">Close
                </button>
                <input type="submit" name="save" value="Ohereza"
                class="btn btn-primary"/>                            
        </div>
        </form>
                    </div>
                </div>
            </div>

<script>
function textCounter2(field,field2)
{
 var countfield = document.getElementById(field2);
 var num=field.value.length-1
countfield.value = num+1;

  return num-1;
 
}
</script>
 
   @push('end_scripts')
   <script>

    $('#edit_sms_modal').on('show.bs.modal', function (event) {

    var button = $(event.relatedTarget)// Button that triggered the modal
    var c_id = button.data('c_id') 
    var id = button.data('id') 
    var sender = button.data('sender') 
    var message = button.data('message') 
    var counter2 = button.data('counter2') 
   
      
    console.log('testing')
    console.log(c_id)
    var modal = $(this)
   
    modal.find('.modal-body #c_id').val(c_id)
    modal.find('.modal-body #sender').val(sender)
    modal.find('.modal-body #message').val(message)
    modal.find('.modal-body #counter2').val(counter2)
    
    });
   </script>        
  @endpush    
   