

<div class="row">
    <div class="col-md-12">
        <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
            <div
                class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">

                </div>
            </div>
            
            <div class="kt-portlet__body" id="members">
               
                <table class="table table-striped- table-bordered table-hover table-checkable"
                    id="data-table-district">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Code</th>
                            <th>New-Code</th>
                            <th>name</th>
                            {{-- <th>tel</th>
                            <th>District</th> --}}
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
                <!--end: Datatable -->
            </div>
        </div>
    </div>
</div>
 @include('include.datatable_actions_district')


