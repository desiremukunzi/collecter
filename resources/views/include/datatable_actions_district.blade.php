<script type="text/javascript">
  $(function () {
    
    $('#data-table-district').DataTable({
         destroy: true,
        processing: true,
        serverSide: true,
    //     dom: 'Bfrtip',
    // buttons: [
    //     'copy', 'csv', 'excel', 'pdf', 'print'
    // ],
        ajax: "{{route('cooperative',request()->segment(2))}}",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'code', name: 'code'},
            {data: 'new_code', name: 'new_code'},
            {data: 'name', name: 'name'},            
           
        ]
    });
    
  });
</script>