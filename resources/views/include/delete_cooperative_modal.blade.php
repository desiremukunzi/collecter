<!--begin::Modal-->
<div class="modal fade" id="delete_cooperative_modal" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><i
                class="flaticon2-minus-1"></i> Gusiba Koperative</h5>
                <button type="button" class="close" data-dismiss="modal"
                aria-label="Close">
                </button>
            </div>
             <form class="kt-form kt-form--label-right" method="POST"
                    action="{{route('cooperatives.destroy')}}">
                    @csrf
            <div class="modal-body">
              <input type="hidden" name="id" id="destroy_id2">
              <h5><center>urashaka koko gusiba iyi koperative ? <br>n'abanyamuryango bayo barasibwa !</center></h5>
            </div>
           
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary"
                data-dismiss="modal">Hoya
                </button>
                <input type="submit" name="save" value="Yego"
                class="btn btn-primary">                            
        </div>
        </form>
                    </div>
                </div>
            </div>
 @push('end_scripts')
   <script>

    $('#delete_cooperative_modal').on('show.bs.modal', function (event) {

    var button = $(event.relatedTarget)// Button that triggered the modal
    var destroy_id2 = button.data('destroy_id2') 
    var modal = $(this)
    console.log(destroy_id2)
   
    modal.find('.modal-body #destroy_id2').val(destroy_id2)
    });
   </script>       
  @endpush 