<!--begin::Modal-->
<div class="modal fade" id="delete_member_modal" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><i
                class="flaticon2-minus-1"></i> Gusiba Umunyamuryango</h5>
                <button type="button" class="close" data-dismiss="modal"
                aria-label="Close">
                </button>
            </div>
             <form class="kt-form kt-form--label-right" method="POST"
                    action="{{route('motorcyclist_delete')}}">
                    @csrf
            <div class="modal-body">
              <input type="hidden" name="id" id="destroy_id">
              <h5><center>urashaka koko gusiba uyu munyamuryango ?</center></h5>
            </div>
           
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary"
                data-dismiss="modal">Hoya
                </button>
                <input type="submit" name="save" value="Yego"
                class="btn btn-primary">                            
        </div>
        </form>
                    </div>
                </div>
            </div>
 @push('end_scripts')
   <script>

    $('#delete_member_modal').on('show.bs.modal', function (event) {

    var button = $(event.relatedTarget)// Button that triggered the modal
    var destroy_id = button.data('destroy_id') 
    var modal = $(this)
   
    modal.find('.modal-body #destroy_id').val(destroy_id)
    });
   </script>       
  @endpush 