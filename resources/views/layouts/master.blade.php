<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8"/>
    <title>@yield('title') | Motari</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--begin::Fonts -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> --}}

    <script>
        WebFont.load({
            google: {
                "families": ["Poppins:300,400,500,600,700"]
            },
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script>

    <!--end::Fonts -->

    <!--begin::Page Vendors Styles(used by this page) -->
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css"/>

    <!--end::Page Vendors Styles -->

    <!--begin:: Global Mandatory Vendors -->
    <link href="{{ asset('assets/vendors/general/perfect-scrollbar/css/perfect-scrollbar.css')}}" rel="stylesheet"
          type="text/css"/>

    <!--end:: Global Mandatory Vendors -->

    <!--begin:: Global Optional Vendors -->
    <link href="{{ asset('assets/vendors/general/tether/dist/css/tether.css')}}" rel="stylesheet" type="text/css"/>

    <link href="{{ asset('assets/vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css')}}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/vendors/general/bootstrap-datetime-picker/css/bootstrap-datetimepicker.css')}}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/vendors/general/bootstrap-timepicker/css/bootstrap-timepicker.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('assets/vendors/general/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('assets/vendors/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css')}}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/vendors/general/bootstrap-select/dist/css/bootstrap-select.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('assets/vendors/general/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css')}}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/vendors/general/select2/dist/css/select2.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/vendors/general/ion-rangeslider/css/ion.rangeSlider.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('assets/vendors/general/nouislider/distribute/nouislider.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('assets/vendors/general/owl.carousel/dist/assets/owl.carousel.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('assets/vendors/general/owl.carousel/dist/assets/owl.theme.default.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('assets/vendors/general/dropzone/dist/dropzone.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/vendors/general/summernote/dist/summernote.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/vendors/general/bootstrap-markdown/css/bootstrap-markdown.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('assets/vendors/general/animate.css/animate.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/vendors/general/toastr/build/toastr.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/vendors/general/morris.js/morris.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/vendors/general/socicon/css/socicon.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/vendors/custom/vendors/line-awesome/css/line-awesome.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('assets/vendors/custom/vendors/flaticon/flaticon.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/vendors/custom/vendors/flaticon2/flaticon.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/vendors/general/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet"
          type="text/css"/>


    <!--end:: Global Optional Vendors -->

    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="{{ asset('assets/css/demo9/style.bundle.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('message.css')}}" rel="stylesheet" type="text/css"/>


    <!--end::Global Theme Styles -->

    <!--begin::Layout Skins(used by all pages) -->

@stack('styles')

<!--end::Layout Skins -->
    <link rel="shortcut icon" href="{{ asset('assets/media/logos/favicon.ico')}}"/>

    <style type="text/css">


        .select2-container--default .select2-selection--single .select2-selection__rendered {
            background: #e1e3ec;
            line-height: 1 !important;
        }


        /*Bootstrap 5 equal*/

        .col-xs-15,
        .col-sm-15,
        .col-md-15,
        .col-lg-15 {
            position: relative;
            min-height: 1px;
            padding-right: 10px;
        }

        .col-xs-15 {
            width: 20%;
            float: left;
        }

        @media (min-width: 768px) {
            .col-sm-15 {
                width: 20%;
                float: left;
            }
        }

        @media (min-width: 992px) {
            .col-md-15 {
                width: 20%;
                float: left;
            }
        }

        @media (min-width: 1200px) {
            .col-lg-15 {
                width: 20%;
                float: left;
            }
        }

        .kt-widget26 .kt-widget26__content .kt-widget26__number {
            font-size: 1.6em !important;
        }

        .alert.alert-success {
            background: #0abb87 !important;
            border: 1px solid #0abb87 !important;;
            color: #ffffff !important;;
        }

        .alert.alert-danger {
            background: #fd397a !important;
            border: 1px solid #fd397a !important;
            color: #ffffff !important;
        }

        .dataTables_wrapper .dataTable th, .dataTables_wrapper .dataTable td {
            color: #3a3b3e;
        }

        .dropdown-item {
            color: #444754;
        }

        body {
            color: #4f515f;
        }

        .form-control {
            color: #272829
        }

        .normal-link {
            color: #495084;
        }

        .alert.alert-solid-danger {
            background: rgba(253, 39, 39, 0.1);
        }

        .alert.alert-solid-danger .alert-text {
            color: #bf4545;
        }

    </style>

    @livewireStyles

</head>

<body
    class="kt-page--loading-enabled kt-page--loading kt-page--fixed kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--left kt-aside--fixed kt-page--loading">

    <!-- begin:: Page -->

    <!-- begin:: Header Mobile -->
    <div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
        <div class="kt-header-mobile__logo">
            <a href="demo9/index.html">
                <img alt="Logo" src="{{ asset('assets/media/logos/logo-9-sm.png')}}"/>
            </a>
        </div>
        <div class="kt-header-mobile__toolbar">
            <button class="kt-header-mobile__toolbar-toggler kt-header-mobile__toolbar-toggler--left"
                    id="kt_aside_mobile_toggler"><span></span></button>
            <button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
            <button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i
                    class="flaticon-more-1"></i></button>
        </div>
    </div>

    <div class="kt-grid kt-grid--hor kt-grid--root">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

                <!-- begin:: Header -->
                <div id="kt_header" class="kt-header  kt-header--fixed " data-ktheader-minimize="on">
                    <div class="kt-container kt-container--fluid">

                        <!-- begin: Header Menu -->
                        <button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i
                                class="la la-close"></i></button>
                        <div class="kt-header-menu-wrapper kt-grid__item kt-grid__item--fluid"
                             id="kt_header_menu_wrapper">
                            <button class="kt-aside-toggler kt-aside-toggler--left" id="kt_aside_toggler"><span></span>
                            </button>
                            <div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile ">
                                <ul class="kt-menu__nav ">
                                    <li class="kt-menu__item kt-menu__item--submenu kt-menu__item--rel {{ active_link('dashboard') }}"
                                        data-ktmenu-submenu-toggle="click" aria-haspopup="true">
                                        <a href="{{ route('index') }}" class="kt-menu__link "><span
                                                class="kt-menu__link-text">Ahabanza</span></a>
                                    </li>
                                    @if(auth()->user()->hasRole('admin'))
                                    <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel {{ active_link('users') }}"
                                        data-ktmenu-submenu-toggle="click" aria-haspopup="true">
                                        <a href="{{ route('users.index') }}" class="kt-menu__link"><span
                                                class="kt-menu__link-text mr-4">Users</span></a>
                                            </li>
                                    @endif
                                            <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel {{ active_link('amakoperative') }}">

                                        <a href="{{ route('cooperatives.index') }}" class="kt-menu__link"><span
                                                class="kt-menu__link-text">Amakoperative</span></a>
                                            </li>
                                            <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel {{ active_link('sms') }}">

                                         <a href="{{ route('sms.index') }}" class="kt-menu__link"><span
                                                class="kt-menu__link-text">SMS</span></a>
                                                
                                    </li>  
                                    <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel {{ active_link('bonus') }}"
                                        data-ktmenu-submenu-toggle="click" aria-haspopup="true">
                                        <a href="#" class="kt-menu__link" data-toggle="modal" data-target="#add_bonus_modal"> 
                                        <span class="kt-menu__link-text mr-4">bonus</span></a>
                                            </li>                           
                                </ul>
                            </div>
                        </div>

                        <!-- end: Header Menu -->

                        <!-- begin:: Brand -->
                        <div class="kt-header__brand   kt-grid__item" id="kt_header_brand">
                            <a class="kt-header__brand-logo" href="demo9/index.html">
                                <img alt="Logo" src="{{ asset('assets/media/logos/logo-9.png')}}"/>
                            </a>
                        </div>

                        <!-- end:: Brand -->

                        <!-- begin:: Header Topbar -->
                        <div class="kt-header__topbar kt-grid__item">
                            <div class="kt-header__topbar-item kt-header__topbar-item--search dropdown"
                                 id="kt_quick_search_toggle">
                                <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
                                    <span class="kt-header__topbar-icon"><i class="flaticon2-search-1"></i></span>
                                </div>
                                <div
                                    class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-lg">
                                    <div class="kt-quick-search kt-quick-search--inline" id="kt_quick_search_inline">
                                        <form method="post" action="{{route('info')}}" class="kt-quick-search__form">
                                            @csrf
                                            <div class="input-group">                  
                                            <input type="text" name="identifier" class="form-control kt-quick-search__input"
                                                       placeholder="kode | tel | NID | PID | Purake | Card">
                                                <div class="input-group-prepend">
                                                    <button class="btn btn-sm btn-info" type="submit"><span class="input-group-text"><i
                                                            class="flaticon2-search-1"></i></span></button></div>
                                            </div>
                                        </form>
                                        
                                    </div>
                                </div>
                            </div> 
                            <!--begin: User bar -->
                            <div class="kt-header__topbar-item kt-header__topbar-item--user">
                                <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
                                    <span class="kt-header__topbar-welcome kt-visible-desktop">Hi,</span>
                                    <span
                                        class="kt-header__topbar-username kt-visible-desktop">@if (Auth::check()){{$name=Auth::user()->name }}
                                    @endif</span>
                                    <span
                                        class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold">
                                        @if (Auth::check()){{substr($name,0,1)}}
                                        @endif
                                    </span>

                                    <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
                                    <span
                                        class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold kt-hidden">@if (Auth::check()){{substr($name,0,1)}}
                                        @endif</span>
                                </div>
                                <div
                                    class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">

                                    <!--begin: Head -->
                                    <div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x"
                                         style="background-image: url({{ asset('assets/media/misc/bg-1.jpg') }})">
                                        <div class="kt-user-card__avatar">
                                        <!-- <img class="kt-hidden-" alt="Pic" src="{{ asset('assets/media/users/300_25.jpg')}}" /> -->

                                            <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->

                                            <span
                                                class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold">@if (Auth::check()){{substr($name,0,1)}}
                                        @endif</span>
                                        </div>
                                        <div class="kt-user-card__name">
                                            @if (Auth::check()){{ $name=Auth::user()->name }}
                                    @endif
                                        </div>
                                    </div>

                                    <!--end: Head -->

                                    <!--begin: Navigation -->
                                    <div class="kt-notification">
                                        @if (Auth::check())
                                        <a href="#" data-name="{{$name}}" data-telephone="{{Auth::user()->telephone}}"
                data-email="{{Auth::user()->email}}" data-id="{{Auth::user()->id}}"
                data-toggle="modal" data-target="#editFormModalFew" class="kt-notification__item">
                                        @endif
                                            <div class="kt-notification__item-icon">
                                                <i class="flaticon2-calendar-3 kt-font-success"></i>
                                            </div>
                                            <div class="kt-notification__item-details">
                                                <div class="kt-notification__item-title kt-font-bold">
                                                    My Profile
                                                </div>
                                                <div class="kt-notification__item-time">
                                                    Account details and free to update
                                                </div>
                                            </div>
                                        </a>                                        
                                        <div class="kt-notification__custom kt-space-between">
                                            <a href="#" target="_blank"
                                               class="btn btn-label btn-label-brand btn-sm btn-bold" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Sign Out</a>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                            <a href="#" class="btn btn-clean btn-sm btn-bold">Country Administrator</a>
                                        </div>
                                    </div>

                                    <!--end: Navigation -->
                                </div>
                            </div>

                            <!--end: User bar -->

                        </div>

                        <!-- end:: Header Topbar -->
                    </div>
                </div>

                <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-grid--stretch">
                    <div class="kt-container kt-body  kt-grid kt-grid--ver" id="kt_body">

                        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
                
                        @include('include.messages')

                        @yield('content')



                        <!-- begin:: Footer -->
                            <div class="kt-footer kt-grid__item" id="kt_footer">
                                <div class="kt-container">
                                    <div class="kt-footer__bottom">
                                        <div class="kt-footer__copyright">
                                            {{date('Y')}}&nbsp;&copy;&nbsp;<a href="#"
                                                                     target="_blank" class="kt-link">Mopay</a>
                                        </div>
                                        <div class="kt-footer__menu">
                                            <a href="http://keenthemes.com/metronic" target="_blank" class="kt-link">MMS
                                                - Version 2</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
    
@include('include.datatable_actions_members') 
@include('include.sms_modal') 
@include('pages.users.partials.editFormModalFew')
@include('include.add_bonus_modal') 



    <!-- begin::Global Config(global config for global JS sciprts) -->
    <script>
        var KTAppOptions = {
            "colors": {
                "state": {
                    "brand": "#2c77f4",
                    "light": "#ffffff",
                    "dark": "#282a3c",
                    "primary": "#5867dd",
                    "success": "#34bfa3",
                    "info": "#36a3f7",
                    "warning": "#ffb822",
                    "danger": "#fd3995"
                },
                "base": {
                    "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                    "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
                }
            }
        };
    </script>

    <!-- end::Global Config -->

    <!--begin:: Global Mandatory Vendors -->
    <script src="{{ asset('assets/vendors/general/jquery/dist/jquery.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/popper.js/dist/umd/popper.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/bootstrap/dist/js/bootstrap.min.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/js-cookie/src/js.cookie.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/moment/min/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/tooltip.js/dist/umd/tooltip.min.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/perfect-scrollbar/dist/perfect-scrollbar.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/sticky-js/dist/sticky.min.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/wnumb/wNumb.js')}}" type="text/javascript"></script>

    <!--end:: Global Mandatory Vendors -->

    <!--begin:: Global Optional Vendors -->
    <script src="{{ asset('assets/vendors/general/jquery-form/dist/jquery.form.min.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/block-ui/jquery.blockUI.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/custom/js/vendors/bootstrap-datepicker.init.js')}}"
            type="text/javascript"></script>
    <script
        src="{{ asset('assets/vendors/general/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js')}}"
        type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/custom/js/vendors/bootstrap-timepicker.init.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/bootstrap-daterangepicker/daterangepicker.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/bootstrap-maxlength/src/bootstrap-maxlength.js')}}"
            type="text/javascript"></script>
    <script
        src="{{ asset('assets/vendors/custom/vendors/bootstrap-multiselectsplitter/bootstrap-multiselectsplitter.min.js')}}"
        type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/bootstrap-select/dist/js/bootstrap-select.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/bootstrap-switch/dist/js/bootstrap-switch.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/custom/js/vendors/bootstrap-switch.init.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/select2/dist/js/select2.full.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/ion-rangeslider/js/ion.rangeSlider.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/typeahead.js/dist/typeahead.bundle.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/handlebars/dist/handlebars.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/inputmask/dist/jquery.inputmask.bundle.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/inputmask/dist/inputmask/inputmask.date.extensions.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/inputmask/dist/inputmask/inputmask.numeric.extensions.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/nouislider/distribute/nouislider.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/owl.carousel/dist/owl.carousel.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/autosize/dist/autosize.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/clipboard/dist/clipboard.min.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/dropzone/dist/dropzone.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/summernote/dist/summernote.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/markdown/lib/markdown.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/bootstrap-markdown/js/bootstrap-markdown.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/custom/js/vendors/bootstrap-markdown.init.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/bootstrap-notify/bootstrap-notify.min.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/custom/js/vendors/bootstrap-notify.init.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/jquery-validation/dist/jquery.validate.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/jquery-validation/dist/additional-methods.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/custom/js/vendors/jquery-validation.init.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/toastr/build/toastr.min.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/raphael/raphael.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/morris.js/morris.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/chart.js/dist/Chart.bundle.js')}}"
            type="text/javascript"></script>
    <script
        src="{{ asset('assets/vendors/custom/vendors/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js')}}"
        type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/custom/vendors/jquery-idletimer/idle-timer.min.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/waypoints/lib/jquery.waypoints.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/counterup/jquery.counterup.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/es6-promise-polyfill/promise.min.js')}}"
            type="text/javascript"></script>

    <script src="{{ asset('assets/vendors/general/jquery.repeater/src/lib.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/jquery.repeater/src/jquery.input.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/jquery.repeater/src/repeater.js')}}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/general/dompurify/dist/purify.js')}}" type="text/javascript"></script>

    @stack('scripts')

    <!--end:: Global Optional Vendors -->

    <!--begin::Global Theme Bundle(used by all pages) -->
    <script src="{{ asset('assets/js/demo3/scripts.bundle.js')}}" type="text/javascript"></script>

    <!--end::Global Theme Bundle -->

    <!--begin::Page Vendors(used by this page) -->
{{--<script src="{{ asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>
<script src="//maps.google.com/maps/api/js?key=AIzaSyBTGnKT7dt597vo9QgeQ7BFhvSRP4eiMSM')}}" type="text/javascript"></script>
<script src="{{ asset('assets/vendors/custom/gmaps/gmaps.js')}}" type="text/javascript"></script>--}}

<!--end::Page Vendors -->

    <!--begin::Page Scripts(used by this page) -->
    <script src="{{ asset('assets/js/demo9/pages/dashboard.js')}}" type="text/javascript"></script>

    <!--end::Page Scripts -->

    <!--begin::Page Vendors(used by this page) -->
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js')}}"
            type="text/javascript"></script>
    <!--begin::Page Scripts(used by this page) -->
    <script src="{{ asset('assets/js/demo1/pages/crud/forms/widgets/select2.js')}}"
            type="text/javascript"></script>
    <!--begin::Page Scripts(used by this page) -->
    <script src="{{ asset('assets/js/demo1/pages/crud/datatables/data-sources/html.js')}}"
            type="text/javascript"></script>

    @stack('end_scripts')

@livewireScripts
</body>
</html>
