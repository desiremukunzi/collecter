<?php

return [
    'sms' => [
        'username' => env('SMS_USERNAME'),
        'password' => env('SMS_PASSWORD')
    ],
    'saving_info' => env('SAVING_INFO'),    
    'saving_withdraw' => env('SAVING_WITHDRAW'),
    'reset_pin' => env('RESET_PIN'),
    'authorization_token' => env('AUTHORIZATION_TOKEN')
];
